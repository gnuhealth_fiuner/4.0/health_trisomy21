from trytond.pool import Pool

from . import health_trisomy21
from . import health
from . import health_pediatrics
from . import sequences
from . import configuration

from .wizard import wizard_health_calendar
from .wizard import wizard_create_patient
from .wizard import wizard_appointment_report
from .wizard import wizard_create_lejeune_attention
from .wizard import wizard_shortcut_patient_family
from .report import appointment_report
from .report import lejeune_attention_report
from .report import lejeune_attention_requests_report

def register():
    Pool.register(
        health_trisomy21.GrowthLine,
        health_trisomy21.BarthelEvaluation,
        health_trisomy21.LawtonBrodyEvaluation,
        health_trisomy21.LejeuneAttention,
        health_trisomy21.SurgicalHistory,
        health_trisomy21.SurgicalHistoryOperation,
        health_trisomy21.LejeuneAttentionFamilyMember,
        health.Appointment,
        health.PatientData,
        health.PatientEvaluation,
        health.PatientDiseaseInfo,
        health.PatientECG,
        health_pediatrics.Newborn,
        health_pediatrics.NeonatalControl,
        sequences.GnuHealthSequences,
        sequences.BarthelEvaluationSequence,
        sequences.LawtonBrodyEvaluationSequence,
        sequences.LejeuneAttentionSequence,
        wizard_health_calendar.CreateAppointmentStart,
        wizard_create_patient.CreatePatientStart,
        wizard_appointment_report.AppointmentReportStart,
        wizard_create_lejeune_attention.PatientRelative,
        wizard_create_lejeune_attention.CreateLejeuneAttentionStart,
        wizard_create_lejeune_attention.CreateLejeuneAttentionPediatrics,
        wizard_create_lejeune_attention.CreateLejeuneAttentionAnamnesis,
        wizard_create_lejeune_attention.CreateLejeuneAttentionVaccines,
        wizard_create_lejeune_attention.CreateLejeuneAttentionMedication,
        wizard_create_lejeune_attention.CreateLejeuneAttentionCurrentTherapies,
        wizard_create_lejeune_attention.CreateLejeuneAttentionPhysicalExam,
        wizard_create_lejeune_attention.CreateLejeuneAttentionSchooling,
        wizard_create_lejeune_attention.CreateLejeuneAttentionPsycologicAssesment,
        wizard_create_lejeune_attention.CreateLejeuneAttentionAspectsEverydayLife,
        wizard_create_lejeune_attention.CreateLejeuneAttentionAutonomyConquest,
        wizard_create_lejeune_attention.CreateLejeuneAttentionBehaviourDuringConsultation,
        wizard_create_lejeune_attention.CreateLejeuneAttentionRequestsSuggestions,
        wizard_create_lejeune_attention.CreateLejeuneAttentionLaboratory,
        configuration.TrisomyConfiguration,
        module='health_trisomy21', type_='model'
        )
    Pool.register(
        wizard_create_patient.CreatePatientWizard,
        wizard_appointment_report.AppointmentReportWizard,
        wizard_health_calendar.CreateAppointment,
        wizard_create_lejeune_attention.CreateLejeuneAttentionWizard,
        wizard_shortcut_patient_family.ShortcutPatientFamily,
        module='health_trisomy21', type_='wizard')
    Pool.register(
        appointment_report.AppointmentReport,
        lejeune_attention_report.LejeuneAttentionPediatric,
        lejeune_attention_report.LejeuneAttentionClinic,
        lejeune_attention_requests_report.LejeuneAttentionRequests,
        module='health_trisomy21', type_='report')
