from trytond.report import Report
from trytond.pool import Pool

from datetime import datetime

class AppointmentReport(Report):
    'Appointment Report'
    __name__ = 'gnuhealth.trisomy21.appointment_report'

    @classmethod
    def get_context(cls, records, data, name=None):
        pool = Pool()
        Companys = pool.get('company.company')
        Appointment = pool.get('gnuhealth.appointment')
        Patient = pool.get('gnuhealth.patient')
        context = super().get_context(records, data, name)

        start_date = datetime.combine(context['data']['start_date'],
                                      datetime.min.time())
        end_date = datetime.combine(context['data']['end_date'],
                                    datetime.max.time())

        appointments = Appointment.search([
            ('appointment_date','>=', start_date),
            ('appointment_date', '<=', end_date),
            ('state', 'in', ['done', 'confirmed']),
            ('healthprof','!=', None)
            ])
        context['appointments'] = appointments

        hps = list(set([
                appointment.healthprof for appointment in appointments
                ]))
        context['hps'] = hps
        context['hps_id'] = {}
        for hp in hps:
            context['hps_id'][hp.id] = {}
            # pacientes atendidos por el correspondiente profesional
            patients_cared = [app.patient for app in appointments
                                if hp.id == app.healthprof.id]
            context['hps_id'][hp.id]['patients'] = patients_cared

            # cantidad de pacientes subsidiados
            qty_subsidized = len([patient for patient in patients_cared
                                    if patient.subsidized])
            context['hps_id'][hp.id]['qty_subsidized'] = qty_subsidized
            if patients_cared:
                # porcentaje de pacientes subsidiados
                context['hps_id'][hp.id]['qty_subsidized_percentage'] = \
                    qty_subsidized / len(patients_cared) * 100
                # porcentaje de subsidio
                context['hps_id'][hp.id]['percentage_subsidized'] = \
                    sum([patient.subsidized_percentage for patient
                    in patients_cared if patient.subsidized]) / len(patients_cared)
            else:
                context['qty_subsidized_percentage'] = 0
                context['percentage_subsidized'] = 0
        return context

