from trytond.report import Report
from trytond.pool import Pool
from trytond.transaction import Transaction

from datetime import date


class LejeuneAttentionRequests(Report):
    'Lejeune Attention - Requests'
    __name__ = 'gnuhealth.trisomy21.lejeune_attention.requests.report'

    @classmethod
    def get_context(cls, records, data, name=None):
        pool = Pool()
        Companys = pool.get('company.company')
        Attention = pool.get('gnuhealth.trisomy21.lejeune_attention')
        context = super().get_context(records, data, name)

        attention_id = context['data']['id']

        attention, = Attention.search([
                    ('id','=',attention_id)
                    ])
        context['today'] = date.today()
        return context
