from trytond.report import Report
from trytond.pool import Pool
from trytond.transaction import Transaction

from datetime import date


class LejeuneAttentionPediatric(Report):
    'Lejeune Attention - Pediatric'
    __name__ = 'gnuhealth.trisomy21.lejeune_attention.pediatric.report'

    @classmethod
    def get_context(cls, records, data, name=None):
        pool = Pool()
        Companys = pool.get('company.company')
        Attention = pool.get('gnuhealth.trisomy21.lejeune_attention')
        Patient = pool.get('gnuhealth.patient')
        Condition = pool.get('gnuhealth.patient.disease')
        Newborn = pool.get('gnuhealth.newborn')
        Neonatal = pool.get('gnuhealth.neonatal_control')
        Family = pool.get('gnuhealth.family')
        SurgicalHistory = pool.get('gnuhealth.surgical_history')

        context = super().get_context(records, data, name)

        attention_id = context['data']['id']

        attention, = Attention.search([
                    ('id','=',attention_id)
                    ])
        patient_id = attention.patient.id
        party_id = attention.patient.name.id

        try:
            newborn, = Newborn.search([
                        ('id', '=', patient_id)
                        ])
        except:
            pass

        try:
            neonatal, = Neonatal.search([
                    ('id', '=', patient_id)
                    ])
        except:
            pass

        families = Family.search([
                    ('members.name.id', '=', party_id)
                    ])

        context['families'] = families
        conditions = Condition.search([
                    ('name.id', '=', patient_id)
                    ])
        surgical_histories = SurgicalHistory.search([
                    ('patient.id', '=', patient_id)
                    ])
        context['today'] = date.today()

        return context


class LejeuneAttentionClinic(Report):
    'Lejeune Attention - Pediatric'
    __name__ = 'gnuhealth.trisomy21.lejeune_attention.clinic.report'

    @classmethod
    def get_context(cls, records, data, name=None):
        pool = Pool()
        Companys = pool.get('company.company')
        Attention = pool.get('gnuhealth.trisomy21.lejeune_attention')
        Patient = pool.get('gnuhealth.patient')
        Condition = pool.get('gnuhealth.patient.disease')
        Newborn = pool.get('gnuhealth.newborn')
        Neonatal = pool.get('gnuhealth.neonatal_control')
        Family = pool.get('gnuhealth.family')
        SurgicalHistory = pool.get('gnuhealth.surgical_history')
        Barthel = pool.get('gnuhealth.barthel_evaluation')
        LawtonBrody = pool.get('gnuhealth.lawton_brody_evaluation')

        context = super().get_context(records, data, name)

        attention_id = context['data']['id']

        attention, = Attention.search([
                    ('id','=',attention_id)
                    ])
        patient_id = attention.patient.id
        party_id = attention.patient.name.id

        try:
            newborn, = Newborn.search([
                        ('id', '=', patient_id)
                        ])
        except:
            pass

        try:
            neonatal, = Neonatal.search([
                    ('id', '=', patient_id)
                    ])
        except:
            pass


        families = Family.search([
                    ('members.name.id', '=', party_id)
                    ])
        context['families'] = families

        barthel_evaluations = Barthel.search([
                        ('patient.id', '=', patient_id)
                        ])
        if barthel_evaluations:
            context['barthel'] = barthel_evaluations[-1]
        else:
            context['barthel'] = None

        lawton_brody_evaluations = LawtonBrody.search([
                        ('patient.id', '=', patient_id)
                        ])
        if lawton_brody_evaluations:
            context['lawton_brody'] = lawton_brody_evaluations[-1]
        else:
            context['lawton_brody'] = None

        conditions = Condition.search([
                    ('name.id', '=', patient_id)
                    ])
        surgical_histories = SurgicalHistory.search([
                    ('patient.id', '=', patient_id)
                    ])
        context['today'] = date.today()

        return context
