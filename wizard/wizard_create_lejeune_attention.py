from trytond.model import ModelView, fields
from trytond.wizard import (Wizard, Button, StateView,
                        StateTransition, StateAction)
from trytond.pool import Pool
from trytond.i18n import gettext
from trytond.transaction import Transaction
from trytond.pyson import Bool, Eval, Or
from trytond.exceptions import UserError

from datetime import date, datetime, time
from dateutil.relativedelta import relativedelta


class CreateLejeuneAttention():

    STATES = {'readonly': Eval('state')=='signed'}

    title = fields.Char('Title', readonly=True)
    lejeune_attention = \
        fields.Many2One('gnuhealth.trisomy21.lejeune_attention',
            'Lejeune Attention', help="Lejeune Attention record",
            readonly=True)
    patient = fields.Many2One('gnuhealth.patient', 'Patient', required=True,
            domain=[('id', '=', Eval('patient_domain'))],
            states=STATES)
    patient_domain = fields.Many2One('gnuhealth.patient', 'Patient')
    state = fields.Selection([
        (None, ''),
        ('signed', 'Signed')
        ], 'State', sort=False)

    @fields.depends('patient_domain', 'patient')
    def on_change_with_patient(self, name=None):
        if self.patient_domain:
            return self.patient_domain.id
        return None


class PatientRelative(ModelView):
    'Patient Relative'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.patient_relative'

    create_lejeune_attention_start = \
        fields.Many2One('gnuhealth.trisomy21.create_lejeune_attention.start',
            'Start')
    patient = fields.Many2One('gnuhealth.patient', 'Patient',
                domain=[('id', 'in', Eval('patient_domain'))])
    role = fields.Char('Role', readonly=True)
    conditions = fields.One2Many('gnuhealth.patient.disease',
        None, 'Conditions',
        domain=[('name', '=', Eval('patient'))])

    patient_domain = fields.Many2Many('gnuhealth.patient', None, None, 'Patients domain')


class CreateLejeuneAttentionStart(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Start'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.start'

    STATES = {'readonly': Eval('state')=='signed'}

    family_relatives = fields.One2Many('gnuhealth.trisomy21.create_lejeune_attention.patient_relative',
            'create_lejeune_attention_start', 'Family relatives',
            domain=[('patient', 'in', Eval('family_relatives_domain'))],
            states=STATES)
    family_relatives_domain = fields.Many2Many('gnuhealth.patient',
        None, None, 'Family relatives domain',
            states=STATES)
    families = fields.Many2Many('gnuhealth.family', None, None,
            'Family',
            states=STATES)
    start_briefing = fields.Text('Start briefing', readonly=True)

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_start_title')

    @staticmethod
    def default_start_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.start_briefing


class CreateLejeuneAttentionPediatrics(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Pediatrics'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.pediatrics'

    STATES = {'readonly': Eval('state')=='signed'}

    newborn = fields.Many2One('gnuhealth.newborn', 'Newborn',
            domain=[('id', '=', Eval('newborn'))],
            states=STATES)
    newborn_domain = fields.Many2One('gnuhealth.newborn', 'Newborn',
            required=True, domain=[('id', '=', Eval('newborn_domain'))],
            states=STATES)
    neonatal_control = fields.Many2One('gnuhealth.neonatal_control',
            'Neonatal control',
            domain=[('id', '=', Eval('neonatal_control'))],
            states=STATES)
    neonatal_control_domain = fields.Many2One('gnuhealth.neonatal_control',
            'Neonatal control',
            domain=[('id', '=', Eval('neonatal_control_domain'))],
            states=STATES)
    controlled_pregnancy = fields.Boolean('Controlled pregnancy',
            states=STATES)
    controlled_pregnancy_observations = fields.Text('Observations',
            states=STATES)
    ft_screening = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'First trimester screening', sort=False,
            states=STATES)
    ft_screening_check = fields.Boolean('Screening normal',
            states={'invisible': Or(Eval('ft_screening')=='no', Eval('state')=='signed')})
    ft_screening_check_observations = fields.Text('Observations',
                        states=STATES)
    caesarean = fields.Boolean('Caesarean birth',
            states=STATES)
    cephalic_perimeter = fields.Integer('CP (cm)',
        help="Cephalic Perimeter in centimeters (cm)",
            states=STATES)
    length = fields.Integer('Length (cm)',
        help="Length in centimeters (cm)",
            states=STATES)
    weight = fields.Integer('Weight (kg)',
        help="Weight in grams (g)",
            states=STATES)
    test_metabolic = fields.Boolean(
        'Metabolic ("heel stick screening")',
        help="Test for Fenilketonuria, Congenital Hypothyroidism, "
        "Quistic Fibrosis, Galactosemia",
            states=STATES)
    test_metabolic_observations = fields.Text('Observations', states=STATES)
    #neonatal control
    internment = fields.Text('Internment/hospitalization',
            states=STATES)
    maternal_lactancy_until = fields.Integer('Maternal lactancy until',
        help='Age expresed in years',
            states=STATES)
    control_15_days = fields.Text('15 days life control',
            states=STATES)
    control_30_days = fields.Text('30 days life control',
            states=STATES)

    # apgar scores
    apgar_scores = fields.One2Many('gnuhealth.neonatal.apgar',
        None, 'APGAR scores',
        domain=[('name', '=', Eval('newborn'))],
            states=STATES)
    pediatrics_briefing = fields.Text("Pediatrics briefing", readonly=True)

    @fields.depends('newborn_domain', 'newborn')
    def on_change_with_newborn(self, name=None):
        if self.newborn_domain:
            return self.newborn_domain.id
        return None

    @fields.depends('neonatal_control_domain', 'neonatal_control')
    def on_change_with_neonatal_control(self, name=None):
        if self.neonatal_control_domain:
            return self.neonatal_control_domain.id
        return None

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_pediatrics_title')

    @staticmethod
    def default_pediatrics_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.pediatrics_briefing


class CreateLejeuneAttentionAnamnesis(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Anamnesis'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.anamnesis'

    STATES = {'readonly': Eval('state')=='signed'}

    name = fields.Many2One('gnuhealth.patient', 'Patient',
            states=STATES)
    conditions = fields.One2Many('gnuhealth.patient.disease',
        None, 'Conditions',
        domain=[('name', '=', Eval('patient'))],
            states=STATES)
    surgical_histories = fields.One2Many('gnuhealth.surgical_history',
        None, 'Surgical Histories',
        domain=[('patient', '=', Eval('patient'))],
            states=STATES)
    gender = fields.Char('Gender')
    conditions_briefing = fields.Text("Conditions briefing", readonly=True)
    # imaging
    imaging_results = fields.One2Many('gnuhealth.imaging.test.result',
        None, 'Images',
        domain=[('patient', '=', Eval('patient'))],
            states=STATES)
    # cardiovascular
    ecgs = fields.One2Many('gnuhealth.patient.ecg',
        None, 'ECG',
        domain=[('name', '=', Eval('patient'))],
            states=STATES)
    cardiov_healthprof = fields.Many2One('gnuhealth.healthprofessional',
        'Health professional',
            states=STATES)
    cardiov_health_inst = fields.Many2One('gnuhealth.institution',
        'Health institution',
            states=STATES)
    # neurological
    age_wich_sat = fields.Integer('Age at which he/she sat down',
                help="Age in years",
            states=STATES)
    age_wich_walked = fields.Integer('Age at which he/she started to walk',
                help="Age in years",
            states=STATES)
    age_wich_talked = fields.Integer('Age at which he/she started to talk',
                help="Age in years",
            states=STATES)
    neuropsyc_assessment = fields.Boolean('Neuropsychological assessment',
            states=STATES)
    dementia_assessment = fields.Boolean('Dementia assessment',
            states=STATES)
    cephalic_support = fields.Boolean('Cephalic support', states=STATES)
    sphinters_control = fields.Boolean('Sphinters control', states=STATES)
    neuro_observations = fields.Text('Observations', states=STATES)
    neuro_healthprof = fields.Many2One('gnuhealth.healthprofessional',
        'Health professional',
            states=STATES)
    neuro_health_inst = fields.Many2One('gnuhealth.institution',
        'Health institution',
            states=STATES)
    neurologic_briefing = fields.Text("Neurologic Briefing", readonly=True)
    # psychiatric
    behaviour_disorder = fields.Boolean('Behaviour disorder',
            states=STATES)
    self_inflicted_damage = fields.Boolean('Self inflicted damage',
            states=STATES)
    tea = fields.Boolean('TEA',
            states=STATES)
    tdha = fields.Boolean('TDHA',
            states=STATES)
    psych_observations = fields.Text('Observations', states=STATES)
    psych_health_prof = fields.Many2One('gnuhealth.healthprofessional',\
        'Health Professional', states=STATES)
    psych_health_inst = fields.Many2One('gnuhealth.institution', \
        'Health Institution', states=STATES)
    psychiatric_briefing = fields.Text("Psychiatric briefing", readonly=True)
    # gastrointestinal
    feeding = fields.Text('Feeding',
            states=STATES)
    feeding_intolerance = fields.Boolean('Feeding intolerance',
            states=STATES)
    feeding_intolerance_description = fields.Text('Feeding intolerance',
                states={'readonly': Or(~Eval('feeding_intolerance'), Eval('state')=='signed')})
    bowel_habit = fields.Text('Bowel habit',
            states=STATES)
    gerd_symptoms = fields.Boolean('GERD symptoms',
            states=STATES)
    fecal_occult_blood = fields.Text('Fecal occult blood',
            states=STATES)
    videocolonoscopy = fields.Text('Videocolonoscopy',
            states=STATES)
    celiac_screening = fields.Selection([
        (None, ''),
        ('positive', '+'),
        ('negative', '-')
        ], 'Celiac screening', sort=False,
            states=STATES)
    hla_dq2_dq8 = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'HLA-DQ2-DQ8', sort=False,
            states=STATES)
    gastroint_observations = fields.Text('Observations', states=STATES)
    gastroint_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                'Health Professional',
            states=STATES)
    gastroint_health_inst = fields.Many2One('gnuhealth.institution',
                'Health Institution',
            states=STATES)
    gastrointestinal_briefing = fields.Text("Gastrointestinal briefing", readonly=True)
    # neumonology
    recurrent_infections = fields.Boolean('Recurrent infections',
            states=STATES)
    recurrent_infections_description = fields.Text('Recurrent infections',
                states={'readonly': Or(~Eval('recurrent_infections'), Eval('state')=='signed')})
    hospitalizations = fields.Boolean('Hospitalizations',
            states=STATES)
    hospitalizations_description = fields.Text('Hospitalizations',
                states={'readonly': Or(~Eval('hospitalizations'), Eval('state')=='signed')})
    neumo_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                'Health Professional',
            states=STATES)
    neumo_health_inst = fields.Many2One('gnuhealth.institution',
                'Health institution',
            states=STATES)
    neumonology_briefing = fields.Text("Neumonology briefing")
    orl_briefing = fields.Text("ORL briefing")
    odontostomatology_briefing = fields.Text("Odontostomatology briefing")
    ophthalmology_briefing = fields.Text("Ophthalmology briefing")
    endocrinology_briefing = fields.Text("Endocrinology briefing")
    gynecology_briefing = fields.Text("Gynecology briefing")
    fertility_briefing = fields.Text("Fertility briefing")
    andrology_briefing = fields.Text("Andrology briefing")
    uronefrology_briefing = fields.Text("Uronefrology briefing")
    orthopedics_traumato_briefing = fields.Text(
                    "Orthopedics and Traumatology briefing")
    disability_briefing = fields.Text("Disability briefing")
    oncohematology_briefing = fields.Text("Oncohematology briefing",
                    readonly=True)
    # orl
    last_audiometry = fields.Date('Last audiometry',
            states=STATES)
    sleep_wake_cycle = fields.Text('Sleep/wake cycle',
                help='Description of sleep/wake cycle',
            states=STATES)
    apnea = fields.Boolean('Apnea',
            states=STATES)
    snoring = fields.Boolean('Snoring',
            states=STATES)
    polysomnography = fields.Boolean('Polysomnography',
            states=STATES)
    polysomnography_date = fields.Date('Last date of polysomnography',
            states=STATES)
    oea = fields.Selection([
        (None, ''),
        ('pass_test', 'Pass test'),
        ('not_pass_test', 'Do not pass test')
        ], 'OEA', sort=False, states=STATES)
    orl_observations = fields.Text('Observations', states=STATES)
    orl_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                'Health professional',
            states=STATES)
    orl_health_inst = fields.Many2One('gnuhealth.institution',
                'Health institution',
            states=STATES)
    neumonology_briefing = fields.Text("Neumonology briefing")
    orl_briefing = fields.Text("ORL briefing", readonly=True)
    # odontostomatology
    agenesia = fields.Boolean('Agenesia',
            states=STATES)
    late_dentition = fields.Boolean('Late dentition',
            states=STATES)
    last_dental_control = fields.Date('Last dental control',
            states=STATES)
    dental_orthosis = fields.Boolean('Dental Orthesis',
            states=STATES)
    dental_orthosis_description = fields.Text('Dental orthosis description',
            states={'readonly': Or(~Eval('dental_orthosis'), Eval('state')=='signed')})
    periodontal_disease_counseling = \
        fields.Text('Periodontal disease counseling',
            states=STATES)
    dental_health_prof = fields.Many2One('gnuhealth.healthprofessional',
            'Health professional',
            states=STATES)
    dental_health_inst = fields.Many2One('gnuhealth.institution',
            'Health institution',
            states=STATES)
    odontostomatology_briefing = fields.Text("Odontostomatology briefing",
            readonly=True)
    # ophthalmology
    ophthalmology_assessments = \
        fields.One2Many('gnuhealth.ophthalmology.evaluation',
            None, 'Ophtalmology assessments',
            domain=[('patient', '=', Eval('patient'))],
            states=STATES)
    last_biannual_ophthalm_assessment = \
        fields.Date('Last biannual ophthalmologic assessment',
            states=STATES)
    use_glasses = fields.Binary('Use of glasses',
            states=STATES)
    glasses = fields.Boolean('Glasses', states=STATES)
    ophthalm_healthprof = fields.Many2One('gnuhealth.healthprofessional',
                    'Health professional',
            states=STATES)
    ophthalm_health_inst = fields.Many2One('gnuhealth.institution',
                    'Health institution',
            states=STATES)
    ophthalmology_briefing = fields.Text("Ophthalmology briefing", readonly=True)
    # endocrinology
    thyroid_ultrasound = fields.Boolean('Thyroid ultrasound',
            states=STATES)
    thyroid_ultrasound_observations = fields.Text('Thyroid ultrasound - observations',
            states=STATES)
    annual_tsh = fields.Integer('Annual tsh',
                help="Request Ac if the value is between 5 and 10\n"
                    "(>10 refer to endocrinology with thyroid ultrasound)",
            states=STATES)
    glycemia = fields.Integer('Glycemia', help="Every 6 months if has FR",
            states=STATES)
    cholesterol = fields.Integer('Cholesterol', help="Biennial if BMI>25",
            states=STATES)
    bone_densitometry = fields.Char('Bone densitometry',
            help="From the age of 40",
            states=STATES)
    metabolic_anormal = fields.Boolean('Metabolic anormalities',
            states=STATES)
    metabolic_anormal_description = \
        fields.Text('Metabolic anormalities description',
            states={'readonly': Or(~Eval('metabolic_anormal'),Eval('state')=='signed')})
    t4 = fields.Text('T4', states=STATES)
    endoc_health_prof = \
        fields.Many2One('gnuhealth.healthprofessional',
            'Health professional',
            states=STATES)
    endoc_health_inst = fields.Many2One('gnuhealth.institution',
            'Institution',
            states=STATES)
    endocrinology_briefing = fields.Text("Endocrinology briefing",
            readonly=True)
    # gynecology
    menarche = fields.Integer('Menarche', help="Age in years",
            states=STATES)
    menopause = fields.Integer('Menopause', help="Age in years",
            states=STATES)
    ### early_menopause
    early_mp_suffocation = fields.Boolean('Suffocation',
                        help="As an early menopause symptom",
            states=STATES)
    early_mp_night_sweats = fields.Boolean('Night sweats',
                        help="As an early menopause symptom",
            states=STATES)
    early_mp_headache = fields.Boolean('Headaches',
                        help="As an early menopause symptom",
            states=STATES)
    early_mp_dec_libido = fields.Boolean('Decreased libido',
                        help="As an early menopause symptom",
            states=STATES)
    early_mp_menstrual_change = \
            fields.Boolean('Changes in the menstrual cycle',
                       help="As an early menopause symptom",
            states=STATES)
    early_mp_irritability = \
            fields.Boolean('Irritability and depresion periods',
                        help="As an early menopause symptom",
            states=STATES)
    early_mp_mood_changes = fields.Boolean('Mood changes',
            help="As an early menopause symptom",
            states=STATES)
    menstrual_history = fields.One2Many('gnuhealth.patient.menstrual_history',
            None, 'Menstrual history',
            domain=[('patient', '=', Eval('patient'))],
            states=STATES)
    gyneco_health_prof= fields.Many2One('gnuhealth.healthprofessional',
            'Health professional',
            states=STATES)
    gyneco_health_inst= fields.Many2One('gnuhealth.institution',
            'Health Institution',
            states=STATES)
    gynecology_briefing = fields.Text("Gynecology briefing",
            readonly=True)
    # fertility
    sex_education = fields.Boolean('Sex education and anticonceptive',
            states=STATES)
    sexual_relations = fields.Boolean('Sexual relations',
            states=STATES)
    anticonceptive = fields.Selection([
        (None, ''),
        ('0', 'None'),
        ('1', 'Pill / Minipill'),
        ('2', 'Male condom'),
        ('3', 'Vasectomy'),
        ('4', 'Female sterilisation'),
        ('5', 'Intra-uterine device'),
        ('6', 'Withdrawal method'),
        ('7', 'Fertility cycle awareness'),
        ('8', 'Contraceptive injection'),
        ('9', 'Skin Patch'),
        ('10', 'Female condom'),
        ], 'Contraceptive Method', sort=False,
            states=STATES)
    fert_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                                    'Health professional',
            states=STATES)
    fert_health_inst = fields.Many2One('gnuhealth.institution',
                                    'Institution',
            states=STATES)
    fertility_briefing = fields.Text("Fertility briefing",
            readonly=True)
    # andrology
    andro_mood_changes = fields.Boolean('Mood changes',
                    help="Andropause signs",
            states=STATES)
    andro_fatigue = fields.Boolean('Fatigue',
                    help="Andropause signs",
            states=STATES)
    andro_weight_inc = fields.Boolean('Weight increment',
                    help="Andropause signs",
            states=STATES)
    andro_irritability = fields.Boolean('Irritability',
                    help="Andropause signs",
            states=STATES)
    andro_depression = fields.Boolean('Depression',
                    help="Andropause signs",
            states=STATES)
    andro_sweat = fields.Boolean('Sweat', help="Andropause signs",
            states=STATES)
    andro_headache = fields.Boolean('Headache', help="Andropause signs",
            states=STATES)
    annual_testicular_control = fields.Boolean('Annual testicular control',
                                help="Up to 45 years",
            states=STATES)
    annual_testicular_us = fields.Boolean('Annual testicular ultrasound',
            states=STATES)
    andro_health_prof = \
        fields.Many2One('gnuhealth.healthprofessional',
                'Health professional',
            states=STATES)
    andro_health_inst = fields.Many2One('gnuhealth.institution',
                'Health institution',
            states=STATES)
    andrology_briefing = fields.Text("Andrology briefing",
            readonly=True)
    # uronefrology
    uronefro_observations = fields.Text('Uronefrology observations',
            states=STATES)
    uronefro_health_prof = \
        fields.Many2One('gnuhealth.healthprofessional',
                'Health professional',
            states=STATES)
    uronefro_health_inst = fields.Many2One('gnuhealth.institution',
                'Health institution',
            states=STATES)
    uronefrology_briefing = fields.Text("Uronefrology briefing",
            readonly=True)
    # orthopedics and traumatology
    scoliosis = fields.Boolean('Scoliosis',
            states=STATES)
    scoliosis_description = fields.Text('Scoliosis description',
                    states={'readonly': Or(~Eval('scoliosis'), Eval('state')=='signed')})
    patella_inestability = \
        fields.Boolean('Risk factors or inestability on the patella',
                help="overweight, flatfoot",
            states=STATES)
    patella_inestability_description = \
        fields.Text('Risk factors or inestability on the patella description',
                states={'readonly': Or(~Eval('patella_inestability'), Eval('state')=='signed')})
    carpal_tunnel_synd = fields.Boolean('Carpal tunnel syndrome',
            states=STATES)
    chronic_cervical_myel = fields.Boolean('Chronic cervical myelopathy',
            states=STATES)
    flatfoot = fields.Boolean('Flatfoot',
            states=STATES)
    orthesis_prosthesis = fields.Boolean('Orthesis/prosthesis',
            states=STATES)
    xray_cervical = fields.Boolean('X-ray cervical F and P with Torg index',
            states=STATES)
    xray_pavlov = fields.Boolean('X-ray Pavlov',
            states=STATES)
    xray_hip_panoramic = fields.Boolean('X-ray panoramic hip',
            states=STATES)
    xray_von_rosen = fields.Boolean('X-ray Von Rosen',
            states=STATES)
    reumatology = fields.Text('Reumatology', states=STATES)
    orth_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                    'Health professional', states=STATES)
    orth_health_inst = fields.Many2One('gnuhealth.institution',
                    'Institution', states=STATES)
    orthopedics_traumato_briefing = fields.Text(
                    "Orthopedics and Traumatology briefing",
                    readonly=True)
    # disability
    disab_assesments = fields.One2Many('gnuhealth.patient.disability_assessment',
            None, 'Disabilities assessments',
            domain=[('patient', '=', Eval('patient'))],
            states=STATES)
    disab_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                    'Health professional', states=STATES)
    disab_health_inst = fields.Many2One('gnuhealth.institution',
                    'Institution', states=STATES)
    disability_briefing = fields.Text("Disability briefing",
                    readonly=True)
    # dermatology
    periodic_phys_examination = \
        fields.Boolean('Periodic physical examination',
                       help="Skin, nail, hair",
            states=STATES)
    autoimmune_disease = fields.Boolean('Autoimmune disease',
                        help="Alopecia areata, vitiligo",
            states=STATES)
    derma_observations = fields.Text('Observations', states=STATES)
    derma_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                    'Health professional', states=STATES)
    derma_health_inst = fields.Many2One('gnuhealth.institution',
                    'Institution', states=STATES)
    dermatology_briefing = fields.Text("Dermatology briefing",
                    readonly=True)
    # oncohematology
    onco_observations = fields.Text('Observations', states=STATES)
    onco_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                        'Health professional', states=STATES)
    onco_health_inst = fields.Many2One('gnuhealth.institution',
                        'Institution', states=STATES)
    oncohematology_briefing = fields.Text("Oncohematology briefing",
                readonly=True)

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_anamnesis_title')

    @staticmethod
    def default_conditions_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.conditions_briefing

    @staticmethod
    def default_neurologic_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.neurologic_briefing

    @staticmethod
    def default_psychiatric_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.psychiatric_briefing

    @staticmethod
    def default_gastrointestinal_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.gastrointestinal_briefing

    @staticmethod
    def default_neumonology_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.neumonology_briefing

    @staticmethod
    def default_orl_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.orl_briefing

    @staticmethod
    def default_odontostomatology_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.odontostomatology_briefing

    @staticmethod
    def default_ophthalmology_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.ophthalmology_briefing

    @staticmethod
    def default_endocrinology_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.endocrinology_briefing

    @staticmethod
    def default_gynecology_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.gynecology_briefing

    @staticmethod
    def default_fertility_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.fertility_briefing

    @staticmethod
    def default_andrology_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.andrology_briefing

    @staticmethod
    def default_uronefrology_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.uronefrology_briefing

    @staticmethod
    def default_orthopedics_traumato_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.orthopedics_traumato_briefing

    @staticmethod
    def default_disability_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.disability_briefing

    @staticmethod
    def default_dermatology_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.dermatology_briefing

    @staticmethod
    def default_oncohematology_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.oncohematology_briefing

    @staticmethod
    def default_oncohematology_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.oncohematology_briefing

    @classmethod
    def view_attributes(cls):
        return super().view_attributes() + [
            ('//page[@id="gynecology"]', 'states', {
                'invisible': Eval('gender')=='m'
                })] + [
            ('//page[@id="andrology"]', 'states', {
                'invisible': Eval('gender')=='f'
                })]


class CreateLejeuneAttentionVaccines(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Vaccines'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.vaccines'

    STATES = {'readonly': Eval('state')=='signed'}

    vaccines_observations = fields.Text('Vaccines observations')
    vaccines = fields.One2Many('gnuhealth.vaccination',
            None, 'Vaccines',
            domain=[('name', '=', Eval('patient'))],
            states=STATES)
    vaccines_briefing = fields.Text("Vaccines briefing", readonly=True)

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_vaccines_title')

    @staticmethod
    def default_vaccines_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.vaccines_briefing

class CreateLejeuneAttentionMedication(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Medication'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.medication'

    STATES = {'readonly': Eval('state')=='signed'}

    medication = fields.One2Many('gnuhealth.patient.medication',
            None, 'Medication',
            domain=[('name', '=', Eval('patient'))],
            states=STATES)
    medication_briefing = fields.Text("Medication briefing", readonly=True)

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_medication_title')

    @staticmethod
    def default_medication_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.medication_briefing

class CreateLejeuneAttentionCurrentTherapies(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Current Therapies'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.current_therapies'

    STATES = {'readonly': Eval('state')=='signed'}

    phonoaudiology = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Phonoaudiology', sort=False,
            states=STATES)
    phonoaudiology_freq= fields.Integer('Phonoaudiology frequency',
            help='Monthly frequency',
            states={'readonly': Or(Eval('phonoaudiology') != 'yes', Eval('state')=='signed')})
    occ_therapy = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Occupational therapy', sort=False,
            states=STATES)
    occ_therapy_freq = fields.Integer('Occupational Therapy Frequency',
            help='Monthly frequency',
            states={'readonly': Or(Eval('occ_therapy') != 'yes', Eval('state')=='signed')})
    psycologist = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Psycologist', sort=False,
            states=STATES)
    psycologist_freq = fields.Integer('Psycologist frequency',
            help='Monthly frequency',
            states={'readonly': Or(Eval('psycologist') != 'yes', Eval('state')=='signed')})
    psychiatrist = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Psychiatrist', sort=False,
            states=STATES)
    psychiatrist_freq = fields.Integer('Psychiatrist frequency',
            help='Monthly frequency',
            states={'readonly': Or(Eval('psychiatrist') != 'yes', Eval('state')=='signed')})
    kinesiotherapy = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Kinesiotherapy', sort=False,
            states=STATES)
    kinesiotherapy_freq = fields.Integer('Kinesiotherapy frequency',
            help='Monthly frequency',
            states={'readonly': Or(Eval('kinesiotherapy') != 'yes', Eval('state')=='signed')})
    therapeutic_acc = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Therapeutic accompaniment', sort=False,
            states=STATES)
    therapeutic_acc_freq = fields.Integer(
            'Therapeutic accompaniment frequency',
            help='Monthly frequency',
            states={'readonly': Or(Eval('therapeutic_acc') != 'yes', Eval('state')=='signed')})
    nursing_assistance = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Nursing assistance', sort=False,
            states=STATES)
    nursing_assistance_notes = fields.Text('Nursing assistance',
            states={'readonly': Or(Eval('nursing_assistance') != 'yes', Eval('state')=='signed')})
    observations = fields.Text('Observations', states=STATES)
    current_therapies_briefing = fields.Text("Current therapies briefing",
            readonly=True)

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_current_therapies_title')

    @staticmethod
    def default_current_therapies_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.current_therapies_briefing


class CreateLejeuneAttentionPhysicalExam(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Physical Exam'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.physical_exam'

    STATES = {'readonly': Eval('state')=='signed'}

    # vital signs
    systolic = fields.Integer('Systolic Pressure (mmHg)',
        help='Systolic pressure (mmHg)',
            states=STATES)
    diastolic = fields.Integer('Diastolic Pressure (mmHg)',
        help='Diastolic pressure (mmHg)',
            states=STATES)
    weight = fields.Float('Weight (kg)',
            digits=(3, 2),
            help='Weight in kilos',
            states=STATES)
    height = fields.Float('Height (cm)', digits=(3, 1),
        help='Height in centimeters',
            states=STATES)
    cephalic_perimeter = fields.Float('Cephalic perimeter (cm)', digits=(3, 1),
        help='Perimeter in centimeters',
            states=STATES)
    bmi = fields.Float(
        'BMI', digits=(2, 2),
        help='Body mass index',
            states=STATES)
    indicator = fields.Selection([
        ('l/h-f-a', 'Length/height for age'),
        ('w-f-a', 'Weight for age'),
        ('cp-f-a', 'Cephalic perimeter for age'),
        ], 'Indicator', sort=False)
    growth_lines = fields.One2Many('gnuhealth.trisomy21.create_lejeune_attention.growth_lines',
            None, 'Growth Lines')
    skin = fields.Text('Skin and faneras',
            states=STATES)
    mouth = fields.Text('Mouth',
            states=STATES)
    ears = fields.Text('Ears',
            states=STATES)
    vital_signs_briefing = fields.Text("Vital signs briefing",
            readonly=True)
    # lympha system
    lympha = fields.Text('Lymphatic system',
            states=STATES)
    lymphatic_briefing = fields.Text("Lymphatic briefing",
            readonly=True)
    # respiratory system
    respiratory_system = fields.Text('Respiratory System',
            states=STATES)
    respiratory_briefing = fields.Text("Respiratory briefing",
            readonly=True)
    #cardiovascular system
    cardiovascular_system = fields.Text('Cardiovascular System',
            states=STATES)
    cardiovascular_briefing = fields.Text("Cardiovascular briefing",
            readonly=True)
    #digestive system
    digestive_system = fields.Text('Digestive System',
            states=STATES)
    digestive_briefing = fields.Text("Digestive briefing",
            readonly=True)
    #urologic system
    urologic_system = fields.Text('Urologic System',
            states=STATES)
    urologic_briefing = fields.Text("Urologic briefing",
            readonly=True)
    #oam system
    oam_system = fields.Text('Osteoarthromuscular System',
            states=STATES)
    oam_system_art_limit = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Arthicular limitation', sort=False,
            states=STATES)
    oam_system_scoliosis = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Scoliosis', sort=False,
            states=STATES)
    oam_system_pain = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Arthicular Pain', sort=False,
            states=STATES)
    oam_system_patella_inestability = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Patella inestability', sort=False,
            states=STATES)
    oam_system_heel_valgus = fields.Selection([
        (None,''),
        ('unilateral', 'Unilateral'),
        ('bilateral', 'Bilateral'),
        ('no', 'No')
        ], 'Heel valgus', sort=False,
            states=STATES)
    oam_briefing = fields.Text("OAM briefing", help="Osteo Arthro Muscular")
    # neuro system
    neuro_system = fields.Text('Neurologic system',
            states=STATES)
    neuro_vigil = fields.Text('Vigil',
            states=STATES)
    neuro_orientation_time = fields.Text('Orientation - Time',
            states=STATES)
    neuro_orientation_space = fields.Text('Orientation - Space',
            states=STATES)
    neuro_orientation_people = fields.Text('Orientation - People',
            states=STATES)
    neuro_language = fields.Text('Language',
            states=STATES)
    neuro_tone_strength_sensitivity = fields.Text(
        'Tone, strength and sensitivity',
            states=STATES)
    neuro_dyn_coord_diadochokinesia = fields.Selection([
        (None,''),
        ('succeded', 'Succeded'),
        ('failed', 'Failed'),
        ], 'Diadochokinesia', sort=False,
            states=STATES)
    neuro_dyn_coord_index_nose_test = fields.Selection([
        (None,''),
        ('succeded', 'Succeded'),
        ('failed', 'Failed'),
        ], 'Index-Nose Test', sort=False,
            states=STATES)
    neuro_dyn_coord_walk_straight = fields.Selection([
        (None,''),
        ('succeded', 'Succeded'),
        ('failed', 'Failed'),
        ], 'Walk straight', sort=False,
            states=STATES)
    neuro_dyn_coord_walk_heel = fields.Selection([
        (None,''),
        ('succeded', 'Succeded'),
        ('failed', 'Failed'),
        ], 'Heel walk', sort=False,
            states=STATES)
    neuro_dyn_coord_walk_tiptoes = fields.Selection([
        (None,''),
        ('succeded', 'Succeded'),
        ('failed', 'Failed'),
        ], 'Tiptoes walk', sort=False,
            states=STATES)
    neuro_static_coord_romberg_simple = fields.Selection([
        (None,''),
        ('positive', 'Positive'),
        ('negative', 'Negative'),
        ], 'Romberg simple', sort=False,
            states=STATES)
    neuro_static_coord_romberg_sensitized = fields.Selection([
        (None,''),
        ('positive', 'Positive'),
        ('negative', 'Negative'),
        ], 'Romberg sensitized', sort=False,
            states=STATES)
    neuro_reflex_bicipital = fields.Selection([
        (None,''),
        ('absent', 'Absent'),
        ('present', 'Present')
        ], 'Bicipital reflex',
            states=STATES)
    neuro_reflex_bicipital_notes = fields.Text('Bicipital reflex notes',
            states=STATES)
    neuro_reflex_tricipital = fields.Selection([
        (None,''),
        ('absent', 'Absent'),
        ('present', 'Present')
        ], 'Tricipital reflex',
            states=STATES)
    neuro_reflex_tricipital_notes = fields.Text('Tricipital reflex notes',
            states=STATES)
    neuro_reflex_patella = fields.Selection([
        (None,''),
        ('absent', 'Absent'),
        ('present', 'Present')
        ], 'Patella reflex',
            states=STATES)
    neuro_reflex_patella_notes = fields.Text('Patella reflex notes',
            states=STATES)
    neuro_reflex_aquiliano = fields.Selection([
        (None,''),
        ('absent', 'Absent'),
        ('present', 'Present')
        ], 'Aquiliano reflex',
            states=STATES)
    neuro_reflex_aquiliano_notes = fields.Text('Aquiliano reflex notes',
            states=STATES)
    neuro_reflex_others = fields.Text('Other reflex',
            states=STATES)
    neurologic_exam_briefing = fields.Text("Neurologic exam briefing",
            readonly=True)

    def _get_bmi(self):
        if self.height and self.weight:
            if (self.height > 0):
                return round(self.weight / ((self.height / 100) ** 2), 2)
        return 0

    def _total_months(self, patient=None, attention_lejeune=None):
        p_delta = None
        if patient:
            p_delta = relativedelta(
                self.lejeune_attention.appointment.appointment_date.date(),
                patient.dob)
        elif attention_lejeune:
            p_delta = relativedelta(
                attention_lejeune.appointment.appointment_date.date(),
                attention_lejeune.patient.dob)
        total_months = p_delta.years*12 + p_delta.months
        return total_months

    def _get_p(self, curve):
        pool = Pool()
        model = None

        if self._total_months(patient=self.patient) < 169:
            model = 'gnuhealth.pediatrics.growth_t21.charts.fcsd'
        else:
            model = 'gnuhealth.pediatrics.growth.charts.who'

        try:
            GrowthCharts = pool.get(model)
        except:
            return {}

        lines = GrowthCharts.search([
            ('indicator', '=', self.indicator),
            ('measure', '=', 'p'),
            ('sex', '=', self.patient.gender),
            ('type', '=', curve)
            ], order=[('month', 'ASC')])
        return {point.month:  point.value for point in lines }

    @fields.depends('weight', 'height')
    def on_change_with_bmi(self):
        return self._get_bmi()

    def _update_growth_lines(self):
        pool = Pool()
        AttentionLejeune = pool.get('gnuhealth.trisomy21.lejeune_attention')
        attention_lejeunes = AttentionLejeune.search([
            ('patient', '=', self.patient.id)
            ])
        print('change: ', self.height, self.weight,
              self.indicator, self.cephalic_perimeter)
        self.growth_lines = []
        historical_percentiles = {}
        current_percentil = None
        # set up the historical indicator
        if self.indicator == 'l/h-f-a':
            print('l/h-f-a')
            historical_percentiles = {
                self._total_months(attention_lejeune=al): al.height
                for al in attention_lejeunes if al.height and al.appointment}
            if self.height:
                current_percentil = {
                    self._total_months(patient=self.patient): self.height
                    }
        elif self.indicator == 'w-f-a':
            print('w-f-a')
            historical_percentiles = {
                self._total_months(attention_lejeune=al): al.weight
                for al in attention_lejeunes if self.weight and al.appointment}
            if self.weight:
                current_percentil = {
                    self._total_months(patient=self.patient): self.weight
                    }
        elif self.indicator == 'cp-f-a':
            print('cp-f-a')
            historical_percentiles = {
                self._total_months(attention_lejeune=al): al.cephalic_perimeter
                for al in attention_lejeunes if self.cephalic_perimeter and al.appointment}
            if self.cephalic_perimeter:
                current_percentil = {
                    self._total_months(patient=self.patient): self.cephalic_perimeter
                    }
        # update the historical with the current value
        if current_percentil and \
            self._total_months(patient=self.patient) in historical_percentiles:
            historical_percentiles.update(current_percentil)

        # load all the curves
        p3 = self._get_p('P3')
        p10 = self._get_p('P10')
        p25 = self._get_p('P25')
        p50 = self._get_p('P50')
        p75 = self._get_p('P75')
        p90 = self._get_p('P90')
        p97 = self._get_p('P97')

        # set up the data to load the growth_line model with the loaded curves
        data = [{
            'month': month,
            'p3_value': p3[month],
            'p10_value': month in p10 and p10[month],
            'p25_value': month in p25 and p25[month],
            'p50_value': month in p50 and p50[month],
            'p75_value': month in p75 and p75[month],
            'p90_value': month in p90 and p90[month],
            'p97_value': month in p97 and p97[month],
            } for month in p3]
        # put the historical into the data
        if historical_percentiles:
            for month in historical_percentiles:
                if month in [d['month'] for d in data]:
                    [
                        d.update({'patient_value': historical_percentiles[month] })
                        for d in data if month == d['month']
                    ]
                else:
                    data.append({
                        'month': month,
                        'patient_value': historical_percentiles[month]})
        # update the model
        self.growth_lines = data

    @fields.depends('weight', 'height', 'cephalic_perimeter',
        'patient', 'lejeune_attention', 'indicator',
        'growth_lines')
    def on_change_indicator(self):
        self._update_growth_lines()

    @fields.depends('weight', 'height','cephalic_perimeter',
        'patient', 'lejeune_attention', 'indicator',
        'growth_lines')
    def on_change_weight(self):
        self._update_growth_lines()

    @fields.depends('weight', 'height','cephalic_perimeter',
        'patient', 'lejeune_attention', 'indicator',
        'growth_lines')
    def on_change_height(self):
        self._update_growth_lines()

    @fields.depends('weight', 'height','cephalic_perimeter',
        'patient', 'lejeune_attention', 'indicator',
        'growth_lines')
    def on_change_cephalic_perimeter(self):
        self._update_growth_lines()

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_physical_exam_title')

    @staticmethod
    def default_vital_signs_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.vital_signs_briefing

    @staticmethod
    def default_lymphatic_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.lymphatic_briefing

    @staticmethod
    def default_respiratory_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.respiratory_briefing

    @staticmethod
    def default_cardiovascular_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.cardiovascular_briefing

    @staticmethod
    def default_digestive_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.digestive_briefing

    @staticmethod
    def default_urologic_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.urologic_briefing

    @staticmethod
    def default_oam_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.oam_briefing

    @staticmethod
    def default_neurologic_exam_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.neurologic_exam_briefing

    @staticmethod
    def default_indicator():
        return 'l/h-f-a'


class CreateLejeuneAttentionSchooling(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Schooling'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.schooling'

    STATES = {'readonly': Eval('state')=='signed'}

    ses_assessments = fields.One2Many('gnuhealth.ses.assessment',
            None, 'SES assessments',
            domain=[('patient', '=', Eval('patient'))],
            states=STATES)
    schoolarship_briefing = fields.Text("Schoolarship briefing",
            readonly=True)

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_schooling_title')

    @staticmethod
    def default_schoolarship_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.schoolarship_briefing


class CreateLejeuneAttentionPsycologicAssesment(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Aspects of Everyday Life'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment'

    STATES = {'readonly': Eval('state')=='signed'}

    id_level = fields.Integer('ID level',
            states=STATES)
    id_level_legend = fields.Selection([
        (None,''),
        ('mild_mr', 'Mild mental retardation'),
        ('moderate_mr', 'Moderate mental retardation'),
        ('severe_mr', 'Severe mental retardation'),
        ('above_average', 'Above average'),
        ], 'ID level legend', sort=False,
            states=STATES)
    language_level = fields.Text('Language level',
            states=STATES)
    mood_personality = fields.Text('Mood / personality',
            states=STATES)
    # communication
    read_ = fields.Boolean('Read',
            states=STATES)
    write_ = fields.Selection([
        (None,''),
        ('alone', 'Alone'),
        ('with_support', 'With support')
        ], 'Write', sort=False,
            states=STATES)
    sign_language = fields.Boolean('Sign language',
            states=STATES)
    graph_language = fields.Boolean('Graph language',
            states=STATES)
    other_language = fields.Char('Other language',
            states=STATES)
    # sociability
    has_couple = fields.Boolean('Has couple',
            states=STATES)
    group_of_friends = fields.Boolean('Group of friends',
            states=STATES)
    activity_workshop = fields.Char('Activity workshop',
            states=STATES)

    alcohol = fields.Boolean('Alcohol consumption',
            states=STATES)
    alcohol_frequency = fields.Selection([
        (None, ''),
        ('daily', 'Daily'),
        ('weekly', 'Weekly'),
        ('social', 'Social'),
        ], 'Alcohol frequency', sort=False,
            states=STATES)

    sports_hobbies = fields.Text('Sports and hobbies',
            states=STATES)
    sports_hobbies_center = fields.Boolean('Sports and hobbies specialized center',
            states=STATES)

    occupational_center = fields.Boolean('Occupational center',
            states=STATES)
    has_job = fields.Boolean('Has job?',
            states=STATES)
    job = fields.Many2One('gnuhealth.occupation', 'Job',
            states={'readonly': Or(~Eval('has_job'), Eval('state')=='signed')})
    would_like_job = fields.Boolean('Would like a job?',
            states=STATES)
    # fine motor activity
    sewing = fields.Char('Sewing',
            states=STATES)
    drawing = fields.Char('Drawing',
            states=STATES)
    knitting = fields.Char('Knitting',
            states=STATES)
    scissors = fields.Char('Scissors',
            states=STATES)
    zip_closure = fields.Char('Zip closure',
            states=STATES)
    # means of mobilization
    bicycle = fields.Char('Bicycle',
            states=STATES)
    skate = fields.Char('Skate',
            states=STATES)
    other_means_of_mobilization = fields.Char('Other means of mobilization',
            states=STATES)
    attentional_focus = fields.Text('Maintenance of attentional focus',
            states=STATES)
    psycologic_briefing = fields.Text("Psycologic briefing",
            readonly=True)

    @fields.depends('id_level')
    def on_change_with_id_level_legend(self, name=None):
        if self.id_level:
            if self.id_level >= 70:
                return 'above_average'
            if 50 <= self.id_level < 70:
                return 'mild_mr'
            if 35 <= self.id_level < 50:
                return 'moderate_mr'
            if self.id_level < 35:
                return 'severe_mr'
        return None

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_psyc_assessment_title')

    @staticmethod
    def default_psycologic_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.psycologic_briefing


class CreateLejeuneAttentionAspectsEverydayLife(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Aspects of Everyday Life'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.barthel'

    STATES = {'readonly': Eval('state')=='signed'}

    barthel_evaluations = fields.One2Many('gnuhealth.barthel_evaluation',
            None, 'Barthel evaluations',
            domain=[('patient', '=', Eval('patient'))],
            states=STATES)
    barthel_briefing = fields.Text("Barthel briefing",
            readonly=True)

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_aspects_everyday_life_title')

    @staticmethod
    def default_barthel_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.barthel_briefing


class CreateLejeuneAttentionAutonomyConquest(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Aspects of Everyday Life'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.lawton_brody'

    STATES = {'readonly': Eval('state')=='signed'}

    lawton_brody_evaluations = fields.One2Many('gnuhealth.lawton_brody_evaluation',
            None, 'Lawton-Brody evaluations',
            domain=[('patient', '=', Eval('patient'))],
            states=STATES)
    lawton_brody_briefing = fields.Text("Lawton and Brody briefing",
            readonly=True)

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_autonomy_conquest_title')

    @staticmethod
    def default_lawton_brody_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.lawton_brody_briefing


class CreateLejeuneAttentionBehaviourDuringConsultation(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Behaviour during consultation'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.behaviour'

    STATES = {'readonly': Eval('state')=='signed'}

    behaviour = fields.Text('Behaviour during consultation',
            states=STATES)
    accompanying_persons = fields.Many2Many('gnuhealth.family_member',
            None, None, 'Accompanying persons',
            help="accompanying person during consultation",
            domain=[('id', 'in', Eval('accompanying_persons_domain'))],
            states=STATES)
    accompanying_persons_domain = fields.Many2Many('gnuhealth.family_member',
            None, None, 'Accompanying persons domain',
            states=STATES)
    behaviour_briefing = fields.Text("Behaviour briefing",
            readonly=True)

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_behaviour_title')

    @staticmethod
    def default_behaviour_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.behaviour_briefing


class CreateLejeuneAttentionRequestsSuggestions(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Requests and suggestions'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions'

    STATES = {'readonly': Eval('state')=='signed'}

    requests = fields.Text('Requests',
            states=STATES)
    suggestions = fields.Text('Suggestions',
            states=STATES)
    requests_briefing = fields.Text("Requests briefing",
                readonly=True)
    suggestions_briefing = fields.Text("Suggestions briefing",
                readonly=True)

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_requests_suggestions_title')

    @staticmethod
    def default_requests_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.requests_briefing

    @staticmethod
    def default_suggestions_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.suggestions_briefing


class CreateLejeuneAttentionLaboratory(ModelView, CreateLejeuneAttention):
    'Create Lejeune Attention - Laboratory'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.laboratory'

    STATES = {'readonly': Eval('state')=='signed'}

    lab_requests = fields.One2Many('gnuhealth.patient.lab.test',
            None, 'Requests', domain=[('patient_id', '=', Eval('patient'))],
            states=STATES)
    lab_results = fields.One2Many('gnuhealth.lab',
            None, 'Results', domain=[('patient', '=', Eval('patient'))],
            states=STATES)
    laboratory_briefing = fields.Text("Laboratory briefing",
            readonly=True)

    @staticmethod
    def default_title():
        return gettext('health_trisomy21.msg_get_laboratory_title')

    @staticmethod
    def default_laboratory_briefing():
        pool = Pool()
        Config = pool.get('gnuhealth.trisomy21.configuration')
        config = Config(1)
        return config.laboratory_briefing


class CreateLejeuneAttentionWizard(Wizard):
    'Create Lejeune Attention - Wizard'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.wizard'

    start = StateView('gnuhealth.trisomy21.create_lejeune_attention.start',
                'health_trisomy21.create_lejeune_attention_start',[
                    Button('Next', 'start2pediatrics', 'tryton-go-next'),
                    Button('Close', 'end', 'tryton-cancel')
                    ])

    start2pediatrics = StateTransition()

    pediatrics = StateView('gnuhealth.trisomy21.create_lejeune_attention.pediatrics',
                'health_trisomy21.create_lejeune_attention_pediatrics',[
                    Button('Previous', 'pediatrics2start', 'tryton-back'),
                    Button('Next', 'pediatrics2anamnesis', 'tryton-go-next'),
                    Button('Close', 'end', 'tryton-cancel')
                    ])
    pediatrics2start = StateTransition()
    pediatrics2anamnesis = StateTransition()

    anamnesis = StateView('gnuhealth.trisomy21.create_lejeune_attention.anamnesis',
                'health_trisomy21.create_lejeune_attention_anamnesis',[
                    Button('Previous', 'anamnesis2pediatrics', 'tryton-back'),
                    Button('Next', 'anamnesis2vaccines', 'tryton-go-next'),
                    Button('Close', 'end', 'tryton-cancel')
                    ])
    anamnesis2pediatrics = StateTransition()
    anamnesis2vaccines = StateTransition()

    vaccines = StateView('gnuhealth.trisomy21.create_lejeune_attention.vaccines',
                'health_trisomy21.create_lejeune_attention_vaccines',[
                    Button('Previous', 'vaccines2anamnesis', 'tryton-back'),
                    Button('Next', 'vaccines2medication', 'tryton-go-next'),
                    Button('Close', 'end', 'tryton-cancel')
                    ])
    vaccines2anamnesis = StateTransition()
    vaccines2medication = StateTransition()

    medication = StateView('gnuhealth.trisomy21.create_lejeune_attention.medication',
                'health_trisomy21.create_lejeune_attention_medication',[
                    Button('Previous', 'medication2vaccines', 'tryton-back'),
                    Button('Next', 'medication2current_therapies', 'tryton-go-next'),
                    Button('Close', 'end', 'tryton-cancel')
                    ])
    medication2vaccines = StateTransition()
    medication2current_therapies = StateTransition()

    current_therapies = StateView('gnuhealth.trisomy21.create_lejeune_attention.current_therapies',
                'health_trisomy21.create_lejeune_attention_current_therapies',[
                    Button('Previous', 'current_therapies2medication', 'tryton-back'),
                    Button('Next', 'current_therapies2physical_exam', 'tryton-go-next'),
                    Button('Close', 'end', 'tryton-cancel')
                    ])
    current_therapies2medication = StateTransition()
    current_therapies2physical_exam = StateTransition()

    physical_exam = StateView('gnuhealth.trisomy21.create_lejeune_attention.physical_exam',
                'health_trisomy21.create_lejeune_attention_physical_exam',[
                    Button('Previous', 'physical_exam2current_therapies', 'tryton-back'),
                    Button('Next', 'physical_exam2schooling', 'tryton-go-next'),
                    Button('Close', 'end', 'tryton-cancel')
                    ])
    physical_exam2current_therapies = StateTransition()
    physical_exam2schooling = StateTransition()

    schooling = StateView('gnuhealth.trisomy21.create_lejeune_attention.schooling',
                'health_trisomy21.create_lejeune_attention_schooling',[
                    Button('Previous', 'schooling2physical_exam', 'tryton-back'),
                    Button('Next', 'schooling2psyc_assessment', 'tryton-go-next'),
                    Button('Close', 'end', 'tryton-cancel')
                    ])
    schooling2physical_exam = StateTransition()
    schooling2psyc_assessment = StateTransition()

    psyc_assessment = StateView('gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment',
                'health_trisomy21.create_lejeune_attention_psyc_assessment',[
                    Button('Previous', 'psyc2schooling', 'tryton-back'),
                    Button('Next', 'psyc2barthel', 'tryton-go-next'),
                    Button('Close', 'end', 'tryton-cancel')
                    ])
    psyc2schooling = StateTransition()
    psyc2barthel = StateTransition()

    barthel = StateView('gnuhealth.trisomy21.create_lejeune_attention.barthel',
                'health_trisomy21.create_lejeune_attention_barthel',[
                    Button('Previous', 'barthel2psy_assessment', 'tryton-back'),
                    Button('Next', 'barthel2lawton_brody', 'tryton-go-next'),
                    Button('Close', 'end', 'tryton-cancel')
                    ])
    barthel2psy_assessment = StateTransition()
    barthel2lawton_brody = StateTransition()

    lawton_brody = StateView('gnuhealth.trisomy21.create_lejeune_attention.lawton_brody',
                'health_trisomy21.create_lejeune_attention_lawton_brody',[
                    Button('Previous', 'lawton_brody2barthel', 'tryton-back'),
                    Button('Next', 'lawton_brody2behaviour', 'tryton-go-next'),
                    Button('Close', 'end', 'tryton-cancel')
                    ])
    lawton_brody2barthel = StateTransition()
    lawton_brody2behaviour = StateTransition()

    behaviour = StateView('gnuhealth.trisomy21.create_lejeune_attention.behaviour',
                'health_trisomy21.create_lejeune_attention_behaviour',[
                    Button('Previous', 'behaviour2lawton_brody', 'tryton-back'),
                    Button('Next', 'behaviour2requests_suggestions', 'tryton-go-next'),
                    Button('Close', 'end', 'tryton-cancel')
                    ])
    behaviour2lawton_brody = StateTransition()
    behaviour2requests_suggestions = StateTransition()


    requests_suggestions = StateView('gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions',
            'health_trisomy21.create_lejeune_attention_requests_suggestions',[
                Button('Previous', 'reqs_suggs2behaviour', 'tryton-back'),
                Button('Next', 'reqs_suggs2laboratory', 'tryton-go-next'),
                Button('Close', 'end', 'tryton-cancel')
                ])
    reqs_suggs2behaviour = StateTransition()
    reqs_suggs2laboratory = StateTransition()

    laboratory = StateView('gnuhealth.trisomy21.create_lejeune_attention.laboratory',
                'health_trisomy21.create_lejeune_attention_laboratory',[
                    Button('Previous', 'lab2reqs_sugs', 'tryton-back'),
                    Button('End and sign', 'lab2end_sign', 'tryton-go-next'),
                    Button('Close', 'end', 'tryton-cancel')
                    ])
    lab2reqs_sugs = StateTransition()
    lab2end_sign = StateTransition()

    end_sign = StateAction('health_trisomy21.gnuhealth_action_lejeune_attention')

    def default_start(self, fields):
        pool = Pool()
        LejeuneAttention = pool.get('gnuhealth.trisomy21.lejeune_attention')
        Family = pool.get('gnuhealth.family')
        FamilyMember = pool.get('gnuhealth.family_member')
        Appointment = pool.get('gnuhealth.appointment')
        Patient = pool.get('gnuhealth.patient')
        Conditions = pool.get('gnuhealth.patient.disease')

        appointment_id = Transaction().context.get('active_id')
        lejeune_attention = None
        appointment = Appointment(appointment_id)
        patient_id = appointment.patient.id
        try:
            lejeune_attention, = LejeuneAttention.search([
                ('appointment.id', '=', appointment_id),
                ('patient.id', '=', appointment.patient.id)])
            if not lejeune_attention.name:
                lejeune_attention.name = LejeuneAttention().generate_code()
        except:
            lejeune_attention= LejeuneAttention()
            lejeune_attention.name = LejeuneAttention().generate_code()
            lejeune_attention.patient = patient_id
            lejeune_attention.appointment = appointment.id
            lejeune_attention.save()
        families = Family.search([('members.party.id', '=', Patient(patient_id).name.id)])
        members = FamilyMember.search([
            ('name', 'in', [family.id for family in families])
            ])
        family_relatives_domain = Patient.search([
            ('name.id', 'in', [member.party.id for member in members]),
            ('id', '!=', patient_id)
            ])
        data = {
            'lejeune_attention': lejeune_attention.id,
            'patient': appointment.patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'patient_domain': appointment.patient.id,
            'family_relatives_domain': [patient.id for patient in family_relatives_domain],
            'families': [family.id for family in families],
            'family_relatives': [{
                'patient': patient.id,
                'role': '/'.join([member.role for member in members
                    if member.party.id == patient.name.id]),
                'patient_domain': [p.id for p in family_relatives_domain],
                'conditions': [c.id for c in
                    Conditions.search([('name', '=', patient.id)])],
                } for patient in family_relatives_domain],
            }
        return data

    def save_start(self):
        family_relatives = self.start.family_relatives
        for relative in family_relatives:
            for condition in relative.conditions:
                condition.save()

    def transition_start2pediatrics(self):
        self.save_start()
        return 'pediatrics'

    def default_pediatrics(self, fields):
        pool = Pool()
        Newborn = pool.get('gnuhealth.newborn')
        NeonatalControl = pool.get('gnuhealth.neonatal_control')

        newborn = None
        neonatal_control = None
        patient = self.start.patient
        patient_domain = patient
        lejeune_attention = self.start.lejeune_attention

        try:
            newborn, = Newborn.search([
                ('patient', '=', patient.id)
                ])
        except:
            newborn = Newborn()
            newborn.patient = patient.id
            newborn.sex = patient.gender if patient.gender in ['m','f'] else \
                    'm' if patient.gender == 'm-f' \
                    else 'f'
            newborn.birth_date = datetime.combine(patient.dob, datetime.min.time())
            newborn.save()

        try:
            neonatal_control, = NeonatalControl.search([
                ('patient', '=', patient.id)
                ])
        except:
            neonatal_control = NeonatalControl()
            neonatal_control.patient = patient.id
            neonatal_control.save()

        apgar_scores = [apgar.id for apgar in newborn.apgar_scores if newborn.apgar_scores]

        return {
            'lejeune_attention': self.start.lejeune_attention.id,
            'patient': patient.id,
            'patient_domain': patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'controlled_pregnancy': newborn.controlled_pregnancy,
            'controlled_pregnancy_observations': newborn.controlled_pregnancy_observations,
            'ft_screening': newborn.ft_screening,
            'ft_screening_check': newborn.ft_screening_check,
            'ft_screening_check_observations': newborn.ft_screening_check_observations,
            'caesarean': newborn.caesarean,
            'cephalic_perimeter': newborn.cephalic_perimeter,
            'length': newborn.length,
            'weight': newborn.weight,
            'test_metabolic': newborn.test_metabolic,
            'test_metabolic_observations': newborn.test_metabolic_observations,
            'internment': neonatal_control.internment,
            'maternal_lactancy_until': neonatal_control.maternal_lactancy_until,
            'control_15_days': neonatal_control.control_15_days,
            'control_30_days': neonatal_control.control_30_days,
            'apgar_scores': apgar_scores,
            'newborn': newborn.id,
            'newborn_domain': newborn.id,
            'neonatal_control': neonatal_control.id,
            'neonatal_control_domain': neonatal_control.id
            }

    def save_pediatrics(self):
        # save all info to the corresponding record
        newborn = self.pediatrics.newborn
        newborn.controlled_pregnancy = self.pediatrics.controlled_pregnancy
        newborn.controlled_pregnancy_observations = \
            self.pediatrics.controlled_pregnancy_observations
        newborn.ft_screening = self.pediatrics.ft_screening
        newborn.ft_screening_check = self.pediatrics.ft_screening_check
        newborn.ft_screening_check_observations = self.pediatrics.ft_screening_check_observations
        newborn.caesarean = self.pediatrics.caesarean
        newborn.cephalic_perimeter = self.pediatrics.cephalic_perimeter
        newborn.length = self.pediatrics.length
        newborn.weight = self.pediatrics.weight
        newborn.test_metabolic = self.pediatrics.test_metabolic
        newborn.test_metabolic_observations = self.pediatrics.test_metabolic_observations
        newborn.save()

        neonatal_control = self.pediatrics.neonatal_control
        neonatal_control.maternal_lactancy_until = \
            self.pediatrics.maternal_lactancy_until
        neonatal_control.control_15_days = \
            self.pediatrics.control_15_days
        neonatal_control.control_30_days = \
            self.pediatrics.control_30_days
        neonatal_control.internment = \
            self.pediatrics.internment
        neonatal_control.save()

        try:
            apgar_scores = self.pediatrics.apgar_scores
            for apgar in apgar_scores:
                apgar.name = newborn.id
                apgar.save()
        except:
            pass

    def transition_pediatrics2anamnesis(self):
        self.save_pediatrics()
        return 'anamnesis'

    def transition_pediatrics2start(self):
        self.save_pediatrics()
        return 'start'

    def default_anamnesis(self, fields):
        pool = Pool()
        Conditions = pool.get('gnuhealth.patient.disease')
        SurgicalHistory = pool.get('gnuhealth.surgical_history')
        ECG = pool.get('gnuhealth.patient.ecg')
        ImagingResults = pool.get('gnuhealth.imaging.test.result')
        Ophthalmology = pool.get('gnuhealth.ophthalmology.evaluation')
        DisabAssesments = pool.get('gnuhealth.patient.disability_assessment')

        lejeune_attention = self.start.lejeune_attention
        patient = self.start.patient
        conditions = Conditions.search([('name', '=', patient.id)])
        surgical_histories = SurgicalHistory.search([
                ('patient', '=', patient.id)
                ])
        ecgs = ECG.search([('name', '=', patient.id)])
        imaging_results = ImagingResults.search([('patient', '=', patient.id)])

        ophthalmology_assessments = Ophthalmology.search([
            ('patient', '=', patient.id)
            ])
        disab_assesments = DisabAssesments.search([
            ('patient', '=', patient.id)
            ])

        return {
            'lejeune_attention': lejeune_attention.id,
            'patient': patient.id,
            'gender': patient.gender,
            'patient_domain': patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'name': patient.id,
            'conditions': [condition.id for condition in conditions],
            'surgical_histories': [history.id for history in surgical_histories],
            'ecgs': [ecg.id for ecg in ecgs],
            'imaging_results': [result.id for result in imaging_results],
            'cardiov_healthprof': lejeune_attention.cardiov_healthprof \
                                and lejeune_attention.cardiov_healthprof.id,
            'cardiov_health_inst': lejeune_attention.cardiov_health_inst \
                                and lejeune_attention.cardiov_health_inst.id,
            'neuropsyc_assessment': lejeune_attention.neuropsyc_assessment,
            'dementia_assessment': lejeune_attention.dementia_assessment,
            'cephalic_support': lejeune_attention.cephalic_support,
            'sphinters_control': lejeune_attention.sphinters_control,
            'neuro_observations': lejeune_attention.neuro_observations,
            'neuro_healthprof': lejeune_attention.neuro_healthprof \
                                and lejeune_attention.neuro_healthprof.id,
            'neuro_health_inst': lejeune_attention.neuro_health_inst \
                                and lejeune_attention.neuro_health_inst.id,
            'behaviour_disorder': lejeune_attention.behaviour_disorder,
            'self_inflicted_damage': lejeune_attention.self_inflicted_damage,
            'tea': lejeune_attention.tea,
            'tdha': lejeune_attention.tdha,
            'psych_observations': lejeune_attention.psych_observations,
            'psych_health_prof': lejeune_attention.psych_health_prof and \
                                lejeune_attention.psych_health_prof.id,
            'psych_health_inst': lejeune_attention.psych_health_inst and \
                                lejeune_attention.psych_health_inst.id,
            'feeding': lejeune_attention.feeding,
            'feeding_intolerance': lejeune_attention.feeding_intolerance,
            'feeding_intolerance_description': \
                        lejeune_attention.feeding_intolerance_description,
            'bowel_habit': lejeune_attention.bowel_habit,
            'gerd_symptoms': lejeune_attention.gerd_symptoms,
            'fecal_occult_blood': lejeune_attention.fecal_occult_blood,
            'videocolonoscopy': lejeune_attention.videocolonoscopy,
            'celiac_screening': lejeune_attention.celiac_screening,
            'hla_dq2_dq8': lejeune_attention.hla_dq2_dq8,
            'gastroint_observations': lejeune_attention.gastroint_observations,
            'gastroint_health_prof': lejeune_attention.gastroint_health_prof \
                                and lejeune_attention.gastroint_health_prof.id,
            'gastroint_health_inst': lejeune_attention.gastroint_health_inst \
                                and lejeune_attention.gastroint_health_inst.id,
            'recurrent_infections': lejeune_attention.recurrent_infections,
            'recurrent_infections_description': \
                        lejeune_attention.recurrent_infections_description,
            'hospitalizations': lejeune_attention.hospitalizations,
            'hospitalizations_description': \
                        lejeune_attention.hospitalizations_description,
            'neumo_health_prof': lejeune_attention.neumo_health_prof \
                                and lejeune_attention.neumo_health_prof.id,
            'neumo_health_inst': lejeune_attention.neumo_health_inst \
                                and lejeune_attention.neumo_health_inst.id,
            'last_audiometry': lejeune_attention.last_audiometry,
            'sleep_wake_cycle': lejeune_attention.sleep_wake_cycle,
            'apnea': lejeune_attention.apnea,
            'snoring': lejeune_attention.snoring,
            'polysomnography': lejeune_attention.polysomnography,
            'polysomnography_date': lejeune_attention.polysomnography_date,
            'oea': lejeune_attention.oea,
            'orl_observations': lejeune_attention.orl_observations,
            'orl_health_prof': lejeune_attention.orl_health_prof \
                                and lejeune_attention.orl_health_prof.id,
            'orl_health_inst': lejeune_attention.orl_health_inst \
                                and lejeune_attention.orl_health_inst.id,
            'agenesia': lejeune_attention.agenesia,
            'late_dentition': lejeune_attention.late_dentition,
            'last_dental_control': lejeune_attention.last_dental_control,
            'dental_orthosis': lejeune_attention.dental_orthosis,
            'dental_orthosis_description': \
                            lejeune_attention.dental_orthosis_description,
            'periodontal_disease_counseling': \
                            lejeune_attention.periodontal_disease_counseling,
            'dental_health_prof': lejeune_attention.dental_health_prof \
                            and lejeune_attention.dental_health_prof.id,
            'dental_health_inst': lejeune_attention.dental_health_inst \
                            and lejeune_attention.dental_health_inst.id,
            'ophthalmology_assessments': [oa.id for oa in ophthalmology_assessments],
            'last_biannual_ophthalm_assessment': \
                lejeune_attention.last_biannual_ophthalm_assessment,
            'use_glasses': lejeune_attention.use_glasses,
            'glasses': lejeune_attention.glasses,
            'ophthalm_healthprof': lejeune_attention.ophthalm_healthprof \
                            and lejeune_attention.ophthalm_healthprof.id,
            'ophthalm_health_inst': lejeune_attention.ophthalm_health_inst \
                            and lejeune_attention.ophthalm_health_inst.id,
            'thyroid_ultrasound': lejeune_attention.thyroid_ultrasound,
            'thyroid_ultrasound_observations': lejeune_attention.thyroid_ultrasound_observations,
            'annual_tsh': lejeune_attention.annual_tsh,
            'glycemia': lejeune_attention.glycemia,
            'cholesterol': lejeune_attention.cholesterol,
            'bone_densitometry': lejeune_attention.bone_densitometry,
            'metabolic_anormal': lejeune_attention.metabolic_anormal,
            'metabolic_anormal_description': \
                            lejeune_attention.metabolic_anormal_description,
            't4': lejeune_attention.t4,
            'endoc_health_prof': lejeune_attention.endoc_health_prof \
                            and lejeune_attention.endoc_health_prof.id,
            'endoc_health_inst': lejeune_attention.endoc_health_inst \
                            and lejeune_attention.endoc_health_inst.id,
            'menarche': lejeune_attention.menarche,
            'menopause': lejeune_attention.menopause,
            'early_mp_suffocation': lejeune_attention.early_mp_suffocation,
            'early_mp_night_sweats': lejeune_attention.early_mp_night_sweats,
            'early_mp_headache': lejeune_attention.early_mp_headache,
            'early_mp_dec_libido': lejeune_attention.early_mp_dec_libido,
            'early_mp_menstrual_change': \
                            lejeune_attention.early_mp_menstrual_change,
            'early_mp_irritability': lejeune_attention.early_mp_irritability,
            'early_mp_mood_changes': lejeune_attention.early_mp_mood_changes,
            'gyneco_health_prof': lejeune_attention.gyneco_health_prof \
                            and lejeune_attention.gyneco_health_prof.id,
            'gyneco_health_inst': lejeune_attention.gyneco_health_inst \
                            and lejeune_attention.gyneco_health_inst.id,
            'sex_education': lejeune_attention.sex_education,
            'sexual_relations': lejeune_attention.sexual_relations,
            'anticonceptive': lejeune_attention.anticonceptive,
            'fert_health_prof': lejeune_attention.fert_health_prof \
                            and lejeune_attention.fert_health_prof.id,
            'fert_health_inst': lejeune_attention.fert_health_inst \
                            and lejeune_attention.fert_health_inst.id,
            'andro_mood_changes': lejeune_attention.andro_mood_changes,
            'andro_fatigue': lejeune_attention.andro_fatigue,
            'andro_weight_inc': lejeune_attention.andro_weight_inc,
            'andro_irritability': lejeune_attention.andro_irritability,
            'andro_depression': lejeune_attention.andro_depression,
            'andro_sweat': lejeune_attention.andro_sweat,
            'andro_headache': lejeune_attention.andro_headache,
            'annual_testicular_control': \
                            lejeune_attention.annual_testicular_control,
            'annual_testicular_us': lejeune_attention.annual_testicular_us,
            'andro_health_prof': lejeune_attention.andro_health_prof \
                            and lejeune_attention.andro_health_prof.id,
            'andro_health_inst': lejeune_attention.andro_health_inst \
                            and lejeune_attention.andro_health_inst.id,
            'uronefro_observations': lejeune_attention.uronefro_observations,
            'uronefro_health_prof': lejeune_attention.uronefro_health_prof \
                            and lejeune_attention.uronefro_health_prof.id,
            'uronefro_health_inst': lejeune_attention.uronefro_health_inst \
                            and lejeune_attention.uronefro_health_inst.id,
            'scoliosis': lejeune_attention.scoliosis,
            'scoliosis_description': lejeune_attention.scoliosis_description,
            'patella_inestability': lejeune_attention.patella_inestability,
            'patella_inestability_description': \
                            lejeune_attention.patella_inestability_description,
            'carpal_tunnel_synd': lejeune_attention.carpal_tunnel_synd,
            'chronic_cervical_myel': lejeune_attention.chronic_cervical_myel,
            'flatfoot': lejeune_attention.flatfoot,
            'orthesis_prosthesis': lejeune_attention.orthesis_prosthesis,
            'xray_cervical': lejeune_attention.xray_cervical,
            'xray_pavlov': lejeune_attention.xray_pavlov,
            'xray_hip_panoramic': lejeune_attention.xray_hip_panoramic,
            'xray_von_rosen': lejeune_attention.xray_von_rosen,
            'reumatology': lejeune_attention.reumatology,
            'orth_health_prof': lejeune_attention.orth_health_prof \
                            and lejeune_attention.orth_health_prof.id,
            'orth_health_inst': lejeune_attention.orth_health_inst \
                            and lejeune_attention.orth_health_inst.id,
            'disab_assesments': [d.id for d in disab_assesments],
            'disab_health_prof': lejeune_attention.disab_health_prof \
                            and lejeune_attention.disab_health_inst.id,
            'disab_health_inst': lejeune_attention.disab_health_inst \
                            and lejeune_attention.disab_health_inst.id,
            'periodic_phys_examination': \
                            lejeune_attention.periodic_phys_examination,
            'autoimmune_disease': lejeune_attention.autoimmune_disease,
            'derma_observations': lejeune_attention.derma_observations,
            'derma_health_prof': lejeune_attention.derma_health_prof \
                            and lejeune_attention.derma_health_prof.id,
            'derma_health_inst': lejeune_attention.derma_health_inst \
                            and lejeune_attention.derma_health_inst.id,
            'onco_observations': lejeune_attention.onco_observations,
            'onco_health_prof': lejeune_attention.onco_health_prof \
                            and lejeune_attention.onco_health_prof.id,
            'onco_health_inst': lejeune_attention.onco_health_inst \
                            and lejeune_attention.onco_health_inst.id,
            }

    def save_anamnesis(self):
        anamnesis = self.anamnesis
        lejeune_attention = anamnesis.lejeune_attention
        patient = self.start.patient
        conditions = anamnesis.conditions
        histories = anamnesis.surgical_histories
        ecgs = anamnesis.ecgs
        imaging_results = anamnesis.imaging_results
        ophthalmology_assessments = anamnesis.ophthalmology_assessments
        disab_assessments = anamnesis.disab_assesments

        for condition in conditions:
            condition.name = patient.id
            condition.save()
        for history in histories:
            history.patient = patient.id
            history.save()
        for ecg in ecgs:
            ecg.name = patient.id
            ecg.save()
        for results in imaging_results:
            results.patient = patient.id
            results.save()
        for oa in ophthalmology_assessments:
            oa.patient = patient.id
            oa.save()
        for da in disab_assessments:
            da.patient = patient.id
            da.save()
        # cardiovascular
        lejeune_attention.cardiov_healthprof = anamnesis.cardiov_healthprof \
                    and anamnesis.cardiov_healthprof.id
        lejeune_attention.cardiov_health_inst = anamnesis.cardiov_health_inst \
                    and anamnesis.cardiov_health_inst.id
        # neurological
        lejeune_attention.neuropsyc_assessment = \
                    anamnesis.neuropsyc_assessment
        lejeune_attention.dementia_assessment = \
                    anamnesis.dementia_assessment
        lejeune_attention.cephalic_support = \
                    anamnesis.cephalic_support
        lejeune_attention.sphinters_control = \
                    anamnesis.sphinters_control
        lejeune_attention.neuro_observations = \
                    anamnesis.neuro_observations
        lejeune_attention.neuro_healthprof = anamnesis.neuro_healthprof \
                    and anamnesis.neuro_healthprof.id
        lejeune_attention.neuro_health_inst = anamnesis.neuro_health_inst \
                    and anamnesis.neuro_health_inst.id
        # psychiatric
        lejeune_attention.behaviour_disorder = anamnesis.behaviour_disorder
        lejeune_attention.self_inflicted_damage = \
                    anamnesis.self_inflicted_damage
        lejeune_attention.tea = anamnesis.tea
        lejeune_attention.tdha = anamnesis.tdha
        lejeune_attention.psych_observations = anamnesis.psych_observations
        lejeune_attention.psych_health_prof = \
            anamnesis.psych_health_prof and anamnesis.psych_health_prof.id
        lejeune_attention.psych_health_inst = \
            anamnesis.psych_health_inst and anamnesis.psych_health_inst.id
        # gastrointestinal
        lejeune_attention.feeding = anamnesis.feeding
        lejeune_attention.feeding_intolerance = anamnesis.feeding_intolerance
        lejeune_attention.feeding_intolerance_description = \
                    anamnesis.feeding_intolerance_description
        lejeune_attention.bowel_habit = anamnesis.bowel_habit
        lejeune_attention.gerd_symptoms = anamnesis.gerd_symptoms
        lejeune_attention.fecal_occult_blood = anamnesis.fecal_occult_blood
        lejeune_attention.videocolonoscopy = anamnesis.videocolonoscopy
        lejeune_attention.celiac_screening = anamnesis.celiac_screening
        lejeune_attention.hla_dq2_dq8 = anamnesis.hla_dq2_dq8
        lejeune_attention.gastroint_observations = anamnesis.gastroint_observations
        lejeune_attention.gastroint_health_prof = anamnesis.gastroint_health_prof \
                    and anamnesis.gastroint_health_prof.id
        lejeune_attention.gastroint_health_inst = anamnesis.gastroint_health_inst \
                    and anamnesis.gastroint_health_inst.id
        # neumonology
        lejeune_attention.recurrent_infections = \
                    anamnesis.recurrent_infections
        lejeune_attention.recurrent_infections_description = \
                    anamnesis.recurrent_infections_description
        lejeune_attention.hospitalizations = anamnesis.hospitalizations
        lejeune_attention.hospitalizations_description = \
                    anamnesis.hospitalizations_description
        lejeune_attention.neumo_health_prof = anamnesis.neumo_health_prof \
                    and anamnesis.neumo_health_prof.id
        lejeune_attention.neumo_health_inst = anamnesis.neumo_health_inst \
                    and anamnesis.neumo_health_inst.id
        # orl
        lejeune_attention.last_audiometry = anamnesis.last_audiometry
        lejeune_attention.sleep_wake_cycle = anamnesis.sleep_wake_cycle
        lejeune_attention.apnea = anamnesis.apnea
        lejeune_attention.snoring = anamnesis.snoring
        lejeune_attention.polysomnography = anamnesis.polysomnography
        lejeune_attention.polysomnography_date = anamnesis.polysomnography_date
        lejeune_attention.oea = anamnesis.oea
        lejeune_attention.orl_observations = anamnesis.orl_observations
        lejeune_attention.orl_health_prof = anamnesis.orl_health_prof \
                    and anamnesis.orl_health_prof.id
        lejeune_attention.orl_health_inst = anamnesis.orl_health_inst \
                    and anamnesis.orl_health_inst.id
        # odontostomatology
        lejeune_attention.agenesia = anamnesis.agenesia
        lejeune_attention.late_dentition = anamnesis.late_dentition
        lejeune_attention.last_dental_control = anamnesis.last_dental_control
        lejeune_attention.dental_orthosis = anamnesis.dental_orthosis
        lejeune_attention.dental_orthosis_description = \
                    anamnesis.dental_orthosis_description
        lejeune_attention.periodontal_disease_counseling = \
                    anamnesis.periodontal_disease_counseling
        lejeune_attention.dental_health_prof = anamnesis.dental_health_prof \
                    and anamnesis.dental_health_prof.id
        lejeune_attention.dental_health_inst = anamnesis.dental_health_inst \
                    and anamnesis.dental_health_inst.id
        # ophthalmology
        lejeune_attention.last_biannual_ophthalm_assessment = \
            anamnesis.last_biannual_ophthalm_assessment
        lejeune_attention.use_glasses = anamnesis.use_glasses
        lejeune_attention.glasses = anamnesis.glasses
        lejeune_attention.ophthalm_healthprof = anamnesis.ophthalm_healthprof \
                    and anamnesis.ophthalm_healthprof.id
        lejeune_attention.ophthalm_health_inst = anamnesis.ophthalm_health_inst \
                    and anamnesis.ophthalm_health_inst.id
        # endocrinology
        lejeune_attention.thyroid_ultrasound = anamnesis.thyroid_ultrasound
        lejeune_attention.thyroid_ultrasound_observations = anamnesis.thyroid_ultrasound_observations
        lejeune_attention.annual_tsh = anamnesis.annual_tsh
        lejeune_attention.glycemia = anamnesis.glycemia
        lejeune_attention.cholesterol = anamnesis.cholesterol
        lejeune_attention.bone_densitometry = anamnesis.bone_densitometry
        lejeune_attention.metabolic_anormal = anamnesis.metabolic_anormal
        lejeune_attention.metabolic_anormal_description = \
                    anamnesis.metabolic_anormal_description
        lejeune_attention.t4 = anamnesis.t4
        lejeune_attention.endoc_health_prof = anamnesis.endoc_health_prof \
                    and anamnesis.endoc_health_prof.id
        lejeune_attention.endoc_health_inst = anamnesis.endoc_health_inst \
                    and anamnesis.endoc_health_inst.id
        # gynecology
        if patient.gender in ['f', 'f-m']:
            lejeune_attention.early_mp_suffocation = anamnesis.early_mp_suffocation
            lejeune_attention.early_mp_night_sweats = anamnesis.early_mp_night_sweats
            lejeune_attention.early_mp_headache = anamnesis.early_mp_headache
            lejeune_attention.early_mp_dec_libido = anamnesis.early_mp_dec_libido
            lejeune_attention.early_mp_menstrual_change = \
                            anamnesis.early_mp_menstrual_change
            lejeune_attention.early_mp_irritability = anamnesis.early_mp_irritability
            lejeune_attention.early_mp_mood_changes = anamnesis.early_mp_mood_changes
            for history in anamnesis.menstrual_history:
                if history.id < 0:
                    history.patient = patient.id
                    history.save()
            lejeune_attention.gyneco_health_prof = anamnesis.gyneco_health_prof \
                    and anamnesis.gyneco_health_prof.id
            lejeune_attention.gyneco_health_inst = anamnesis.gyneco_health_inst \
                    and anamnesis.gyneco_health_inst.id
        # fertility
        lejeune_attention.sex_education = anamnesis.sex_education
        lejeune_attention.sexual_relations = anamnesis.sexual_relations
        lejeune_attention.anticonceptive = anamnesis.anticonceptive
        lejeune_attention.fert_health_prof = anamnesis.fert_health_prof \
                    and anamnesis.fert_health_prof.id
        lejeune_attention.fert_health_inst = anamnesis.fert_health_inst \
                    and anamnesis.fert_health_inst.id
        # andrology
        if patient.gender in ['m', 'm-f']:
            lejeune_attention.andro_mood_changes = \
                            anamnesis.andro_mood_changes
            lejeune_attention.andro_fatigue = anamnesis.andro_fatigue
            lejeune_attention.andro_weight_inc = anamnesis.andro_weight_inc
            lejeune_attention.andro_irritability = \
                            anamnesis.andro_irritability
            lejeune_attention.andro_depression = anamnesis.andro_depression
            lejeune_attention.andro_sweat = anamnesis.andro_sweat
            lejeune_attention.andro_headache = anamnesis.andro_headache
            lejeune_attention.annual_testicular_control = \
                            anamnesis.annual_testicular_control
            lejeune_attention.annual_testicular_us = \
                            anamnesis.annual_testicular_us
            lejeune_attention.andro_health_prof = anamnesis.andro_health_prof \
                            and anamnesis.andro_health_prof.id
            lejeune_attention.andro_health_inst = anamnesis.andro_health_inst \
                        and anamnesis.andro_health_inst.id
        # uronefrology
        lejeune_attention.uronefro_observations = anamnesis.uronefro_observations
        lejeune_attention.uronefro_health_prof = anamnesis.uronefro_health_prof \
                        and anamnesis.uronefro_health_prof.id
        lejeune_attention.uronefro_health_inst = anamnesis.uronefro_health_inst \
                        and anamnesis.uronefro_health_inst.id
        # orthopedics
        lejeune_attention.scoliosis = anamnesis.scoliosis
        lejeune_attention.scoliosis_description = \
                            anamnesis.scoliosis_description
        lejeune_attention.patella_inestability = \
                            anamnesis.patella_inestability
        lejeune_attention.patella_inestability_description = \
                            anamnesis.patella_inestability_description
        lejeune_attention.carpal_tunnel_synd = \
                            anamnesis.carpal_tunnel_synd
        lejeune_attention.chronic_cervical_myel = \
                            anamnesis.chronic_cervical_myel
        lejeune_attention.flatfoot = anamnesis.flatfoot
        lejeune_attention.orthesis_prosthesis = anamnesis.orthesis_prosthesis
        lejeune_attention.xray_cervical = anamnesis.xray_cervical
        lejeune_attention.xray_pavlov = anamnesis.xray_pavlov
        lejeune_attention.xray_hip_panoramic = anamnesis.xray_hip_panoramic
        lejeune_attention.xray_von_rosen = anamnesis.xray_von_rosen
        lejeune_attention.reumatology = anamnesis.reumatology
        lejeune_attention.orth_health_prof = anamnesis.orth_health_prof \
                        and anamnesis.orth_health_prof.id
        lejeune_attention.orth_health_inst = anamnesis.orth_health_inst \
                        and anamnesis.orth_health_inst.id
        #disabilities
        lejeune_attention.disab_health_prof = anamnesis.disab_health_prof \
                        and anamnesis.disab_health_prof.id
        lejeune_attention.disab_health_inst = anamnesis.disab_health_inst \
                        and anamnesis.disab_health_inst.id
        # dermatology
        lejeune_attention.periodic_phys_examination = \
                            anamnesis.periodic_phys_examination
        lejeune_attention.autoimmune_disease = \
                            anamnesis.autoimmune_disease
        lejeune_attention.derma_observations = \
                            anamnesis.derma_observations
        lejeune_attention.derma_health_prof = anamnesis.derma_health_prof \
                        and anamnesis.derma_health_prof.id
        lejeune_attention.derma_health_inst = anamnesis.derma_health_inst \
                        and anamnesis.derma_health_inst.id
        #oncohematology
        lejeune_attention.onco_observations = anamnesis.onco_observations
        lejeune_attention.onco_health_prof = anamnesis.onco_health_prof \
                        and anamnesis.onco_health_prof.id
        lejeune_attention.onco_health_inst = anamnesis.onco_health_inst \
                        and anamnesis.onco_health_inst.id
        lejeune_attention.save()
        patient.save()

    def transition_anamnesis2pediatrics(self):
        self.save_anamnesis()
        return 'pediatrics'

    def transition_anamnesis2vaccines(self):
        self.save_anamnesis()
        return 'vaccines'

    def default_vaccines(self, fields):
        pool = Pool()
        Vaccine = pool.get('gnuhealth.vaccination')
        vaccines = Vaccine.search([('name', '=', self.start.patient.id)])
        lejeune_attention = self.start.lejeune_attention
        patient = self.start.patient
        patient_domain = patient
        return {
            'lejeune_attention': lejeune_attention.id,
            'patient': patient.id,
            'patient_domain': patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'vaccines_observations': lejeune_attention.vaccines_observations or '',
            'vaccines': [vaccine.id for vaccine in vaccines]
            }

    def save_vaccines(self):
        lejeune_attention = self.start.lejeune_attention
        lejeune_attention.vaccines_observations = self.vaccines.vaccines_observations
        lejeune_attention.save()
        for vaccine in self.vaccines.vaccines:
            vaccine.name = self.start.patient.id
            vaccine.save()

    def transition_vaccines2anamnesis(self):
        self.save_vaccines()
        return 'anamnesis'

    def transition_vaccines2medication(self):
        self.save_vaccines()
        return 'medication'

    def default_medication(self, fields):
        pool = Pool()
        Medication = pool.get('gnuhealth.patient.medication')
        medication = Medication.search([
            ('name', '=', self.start.patient.id)
            ])

        lejeune_attention = self.start.lejeune_attention
        patient = self.start.patient
        patient_domain = patient

        return {
            'lejeune_attention': lejeune_attention.id,
            'patient': patient.id,
            'patient_domain': patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'medication': [m.id for m in medication]
            }

    def save_medication(self):
        for m in self.medication.medication:
            m.name = self.start.patient.id
            m.save()

    def transition_medication2vaccines(self):
        self.save_medication()
        return 'vaccines'

    def transition_medication2current_therapies(self):
        self.save_medication()
        return 'current_therapies'

    def default_current_therapies(self, fields):
        lejeune_attention = self.start.lejeune_attention
        patient = self.start.patient

        return {
            'lejeune_attention': lejeune_attention.id,
            'patient': patient.id,
            'patient_domain': patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'phonoaudiology': lejeune_attention.phonoaudiology,
            'phonoaudiology_freq': lejeune_attention.phonoaudiology_freq,
            'occ_therapy': lejeune_attention.occ_therapy,
            'occ_therapy_freq': lejeune_attention.occ_therapy_freq,
            'psycologist': lejeune_attention.psycologist,
            'psycologist_freq': lejeune_attention.psycologist_freq,
            'psychiatrist': lejeune_attention.psychiatrist,
            'psychiatrist_freq': lejeune_attention.psychiatrist_freq,
            'kinesiotherapy': lejeune_attention.kinesiotherapy,
            'kinesiotherapy_freq': lejeune_attention.kinesiotherapy_freq,
            'therapeutic_acc': lejeune_attention.therapeutic_acc,
            'therapeutic_acc_freq': lejeune_attention.therapeutic_acc_freq,
            'nursing_assistance': lejeune_attention.nursing_assistance,
            'nursing_assistance_notes': lejeune_attention.nursing_assistance_notes,
            'observations': lejeune_attention.current_ther_observations,
            }

    def save_current_therapies(self):
        lejeune_attention = self.current_therapies.lejeune_attention
        lejeune_attention.phonoaudiology = \
            self.current_therapies.phonoaudiology
        lejeune_attention.phonoaudiology_freq = \
            self.current_therapies.phonoaudiology_freq
        lejeune_attention.occ_therapy = \
            self.current_therapies.occ_therapy
        lejeune_attention.occ_therapy_freq = \
            self.current_therapies.occ_therapy_freq
        lejeune_attention.psycologist = \
            self.current_therapies.psycologist
        lejeune_attention.psycologist_freq = \
            self.current_therapies.psycologist_freq
        lejeune_attention.psychiatrist = \
            self.current_therapies.psychiatrist
        lejeune_attention.psychiatrist_freq = \
            self.current_therapies.psychiatrist_freq
        lejeune_attention.kinesiotherapy = \
            self.current_therapies.kinesiotherapy
        lejeune_attention.kinesiotherapy_freq = \
            self.current_therapies.kinesiotherapy_freq
        lejeune_attention.therapeutic_acc = \
            self.current_therapies.therapeutic_acc
        lejeune_attention.therapeutic_acc_freq = \
            self.current_therapies.therapeutic_acc_freq
        lejeune_attention.nursing_assistance = \
            self.current_therapies.nursing_assistance
        lejeune_attention.nursing_assistance_notes = \
            self.current_therapies.nursing_assistance_notes
        lejeune_attention.current_ther_observations = \
            self.current_therapies.observations
        lejeune_attention.save()

    def transition_current_therapies2medication(self):
        self.save_current_therapies()
        return 'medication'

    def transition_current_therapies2physical_exam(self):
        self.save_current_therapies()
        return 'physical_exam'

    def default_physical_exam(self, fields):
        patient = self.start.patient
        lejeune_attention = self.start.lejeune_attention
        patient_domain = patient
        return {
            'lejeune_attention': lejeune_attention.id,
            'patient': patient.id,
            'patient_domain': patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'systolic': lejeune_attention.systolic,
            'diastolic': lejeune_attention.diastolic,
            'weight': lejeune_attention.weight,
            'height': lejeune_attention.height,
            'cephalic_perimeter': lejeune_attention.cephalic_perimeter,
            'bmi': lejeune_attention.bmi,
            'skin': lejeune_attention.skin,
            'mouth': lejeune_attention.mouth,
            'ears': lejeune_attention.ears,
            'lympha': lejeune_attention.lympha,
            'respiratory_system': lejeune_attention.respiratory_system,
            'cardiovascular_system': lejeune_attention.cardiovascular_system,
            'digestive_system': lejeune_attention.digestive_system,
            'urologic_system': lejeune_attention.urologic_system,
            'oam_system': lejeune_attention.oam_system,
            'oam_system_art_limit': lejeune_attention.oam_system_art_limit,
            'oam_system_scoliosis': lejeune_attention.oam_system_scoliosis,
            'oam_system_pain': lejeune_attention.oam_system_pain,
            'oam_system_patella_inestability': lejeune_attention.oam_system_patella_inestability,
            'oam_system_heel_valgus': lejeune_attention.oam_system_heel_valgus,
            'neuro_system': lejeune_attention.neuro_system,
            'neuro_vigil': lejeune_attention.neuro_vigil,
            'neuro_orientation_time': lejeune_attention.neuro_orientation_time,
            'neuro_orientation_space': lejeune_attention.neuro_orientation_space,
            'neuro_orientation_people': lejeune_attention.neuro_orientation_people,
            'neuro_language': lejeune_attention.neuro_language,
            'neuro_tone_strength_sensitivity': lejeune_attention.neuro_tone_strength_sensitivity,
            'neuro_dyn_coord_diadochokinesia': lejeune_attention.neuro_dyn_coord_diadochokinesia,
            'neuro_dyn_coord_index_nose_test': lejeune_attention.neuro_dyn_coord_index_nose_test,
            'neuro_dyn_coord_walk_straight': lejeune_attention.neuro_dyn_coord_walk_straight,
            'neuro_dyn_coord_walk_heel': lejeune_attention.neuro_dyn_coord_walk_heel,
            'neuro_dyn_coord_walk_tiptoes': lejeune_attention.neuro_dyn_coord_walk_tiptoes,
            'neuro_static_coord_romberg_simple': lejeune_attention.neuro_static_coord_romberg_simple,
            'neuro_static_coord_romberg_sensitized': lejeune_attention.neuro_static_coord_romberg_sensitized,
            'neuro_reflex_bicipital': lejeune_attention.neuro_reflex_bicipital,
            'neuro_reflex_bicipital_notes': lejeune_attention.neuro_reflex_bicipital_notes,
            'neuro_reflex_tricipital': lejeune_attention.neuro_reflex_tricipital,
            'neuro_reflex_tricipital_notes': lejeune_attention.neuro_reflex_tricipital_notes,
            'neuro_reflex_patella': lejeune_attention.neuro_reflex_patella,
            'neuro_reflex_patella_notes': lejeune_attention.neuro_reflex_patella_notes,
            'neuro_reflex_aquiliano': lejeune_attention.neuro_reflex_aquiliano,
            'neuro_reflex_aquiliano': lejeune_attention.neuro_reflex_aquiliano,
            'neuro_reflex_others': lejeune_attention.neuro_reflex_others,
            }

    def save_physical_exam(self):
        lejeune_attention = self.start.lejeune_attention
        lejeune_attention.systolic = self.physical_exam.systolic
        lejeune_attention.diastolic = self.physical_exam.diastolic
        lejeune_attention.weight = self.physical_exam.weight
        lejeune_attention.height = self.physical_exam.height
        lejeune_attention.cephalic_perimeter = self.physical_exam.cephalic_perimeter
        lejeune_attention.bmi = self.physical_exam.bmi
        lejeune_attention.skin = self.physical_exam.skin
        lejeune_attention.mouth = self.physical_exam.mouth
        lejeune_attention.ears = self.physical_exam.ears
        lejeune_attention.lympha = self.physical_exam.lympha
        lejeune_attention.respiratory_system = \
                        self.physical_exam.respiratory_system
        lejeune_attention.cardiovascular_system = \
                        self.physical_exam.cardiovascular_system
        lejeune_attention.digestive_system = \
                        self.physical_exam.digestive_system
        lejeune_attention.urologic_system = \
                        self.physical_exam.urologic_system
        lejeune_attention.oam_system = self.physical_exam.oam_system
        lejeune_attention.oam_system_art_limit = \
                        self.physical_exam.oam_system_art_limit
        lejeune_attention.oam_system_scoliosis = \
                        self.physical_exam.oam_system_scoliosis
        lejeune_attention.oam_system_pain = \
                        self.physical_exam.oam_system_pain
        lejeune_attention.oam_system_patella_inestability = \
                        self.physical_exam.oam_system_patella_inestability
        lejeune_attention.oam_system_heel_valgus = \
                        self.physical_exam.oam_system_heel_valgus
        lejeune_attention.neuro_system = self.physical_exam.neuro_system
        lejeune_attention.neuro_vigil = self.physical_exam.neuro_vigil
        lejeune_attention.neuro_orientation_time = \
                        self.physical_exam.neuro_orientation_time
        lejeune_attention.neuro_orientation_space = \
                        self.physical_exam.neuro_orientation_space
        lejeune_attention.neuro_orientation_people = \
                        self.physical_exam.neuro_orientation_people
        lejeune_attention.neuro_language = self.physical_exam.neuro_language
        lejeune_attention.neuro_tone_strength_sensitivity = \
                        self.physical_exam.neuro_tone_strength_sensitivity
        lejeune_attention.neuro_dyn_coord_diadochokinesia = \
                        self.physical_exam.neuro_dyn_coord_diadochokinesia
        lejeune_attention.neuro_dyn_coord_index_nose_test = \
                        self.physical_exam.neuro_dyn_coord_index_nose_test
        lejeune_attention.neuro_dyn_coord_walk_straight = \
                        self.physical_exam.neuro_dyn_coord_walk_straight
        lejeune_attention.neuro_dyn_coord_walk_heel = \
                        self.physical_exam.neuro_dyn_coord_walk_heel
        lejeune_attention.neuro_dyn_coord_walk_tiptoes = \
                        self.physical_exam.neuro_dyn_coord_walk_tiptoes
        lejeune_attention.neuro_static_coord_romberg_simple = \
                        self.physical_exam.neuro_static_coord_romberg_simple
        lejeune_attention.neuro_static_coord_romberg_sensitized = \
                    self.physical_exam.neuro_static_coord_romberg_sensitized
        lejeune_attention.neuro_reflex_bicipital = \
                    self.physical_exam.neuro_reflex_bicipital
        lejeune_attention.neuro_reflex_bicipital_notes = \
                    self.physical_exam.neuro_reflex_bicipital_notes
        lejeune_attention.neuro_reflex_tricipital = \
                    self.physical_exam.neuro_reflex_tricipital
        lejeune_attention.neuro_reflex_tricipital_notes = \
                    self.physical_exam.neuro_reflex_tricipital_notes
        lejeune_attention.neuro_reflex_patella = \
                    self.physical_exam.neuro_reflex_patella
        lejeune_attention.neuro_reflex_patella_notes = \
                    self.physical_exam.neuro_reflex_patella_notes
        lejeune_attention.neuro_reflex_aquiliano = \
                    self.physical_exam.neuro_reflex_aquiliano
        lejeune_attention.neuro_reflex_aquiliano_notes = \
                    self.physical_exam.neuro_reflex_aquiliano_notes
        lejeune_attention.neuro_reflex_others = \
                    self.physical_exam.neuro_reflex_others
        lejeune_attention.save()

    def transition_physical_exam2current_therapies(self):
        self.save_physical_exam()
        return 'current_therapies'

    def transition_physical_exam2schooling(self):
        self.save_physical_exam()
        return 'schooling'

    def default_schooling(self, fields):
        pool = Pool()
        SES = pool.get('gnuhealth.ses.assessment')

        lejeune_attention = self.start.lejeune_attention
        patient = self.start.patient
        ses_assessments = SES.search([('patient', '=', patient.id)])

        return {
            'lejeune_attention': lejeune_attention.id,
            'patient': patient.id,
            'patient_domain': patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'ses_assessments': [ses.id for ses in ses_assessments],
            }

    def save_schooling(self):
        for ses in self.schooling.ses_assessments:
            ses.patient = self.start.patient.id
            ses.save()

    def transition_schooling2physical_exam(self):
        self.save_schooling()
        return 'physical_exam'

    def transition_schooling2psyc_assessment(self):
        self.save_schooling()
        return 'psyc_assessment'

    def default_psyc_assessment(self, fields):
        pool = Pool()

        patient = self.start.patient
        patient_domain = patient
        lejeune_attention = self.start.lejeune_attention
        return {
            'lejeune_attention': lejeune_attention.id,
            'patient': patient.id,
            'patient_domain': patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'id_level': lejeune_attention.id_level,
            'id_level_legend': lejeune_attention.id_level_legend,
            'language_level': lejeune_attention.language_level,
            'mood_personality': lejeune_attention.mood_personality,
            'read_': lejeune_attention.read_,
            'write_': lejeune_attention.write_,
            'sign_language': lejeune_attention.sign_language,
            'graph_language': lejeune_attention.graph_language,
            'other_language': lejeune_attention.other_language,
            'has_couple': lejeune_attention.has_couple,
            'group_of_friends': lejeune_attention.group_of_friends,
            'activity_workshop': lejeune_attention.activity_workshop,
            'alcohol': lejeune_attention.alcohol,
            'alcohol_frequency': lejeune_attention.alcohol_frequency,
            'sports_hobbies': lejeune_attention.sports_hobbies,
            'sports_hobbies_center': lejeune_attention.sports_hobbies_center,
            'occupational_center': lejeune_attention.occupational_center,
            'has_job': lejeune_attention.has_job,
            'job': lejeune_attention.job,
            'would_like_job': lejeune_attention.would_like_job,
            'sewing': lejeune_attention.sewing,
            'drawing': lejeune_attention.drawing,
            'knitting': lejeune_attention.knitting,
            'scissors': lejeune_attention.scissors,
            'zip_closure': lejeune_attention.zip_closure,
            'bicycle': lejeune_attention.bicycle,
            'skate': lejeune_attention.skate,
            'other_means_of_mobilization': lejeune_attention.other_means_of_mobilization,
            'attentional_focus': lejeune_attention.attentional_focus,
            }

    def save_psyc(self):
        lejeune_attention = self.psyc_assessment.lejeune_attention
        psyc_assessment = self.psyc_assessment
        lejeune_attention.id_level = psyc_assessment.id_level
        lejeune_attention.id_level_legend = psyc_assessment.id_level_legend
        lejeune_attention.language_level = psyc_assessment.language_level
        lejeune_attention.mood_personality = psyc_assessment.mood_personality
        lejeune_attention.read_ = psyc_assessment.read_
        lejeune_attention.write_ = psyc_assessment.write_
        lejeune_attention.sign_language = psyc_assessment.sign_language
        lejeune_attention.graph_language = psyc_assessment.graph_language
        lejeune_attention.other_language = psyc_assessment.other_language
        lejeune_attention.has_couple = psyc_assessment.has_couple
        lejeune_attention.group_of_friends = psyc_assessment.group_of_friends
        lejeune_attention.activity_workshop = \
                    psyc_assessment.activity_workshop
        lejeune_attention.alcohol = psyc_assessment.alcohol
        lejeune_attention.alcohol_frequency = \
                    psyc_assessment.alcohol_frequency
        lejeune_attention.sports_hobbies = psyc_assessment.sports_hobbies
        lejeune_attention.sports_hobbies_center = \
                    psyc_assessment.sports_hobbies_center
        lejeune_attention.occupational_center = \
                    psyc_assessment.occupational_center
        lejeune_attention.has_job = psyc_assessment.has_job
        lejeune_attention.job = psyc_assessment.job
        lejeune_attention.would_like_job = psyc_assessment.would_like_job
        lejeune_attention.sewing = psyc_assessment.sewing
        lejeune_attention.drawing = psyc_assessment.drawing
        lejeune_attention.knitting = psyc_assessment.knitting
        lejeune_attention.scissors = psyc_assessment.scissors
        lejeune_attention.zip_closure = psyc_assessment.zip_closure
        lejeune_attention.bicycle = psyc_assessment.bicycle
        lejeune_attention.skate = psyc_assessment.skate
        lejeune_attention.other_means_of_mobilization = \
                    psyc_assessment.other_means_of_mobilization
        lejeune_attention.attentional_focus = \
                    psyc_assessment.attentional_focus
        lejeune_attention.save()

    def transition_psyc2schooling(self):
        self.save_psyc()
        return 'schooling'

    def transition_psyc2barthel(self):
        self.save_psyc()
        return 'barthel'

    def default_barthel(self, fields):
        pool = Pool()
        Barthel = pool.get('gnuhealth.barthel_evaluation')

        lejeune_attention = self.start.lejeune_attention
        patient = self.start.patient
        patient_domain = patient
        barthel_evaluations = Barthel.search([('patient', '=', patient.id)])
        return {
            'lejeune_attention': lejeune_attention.id,
            'patient': patient.id,
            'patient_domain': patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'barthel_evaluations': [evaluation.id for evaluation
                                    in barthel_evaluations],
            }

    def save_barthel(self):
        evaluations = self.barthel.barthel_evaluations
        for evaluation in evaluations:
            evaluation.patient = self.start.patient.id
            evaluation.save()

    def transition_barthel2psy_assessment(self):
        self.save_barthel()
        return 'psyc_assessment'

    def transition_barthel2lawton_brody(self):
        self.save_barthel()
        return 'lawton_brody'

    def default_lawton_brody(self, fields):
        pool = Pool()
        patient = self.start.patient
        LawtonBrody = pool.get('gnuhealth.lawton_brody_evaluation')
        lawton_brody_evaluations = LawtonBrody.search([
                                    ('patient', '=', patient.id)
                                    ])

        lejeune_attention = self.start.lejeune_attention
        patient = self.start.patient
        patient_domain = patient

        return {
            'lejeune_attention': lejeune_attention.id,
            'patient': patient.id,
            'patient_domain': patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'lawton_brody_evaluations': [evaluation.id for evaluation
                                         in lawton_brody_evaluations],
            }

    def save_lawton_brody(self):
        evaluations = self.lawton_brody.lawton_brody_evaluations
        for evaluation in evaluations:
            evaluation.patient = self.start.patient.id
            evaluation.save()

    def transition_lawton_brody2barthel(self):
        self.save_lawton_brody()
        return 'barthel'

    def transition_lawton_brody2behaviour(self):
        self.save_lawton_brody()
        return 'behaviour'

    def default_behaviour(self, fields):
        pool = Pool()
        Family = pool.get('gnuhealth.family')
        Members = pool.get('gnuhealth.family_member')

        lejeune_attention = self.start.lejeune_attention
        patient = self.start.patient
        families = Family.search([
                ('members.party', '=', patient.name.id)
                ])
        accompanying_persons = [ap.id for ap in lejeune_attention.accompanying_persons]
        members_domain = Members.search([
                ('name', 'in', [f.id for f in families]),
                ('party.id', '!=', patient.name.id)
                ])

        return {
            'lejeune_attention': lejeune_attention.id,
            'patient': patient.id,
            'patient_domain': patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'behaviour': lejeune_attention.behaviour,
            'accompanying_persons': accompanying_persons,
            'accompanying_persons_domain': [m.id for m
                                        in members_domain]
            }

    def save_behaviour(self):
        lejeune_attention = self.behaviour.lejeune_attention
        lejeune_attention.behaviour = self.behaviour.behaviour
        lejeune_attention.accompanying_persons = \
            [ap.id for ap in self.behaviour.accompanying_persons]
        lejeune_attention.accompanying_persons_domain = \
            [ap.id for ap in self.behaviour.accompanying_persons_domain]
        lejeune_attention.save()

    def transition_behaviour2lawton_brody(self):
        self.save_behaviour()
        return 'lawton_brody'

    def transition_behaviour2requests_suggestions(self):
        self.save_behaviour()
        return 'requests_suggestions'

    def default_requests_suggestions(self, fields):
        pool = Pool()

        lejeune_attention = self.start.lejeune_attention
        patient = self.start.patient
        patient_domain = patient

        return {
            'lejeune_attention': lejeune_attention.id,
            'patient': patient.id,
            'patient_domain': patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'requests': lejeune_attention.requests,
            'suggestions': lejeune_attention.suggestions,
            }

    def save_reqs_suggs(self):
        lejeune_attention = self.requests_suggestions.lejeune_attention
        lejeune_attention.requests = self.requests_suggestions.requests
        lejeune_attention.suggestions = self.requests_suggestions.suggestions
        lejeune_attention.save()

    def transition_reqs_suggs2behaviour(self):
        self.save_reqs_suggs()
        return 'behaviour'

    def transition_reqs_suggs2laboratory(self):
        self.save_reqs_suggs()
        return 'laboratory'

    def default_laboratory(self, fields):
        pool = Pool()
        LabRequests = pool.get('gnuhealth.patient.lab.test')
        LabResults = pool.get('gnuhealth.lab')

        patient = self.start.patient
        patient_domain = patient
        lejeune_attention = self.start.lejeune_attention
        requests = LabRequests.search([('patient_id', '=', patient.id)])
        results = LabResults.search([('patient', '=', patient.id)])
        return {
            'lejeune_attention': lejeune_attention.id,
            'patient': patient.id,
            'patient_domain': patient.id,
            'state': 'signed' if lejeune_attention.state == 'signed' else '',
            'lab_requests': [req.id for req in requests],
            'lab_results': [res.id for res in results],
            }

    def save_laboratory(self):
        patient = self.start.patient
        for req in self.laboratory.lab_requests:
            req.name = patient.id
            req.save()
        for res  in self.laboratory.lab_results:
            res.patient = patient.id
            res.save()

    def transition_lab2reqs_sugs(self):
        self.save_laboratory()
        return 'requests_suggestions'

    def transition_lab2end_sign(self):
        pool = Pool()
        Barthel = pool.get('gnuhealth.barthel_evaluation')
        LawtonBrody = pool.get('gnuhealth.lawton_brody_evaluation')
        LejeuneAttention = pool.get('gnuhealth.trisomy21.lejeune_attention')

        try:
            barthel_ev, = Barthel.search([
                ('patient', '=', self.start.patient.id)
                ], order=[('create_date','DESC')], limit=1)
            if barthel_ev.state != 'signed':
                Barthel.end_evaluation([barthel_ev])
                Barthel.sign_evaluation([barthel_ev])
        except:
            pass

        try:
            lawton_brody_ev, = LawtonBrody.search([
                ('patient', '=', self.start.patient.id)
                ], order=[('create_date','DESC')], limit=1)
            if lawton_brody_ev and lawton_brody_ev.state != 'signed':
                LawtonBrody.end_evaluation([lawton_brody_ev])
                LawtonBrody.sign_evaluation([lawton_brody_ev])
        except:
            pass

        if self.start.lejeune_attention.state != 'signed':
            LejeuneAttention.end_evaluation([self.start.lejeune_attention])
            LejeuneAttention.sign_evaluation([self.start.lejeune_attention])

        self.save_laboratory()
        return 'end_sign'

    def do_end_sign(self, action):
        data = {'res_id': [self.start.lejeune_attention.id]}
        action['views'].reverse()
        return action, data
