from trytond.wizard import (Wizard, StateAction)
from trytond.transaction import Transaction
from trytond.pool import Pool


class ShortcutPatientFamily(Wizard):
    'Create Patient - Wizard'
    __name__ = 'gnuhealth.shortcut_patient_family.wizard'

    start = StateAction('health.gnuhealth_action_family')

    def do_start(self, action):
        pool = Pool()
        Patient = pool.get('gnuhealth.patient')
        Family = pool.get('gnuhealth.family')

        patient_ids = Transaction().context.get('active_ids')
        patients = Patient.search([('id', 'in', patient_ids)])

        party_ids = [patient.name.id for patient in patients]

        families = Family.search([('members.party.id', 'in', party_ids)])
        family_ids = [family.id for family in families]

        data = {'res_id': family_ids}
        if len(family_ids) == 1:
            action['views'].reverse()
        return action, data
