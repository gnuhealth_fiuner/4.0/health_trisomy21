from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.i18n import gettext
from trytond.modules.health.core import get_institution
from trytond.modules.health_calendar.exceptions import (NoCompanyTimezone,
            EndDateBeforeStart, PeriodTooLong)

from datetime import timedelta, datetime, time
import pytz


class CreateAppointmentStart(metaclass=PoolMeta):
    __name__ = 'gnuhealth.calendar.create.appointment.start'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        # insert the visit_type values on the first
        cls.visit_type.selection.insert(1,('first_consultation', 'First consultation'))
        cls.visit_type.selection.insert(2,('second_consultation', 'Second consultation'))
        cls.visit_type.selection.insert(4,('referral', 'Referral'))
        cls.visit_type.selection[cls.visit_type.selection.index(
            ('followup', 'Followup'))], \
            cls.visit_type.selection[3] = \
                cls.visit_type.selection[3], \
                cls.visit_type.selection[cls.visit_type.selection.index(
                    ('followup', 'Followup'))]
        # remove options from visit_type
        # remove options from visit_type
        cls.visit_type.selection.remove(('well_child', 'Well Child visit'))
        cls.visit_type.selection.remove(('well_woman', 'Well Woman visit'))
        cls.visit_type.selection.remove(('well_man', 'Well Man visit'))

    @staticmethod
    def default_visit_type():
        return 'followup'


class CreateAppointment(metaclass=PoolMeta):
    __name__ = 'gnuhealth.calendar.create.appointment'

    def transition_create_(self):
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        Company = pool.get('company.company')

        timezone = None
        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)
            if company.timezone:
                timezone = pytz.timezone(company.timezone)
            else:
                raise NoCompanyTimezone(
                    gettext('health_calendar.no_company_timezone')
                        )

        appointments = []

        # Iterate over days
        day_count = (self.start.date_end - self.start.date_start).days + 1

        # Validate dates
        if (self.start.date_start and self.start.date_end):
            if (self.start.date_end < self.start.date_start):
                raise EndDateBeforeStart(
                    gettext('health_calendar.msg_end_before_start')
                    )

            if (day_count > 367):
                raise PeriodTooLong(
                    gettext('health_trisomy21.msg_period_too_long')
                    )

        for single_date in (
            self.start.date_start + timedelta(n)
                for n in range(day_count)):
            if ((single_date.weekday() == 0 and self.start.monday)
                or (single_date.weekday() == 1 and self.start.tuesday)
                or (single_date.weekday() == 2 and self.start.wednesday)
                or (single_date.weekday() == 3 and self.start.thursday)
                or (single_date.weekday() == 4 and self.start.friday)
                or (single_date.weekday() == 5 and self.start.saturday)
                    or (single_date.weekday() == 6 and self.start.sunday)):
                # Iterate over time
                dt = datetime.combine(
                    single_date, self.start.time_start)
                dt = timezone.localize(dt)
                dt = dt.astimezone(pytz.utc)
                dt_end = datetime.combine(
                    single_date, self.start.time_end)
                dt_end = timezone.localize(dt_end)
                dt_end = dt_end.astimezone(pytz.utc)
                while dt < dt_end:
                    appointment = {
                        'healthprof': self.start.healthprof.id,
                        'speciality': self.start.specialty.id,
                        'institution': self.start.institution.id,
                        'appointment_date': dt,
                        'appointment_date_end': dt +
                        timedelta(minutes=self.start.appointment_minutes),
                        'state': 'free',
                        }
                    appointments.append(appointment)
                    dt += timedelta(minutes=self.start.appointment_minutes)
        if appointments:
            Appointment.create(appointments)
        return 'open_'
