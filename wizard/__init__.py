from . import wizard_health_calendar
from . import wizard_create_patient
from . import wizard_appointment_report
from . import wizard_create_lejeune_attention
from . import wizard_shortcut_patient_family
