from trytond.model import ModelView, fields
from trytond.wizard import (Wizard, Button,
                    StateView, StateAction, StateTransition)
from trytond.pyson import Eval

from datetime import date


class AppointmentReportStart(ModelView):
    'Appointment Report - Start'
    __name__ = 'gnuhealth.trisomy21.appointment_report.start'

    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required = True,
        domain=[('end_date', '>', Eval('start_date'))])


    @staticmethod
    def default_start_date():
        return date.today().replace(day=1)


class AppointmentReportWizard(Wizard):
    'Appointment Report - Wizard'
    __name__ = 'gnuhealth.trisomy21.appointment_report.wizard'

    start = StateView('gnuhealth.trisomy21.appointment_report.start',
            'health_trisomy21.appointment_report_view_form', [
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Open', 'open_', 'tryton-ok', default=True)
                ])

    open_ = StateAction('health_trisomy21.act_appointment_report')

    def fill_data(self):
        return {
            'start_date': self.start.start_date,
            'end_date': self.start.end_date
            }

    def do_open_(self, action):
        return action, self.fill_data()

    def transition_open_(self):
        return 'end'
