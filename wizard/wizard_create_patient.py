from trytond.model import ModelView, fields
from trytond.wizard import (Wizard, Button, StateView, StateAction,
                StateTransition)
from trytond.pool import Pool
from trytond.exceptions import UserError
from trytond.pyson import Eval

import re
from datetime import datetime


class CreatePatientStart(ModelView):
    'Create Patient - Start'
    __name__ = 'gnuhealth.trisomy21.create_patient.start'

    qr = fields.Char('QR')
    name = fields.Char('Name', required=True)
    lastname = fields.Char('Lastname', required=True)
    puid = fields.Char('PUID', required=True)
    dob = fields.Date('dob',help='Date of Birth', required=True)
    gender = fields.Selection([
        ('m', 'Male'),
        ('f', 'Female'),
        #('f-m', 'Female -> Male'),
        #('m--f', 'Male -> Female'),
        ], 'Gender', sort=False, required=True)

    role = fields.Selection([
        ('main_patient', 'Main patient'),
        ('mother', 'Mother'),
        ('father', 'Father'),
        ('sister', 'Sister'),
        ('brother', 'Brother'),
        ('grandmother', 'Grandmother'),
        ('grandfather', 'Grandfather'),
        ('aunt', 'Aunt'),
        ('uncle', 'Uncle'),
        ('cousin', 'Cousin'),
        ('niece', 'Niece'),
        ('nephew', 'Nephew'),
        ('other', 'Other')
        ], 'Role', sort=False, required=True)
    role_string = role.translated('role')
    family = fields.Many2One('gnuhealth.family', 'Family', required=True)
    family_members = fields.Many2Many('gnuhealth.family_member', None, None,
                'Family members', readonly=True)

    create_du = fields.Selection([
        ('create','Create'),
        ('not_create', 'Use a created one'),
        ], 'DU', sort=False, required=True)
    address_street= fields.Char('Street',
            states={'invisible': Eval('create_du') != 'create'})
    address_country = fields.Many2One('country.country', 'country',
            states={'invisible': Eval('create_du') != 'create'})
    address_subdivision = fields.Many2One('country.subdivision', 'Subdivision',
            domain=[('country','=', Eval('address_country'))],
            depends=[('address_country')],
            states={'invisible': Eval('create_du') != 'create'})
    address_city = fields.Char('City',
            states={'invisible': Eval('create_du') != 'create'})
    address_zip = fields.Char('Zip',
            states={'invisible': Eval('create_du') != 'create'})
    du = fields.Many2One('gnuhealth.du', 'DU',
            states={'invisible': Eval('create_du') != 'not_create'})

    phone = fields.Char('Phone')
    email = fields.Char('E-mail')

    citizenship = fields.Many2One('country.country', 'Citizenship')
    residence = fields.Many2One('country.country', 'Residence')

    @fields.depends('family')
    def on_change_with_family_members(self, name=None):
        if self.family and self.family.members:
            return [member.id for member in self.family.members]
        return None

    @fields.depends('qr')
    def on_change_qr(self, name=None):
        '''
        Example ID to be parsed:
        00305133441"MIAPELLIDO"MI NOMBRE"M"20123456"A"20-12-1950"02-10-2014"207
        '''
        code = self.qr
        if len(code) > 60:
            elements = re.split(r'"|\@|\*', code)
            self.lastname = elements[1]
            self.name = elements[2]
            self.puid = elements[4]
            self.gender = elements[3].lower() if elements[3].lower() in ['m','f'] else ''
            try:
                self.dob = datetime.strptime(elements[6], '%d-%m-%Y')
            except:
                self.dob = datetime.strptime(elements[6], '%d/%m/%Y')
            self.qr = None

    @staticmethod
    def default_create_du():
        return 'create'

class CreatePatientWizard(Wizard):
    'Create Patient - Wizard'
    __name__ = 'gnuhealth.trisomy21.create_patient.wizard'

    start = StateView(
        'gnuhealth.trisomy21.create_patient.start',
        'health_trisomy21.create_patient_start',[
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'create_patient', 'tryton-ok', default=True),
            Button('Create and return', 'create_return', 'tryton-back')
        ])

    create_patient = StateAction('health.action_gnuhealth_patient_view')

    create_return = StateTransition()

    def default_start(self, fields):
        res = {}
        if hasattr(self.start, 'family'):
            family = self.start.family
            res['family'] = family.id
            res['family_members'] = [member.id for member in family.members]
        if hasattr(self.start, 'du'):
            res['create_du'] = 'not_create'
        if hasattr(self.start, 'du'):
            res['du'] = self.start.du.id
        return res

    def _create_patient(self):
        pool = Pool()
        Party = pool.get('party.party')
        DU = pool.get('gnuhealth.du')
        Address = pool.get('party.address')
        Patient = pool.get('gnuhealth.patient')
        Member = pool.get('gnuhealth.family_member')

        party = Party.search([
            'OR',
            ('ref', '=', self.start.puid),
            ('ref', '=', self.start.puid + self.start.gender)
            ])
        if party:
            raise UserError('error')
        party = Party()
        party.is_person = True
        party.is_patient = True
        party.ref = self.start.puid
        party.name = self.start.name
        party.lastname = self.start.lastname
        party.gender = self.start.gender
        party.dob = self.start.dob
        party.fed_country = Party().default_fed_country()
        party.residence = self.start.residence and \
            self.start.residence.id
        party.citizenship = self.start.citizenship and \
            self.start.citizenship.id

        if self.start.create_du == 'not_create':
            party.du = self.start.du.id
        else:
            du = DU()
            du.name = '[' + ', '.join([
                self.start.puid, self.start.lastname, self.start.name]) + \
                '] ' + \
                self.start.address_street + ' ' + \
                self.start.address_city
            du.address_street = self.start.address_street
            du.address_city = self.start.address_city
            du.address_country = self.start.address_country and \
                self.start.address_country.id
            du.address_subdivision = self.start.address_subdivision and \
                self.start.address_subdivision.id
            du.address_zip = self.start.address_zip
            du.save()
            party.du = du.id
        party.save()
        self.start.du = party.du.id

        member = Member()
        member.role = self.start.role_string
        member.name = self.start.family.id
        member.party = party.id
        member.save()

        address = Address()
        address.street = self.start.address_street
        address.postal_code = self.start.address_zip
        address.country = self.start.address_country and \
            self.start.address_country.id
        address.subdivision = self.start.address_subdivision and \
            self.start.address_subdivision.id
        address.city = self.start.address_city
        address.party = party.id
        address.save()

        patient = Patient()
        patient.name = party.id
        patient.email = self.start.email
        patient.phone = self.start.phone
        patient.save()

    def do_create_patient(self, action):
        pool = Pool()
        Patient = pool.get('gnuhealth.patient')
        self._create_patient()

        data = {}
        patients = Patient.search([
            ('name', 'in', [member.party.id for member in self.start.family.members])])
        data = {'res_id': [p.id for p in patients]}
        if len(patients) == 1:
            action['views'].reverse()
        return action, data

    def transition_create_return(self):
        self._create_patient()
        return 'start'
