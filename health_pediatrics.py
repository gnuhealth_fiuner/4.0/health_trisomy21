from trytond.model import fields, ModelView, ModelSQL
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from .health_trisomy21 import Evaluation


class Newborn(metaclass=PoolMeta):
    __name__ = 'gnuhealth.newborn'

    STATES = {'readonly': Eval('state') == 'signed'}
    controlled_pregnancy = fields.Boolean('Controlled pregnancy', states=STATES)
    controlled_pregnancy_observations = fields.Text('Observations', states=STATES)
    ft_screening = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'First trimester screening', sort=False,
        states=STATES)
    ft_screening_check = fields.Boolean('First trimester screening normal',
            states={
                'invisible': Eval('ft_screening') == 'no',
                'readonly': STATES['readonly']
                })
    ft_screening_check_observations = fields.Text('Ft. screening - observations', states=STATES)
    caesarean = fields.Boolean('Caesarean birth',
        states=STATES)
    test_metabolic_observations = fields.Text('Test metabolic - observations', states=STATES)
    def get_rec_name(self, name):
        if self.patient:
            return self.patient.rec_name
        return None

    @staticmethod
    def default_ft_screening():
        return 'no'


class NeonatalControl(ModelView, ModelSQL, Evaluation):
    'Neonatal Control'
    __name__ = 'gnuhealth.neonatal_control'

    name = fields.Char('Neonatal control ID')
    patient = fields.Many2One('gnuhealth.patient', 'Patient')
    internment = fields.Text('Internment/hospitalization')
    maternal_lactancy_from = fields.Integer('Maternal lactancy from',
                        help='Age expresed in years')
    maternal_lactancy_until = fields.Integer('Maternal lactancy until',
                        help='Age expresed in years')
    control_15_days = fields.Text('15 days life control')
    control_30_days = fields.Text('30 days life control')

