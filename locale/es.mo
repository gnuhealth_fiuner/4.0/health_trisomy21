#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:gnuhealth.appointment,confirm_date:"
msgid "Confirm date"
msgstr "Fecha de confirmación"

msgctxt "field:gnuhealth.appointment,doc_submitted:"
msgid "Documentation submitted"
msgstr "Documentación enviada"

msgctxt "field:gnuhealth.appointment,doc_submitted_date:"
msgid "Date of submitted documentation"
msgstr "Fecha de envío de la documentación"

msgctxt "field:gnuhealth.appointment,email:"
msgid "Email"
msgstr "Email"

msgctxt "field:gnuhealth.appointment,referred_hp:"
msgid "Referred Health Prof"
msgstr "Profesional que deriva"

msgctxt "field:gnuhealth.appointment,referred_hp_speciality:"
msgid "Referred Speciality"
msgstr "Especialidad derivada"

msgctxt "field:gnuhealth.barthel_evaluation,appointment:"
msgid "Appointment"
msgstr "Turno"

msgctxt "field:gnuhealth.barthel_evaluation,bathe:"
msgid "Bathe"
msgstr "Baño"

msgctxt "field:gnuhealth.barthel_evaluation,climb_stairs:"
msgid "Climb Stairs"
msgstr "Subir escaleras"

msgctxt "field:gnuhealth.barthel_evaluation,code:"
msgid "Code"
msgstr "Código"

msgctxt "field:gnuhealth.barthel_evaluation,computed_age:"
msgid "Patient age at evaluation"
msgstr "Edad del paciente al momento de la evaluación"

msgctxt "field:gnuhealth.barthel_evaluation,depositions:"
msgid "Depositions"
msgstr "Deposiciones"

msgctxt "field:gnuhealth.barthel_evaluation,digest_current:"
msgid "Current Hash"
msgstr "Hash Actual"

msgctxt "field:gnuhealth.barthel_evaluation,digest_status:"
msgid "Altered"
msgstr "Alterado"

msgctxt "field:gnuhealth.barthel_evaluation,digital_signature:"
msgid "Digital Signature"
msgstr "Firma Digital"

msgctxt "field:gnuhealth.barthel_evaluation,document_digest:"
msgid "Digest"
msgstr "Resumen"

msgctxt "field:gnuhealth.barthel_evaluation,dressing:"
msgid "Dressing"
msgstr "Vestirse"

msgctxt "field:gnuhealth.barthel_evaluation,eating:"
msgid "Eating"
msgstr "Comer"

msgctxt "field:gnuhealth.barthel_evaluation,evaluation_endtime:"
msgid "End"
msgstr "Finalizar"

msgctxt "field:gnuhealth.barthel_evaluation,evaluation_length:"
msgid "Evaluation length"
msgstr "Longitud de la evaluación"

msgctxt "field:gnuhealth.barthel_evaluation,evaluation_start:"
msgid "Start"
msgstr "Inicio"

msgctxt "field:gnuhealth.barthel_evaluation,gender:"
msgid "Gender"
msgstr "Sexo"

msgctxt "field:gnuhealth.barthel_evaluation,healthprof:"
msgid "Health Prof"
msgstr "Prof.  Salud"

msgctxt "field:gnuhealth.barthel_evaluation,hygiene:"
msgid "Hygiene"
msgstr "Higiene"

msgctxt "field:gnuhealth.barthel_evaluation,information_source:"
msgid "Source"
msgstr "Original"

msgctxt "field:gnuhealth.barthel_evaluation,moving:"
msgid "Moving"
msgstr "Moverse"

msgctxt "field:gnuhealth.barthel_evaluation,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt "field:gnuhealth.barthel_evaluation,score:"
msgid "Score"
msgstr "Puntaje"

msgctxt "field:gnuhealth.barthel_evaluation,score_dependency:"
msgid "Score dependency"
msgstr "Puntaje de dependencia"

msgctxt "field:gnuhealth.barthel_evaluation,serializer:"
msgid "Doc String"
msgstr ""

msgctxt "field:gnuhealth.barthel_evaluation,serializer_current:"
msgid "Current Hash"
msgstr "Hash Actual"

msgctxt "field:gnuhealth.barthel_evaluation,signed_by:"
msgid "Signed by - Health Prof"
msgstr "Firmado por"

msgctxt "field:gnuhealth.barthel_evaluation,specialty:"
msgid "Specialty"
msgstr "Especialidad"

msgctxt "field:gnuhealth.barthel_evaluation,state:"
msgid "State"
msgstr "Estado"

msgctxt "field:gnuhealth.barthel_evaluation,toilet_use:"
msgid "Toilet use"
msgstr "Uso del baño"

msgctxt "field:gnuhealth.barthel_evaluation,urination:"
msgid "Urination"
msgstr "Micción"

msgctxt "field:gnuhealth.barthel_evaluation,wait_time:"
msgid "Patient wait time"
msgstr "Tiempo de espera del paciente"

msgctxt "field:gnuhealth.barthel_evaluation,wander:"
msgid "Wander"
msgstr "Deambular"

msgctxt "field:gnuhealth.lawton_brody_evaluation,appointment:"
msgid "Appointment"
msgstr "Turno"

msgctxt "field:gnuhealth.lawton_brody_evaluation,code:"
msgid "Code"
msgstr "Código"

msgctxt "field:gnuhealth.lawton_brody_evaluation,computed_age:"
msgid "Patient age at evaluation"
msgstr "Edad del paciente al momento de la evaluación"

msgctxt "field:gnuhealth.lawton_brody_evaluation,cooking:"
msgid "Cooking"
msgstr "Cocinar"

msgctxt "field:gnuhealth.lawton_brody_evaluation,digest_current:"
msgid "Current Hash"
msgstr "Hash Actual"

msgctxt "field:gnuhealth.lawton_brody_evaluation,digest_status:"
msgid "Altered"
msgstr "Alterado"

msgctxt "field:gnuhealth.lawton_brody_evaluation,digital_signature:"
msgid "Digital Signature"
msgstr "Firma Digital"

msgctxt "field:gnuhealth.lawton_brody_evaluation,document_digest:"
msgid "Digest"
msgstr "Resumen"

msgctxt "field:gnuhealth.lawton_brody_evaluation,evaluation_endtime:"
msgid "End"
msgstr "Finalizar"

msgctxt "field:gnuhealth.lawton_brody_evaluation,evaluation_length:"
msgid "Evaluation length"
msgstr "Longitud de la evaluación"

msgctxt "field:gnuhealth.lawton_brody_evaluation,evaluation_start:"
msgid "Start"
msgstr "Inicio"

msgctxt "field:gnuhealth.lawton_brody_evaluation,gender:"
msgid "Gender"
msgstr "Sexo"

msgctxt "field:gnuhealth.lawton_brody_evaluation,healthprof:"
msgid "Health Prof"
msgstr "Prof.  Salud"

msgctxt "field:gnuhealth.lawton_brody_evaluation,home_care:"
msgid "Home care"
msgstr "Cuidar la casa"

msgctxt "field:gnuhealth.lawton_brody_evaluation,information_source:"
msgid "Source"
msgstr "Original"

msgctxt "field:gnuhealth.lawton_brody_evaluation,laundry:"
msgid "Laundry"
msgstr "Lavar la ropa"

msgctxt "field:gnuhealth.lawton_brody_evaluation,liability_for_medication:"
msgid "Liability for medication"
msgstr "Responsabilidad sobre la medicación"

msgctxt "field:gnuhealth.lawton_brody_evaluation,money_handling:"
msgid "Money handling"
msgstr "Capacidad de utilizar dinero"

msgctxt "field:gnuhealth.lawton_brody_evaluation,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt "field:gnuhealth.lawton_brody_evaluation,phone_use:"
msgid "Phone use"
msgstr "Uso del teléfono"

msgctxt "field:gnuhealth.lawton_brody_evaluation,score:"
msgid "Score"
msgstr "Puntaje"

msgctxt "field:gnuhealth.lawton_brody_evaluation,score_dependency:"
msgid "Score dependency"
msgstr "Dependencia de puntaje"

msgctxt "field:gnuhealth.lawton_brody_evaluation,serializer:"
msgid "Doc String"
msgstr ""

msgctxt "field:gnuhealth.lawton_brody_evaluation,serializer_current:"
msgid "Current Hash"
msgstr "Hash Actual"

msgctxt "field:gnuhealth.lawton_brody_evaluation,shopping:"
msgid "Shopping"
msgstr "Ir de compras"

msgctxt "field:gnuhealth.lawton_brody_evaluation,signed_by:"
msgid "Signed by - Health Prof"
msgstr "Firmado por"

msgctxt "field:gnuhealth.lawton_brody_evaluation,specialty:"
msgid "Specialty"
msgstr "Especialidad"

msgctxt "field:gnuhealth.lawton_brody_evaluation,state:"
msgid "State"
msgstr "Estado"

msgctxt "field:gnuhealth.lawton_brody_evaluation,use_of_transportation:"
msgid "Use of transportation"
msgstr "Uso del transporte"

msgctxt "field:gnuhealth.lawton_brody_evaluation,wait_time:"
msgid "Patient wait time"
msgstr "Tiempo de espera del paciente"

msgctxt "field:gnuhealth.neonatal_control,appointment:"
msgid "Appointment"
msgstr "Turno"

msgctxt "field:gnuhealth.neonatal_control,code:"
msgid "Code"
msgstr "Código"

msgctxt "field:gnuhealth.neonatal_control,computed_age:"
msgid "Patient age at evaluation"
msgstr "Edad del paciente al momento de la evaluación"

msgctxt "field:gnuhealth.neonatal_control,control_15_days:"
msgid "15 days life control"
msgstr "Control a los 15 días de vida"

msgctxt "field:gnuhealth.neonatal_control,control_30_days:"
msgid "30 days life control"
msgstr "Control a los 30 días de vida"

msgctxt "field:gnuhealth.neonatal_control,digest_current:"
msgid "Current Hash"
msgstr "Hash Actual"

msgctxt "field:gnuhealth.neonatal_control,digest_status:"
msgid "Altered"
msgstr "Alterado"

msgctxt "field:gnuhealth.neonatal_control,digital_signature:"
msgid "Digital Signature"
msgstr "Firma Digital"

msgctxt "field:gnuhealth.neonatal_control,document_digest:"
msgid "Digest"
msgstr "Resumen"

msgctxt "field:gnuhealth.neonatal_control,evaluation_endtime:"
msgid "End"
msgstr "Finalizar"

msgctxt "field:gnuhealth.neonatal_control,evaluation_length:"
msgid "Evaluation length"
msgstr "Longitud de la evaluación"

msgctxt "field:gnuhealth.neonatal_control,evaluation_start:"
msgid "Start"
msgstr "Inicio"

msgctxt "field:gnuhealth.neonatal_control,gender:"
msgid "Gender"
msgstr "Sexo"

msgctxt "field:gnuhealth.neonatal_control,healthprof:"
msgid "Health Prof"
msgstr "Prof.  Salud"

msgctxt "field:gnuhealth.neonatal_control,information_source:"
msgid "Source"
msgstr "Original"

msgctxt "field:gnuhealth.neonatal_control,internment:"
msgid "Internment/hospitalization"
msgstr "Internado/Hospitalizado"

msgctxt "field:gnuhealth.neonatal_control,maternal_lactancy_from:"
msgid "Maternal lactancy from"
msgstr "Lactancia materna desde"

msgctxt "field:gnuhealth.neonatal_control,maternal_lactancy_until:"
msgid "Maternal lactancy until"
msgstr "Lactancia materna hasta"

msgctxt "field:gnuhealth.neonatal_control,name:"
msgid "Neonatal control ID"
msgstr "Neoonato - ID de control"

msgctxt "field:gnuhealth.neonatal_control,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt "field:gnuhealth.neonatal_control,score:"
msgid "Score"
msgstr "Puntaje"

msgctxt "field:gnuhealth.neonatal_control,serializer:"
msgid "Doc String"
msgstr ""

msgctxt "field:gnuhealth.neonatal_control,serializer_current:"
msgid "Current Hash"
msgstr "Hash Actual"

msgctxt "field:gnuhealth.neonatal_control,signed_by:"
msgid "Signed by - Health Prof"
msgstr "Firmado por"

msgctxt "field:gnuhealth.neonatal_control,specialty:"
msgid "Specialty"
msgstr "Especialidad"

msgctxt "field:gnuhealth.neonatal_control,state:"
msgid "State"
msgstr "Estado"

msgctxt "field:gnuhealth.neonatal_control,wait_time:"
msgid "Patient wait time"
msgstr "Tiempo de espera del paciente"

msgctxt "field:gnuhealth.newborn,caesarean:"
msgid "Caesarean birth"
msgstr "Nacimiento por cesárea"

msgctxt "field:gnuhealth.newborn,controlled_pregnancy:"
msgid "Controlled pregnancy"
msgstr "Embarazo controlado"

msgctxt "field:gnuhealth.newborn,controlled_pregnancy_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt "field:gnuhealth.newborn,ft_screening:"
msgid "First trimester screening"
msgstr "Screening del primer trimestre"

msgctxt "field:gnuhealth.newborn,ft_screening_check:"
msgid "First trimester screening normal"
msgstr "Screening del primer trimestre normal"

msgctxt "field:gnuhealth.newborn,ft_screening_check_observations:"
msgid "Ft. screening - observations"
msgstr "Screening de primer trimestre - observaciones"

msgctxt "field:gnuhealth.newborn,test_metabolic_observations:"
msgid "Test metabolic - observations"
msgstr "Test metábolico - observaciones"

msgctxt "field:gnuhealth.patient,age_wich_sat:"
msgid "Age at which he/she sat down"
msgstr "Edad a la que se sentó"

msgctxt "field:gnuhealth.patient,age_wich_talked:"
msgid "Age at which he/she started to talk"
msgstr "Edad a la que comenzó a hablar"

msgctxt "field:gnuhealth.patient,age_wich_walked:"
msgid "Age at which he/she started to walk"
msgstr "Edad a la que comenzó a caminar"

msgctxt "field:gnuhealth.patient,andro_depression:"
msgid "Depression"
msgstr "Depresión"

msgctxt "field:gnuhealth.patient,andro_fatigue:"
msgid "Fatigue"
msgstr "Fatiga"

msgctxt "field:gnuhealth.patient,andro_headache:"
msgid "Headache"
msgstr "Cefalea (Dolor de cabeza)"

msgctxt "field:gnuhealth.patient,andro_irritability:"
msgid "Irritability"
msgstr "Irritabilidad"

msgctxt "field:gnuhealth.patient,andro_mood_changes:"
msgid "Mood changes"
msgstr "Cambios de humor"

msgctxt "field:gnuhealth.patient,andro_sweat:"
msgid "Sweat"
msgstr "Sudor"

msgctxt "field:gnuhealth.patient,andro_weight_inc:"
msgid "Weight increment"
msgstr "Incremento de peso"

msgctxt "field:gnuhealth.patient,annual_testicular_control:"
msgid "Annual testicular control"
msgstr "Control  anual  testicular"

msgctxt "field:gnuhealth.patient,annual_testicular_us:"
msgid "Annual testicular ultrasound"
msgstr "Ultrasonido testicular anual"

msgctxt "field:gnuhealth.patient,dementia_assessment:"
msgid "Dementia assessment"
msgstr "Evaluación de la demencia"

msgctxt "field:gnuhealth.patient,email:"
msgid "Email"
msgstr "Email"

msgctxt "field:gnuhealth.patient,last_appointment_attended:"
msgid "Last appointment attended"
msgstr "Último turno atendido"

msgctxt "field:gnuhealth.patient,last_confirmed_appointment:"
msgid "Last confirmed appointment"
msgstr "Último turno confirmado"

msgctxt "field:gnuhealth.patient,neuropsyc_assessment:"
msgid "Neuropsychological assessment"
msgstr "Evaluación Neuropsiquiátrica"

msgctxt "field:gnuhealth.patient,next_appointment_confirmed:"
msgid "Next appointment confirmed"
msgstr "Próximo turno conrifmado"

msgctxt "field:gnuhealth.patient,next_appointment_to_confirm:"
msgid "Next appointment to confirm"
msgstr "Próximo turno a confirmar"

msgctxt "field:gnuhealth.patient,subsidized:"
msgid "Subsidized"
msgstr "Subsidiado"

msgctxt "field:gnuhealth.patient,subsidized_percentage:"
msgid "Subsidized percentage"
msgstr "Porcentaje de subsidio"

msgctxt "field:gnuhealth.patient.ecg,ecg_age:"
msgid "ECG for age"
msgstr "Edad al ECG"

msgctxt "field:gnuhealth.patient.evaluation,healthprof:"
msgid "Initiated by"
msgstr "Iniciada por"

msgctxt "field:gnuhealth.patient.evaluation,signed_by:"
msgid "Signed by"
msgstr "Firmado por"

msgctxt "field:gnuhealth.sequences,barthel_evaluation_sequence:"
msgid "Barthel Evaluation Sequence"
msgstr "Secuencia de la Evaluaciónde Barthel"

msgctxt "field:gnuhealth.sequences,lawton_brody_evaluation_sequence:"
msgid "Lawton - Brody Evaluation Sequence"
msgstr "Secuencia de la Evaluación de Lawton y Brody"

msgctxt "field:gnuhealth.sequences,lejeune_attention_sequence:"
msgid "Lejeune Attention Sequence"
msgstr "Secuencia de Atenciones Lejeune"

msgctxt ""
"field:gnuhealth.sequences.barthel_evaluation_sequence,barthel_evaluation_sequence:"
msgid "Barthel Evaluation Sequence"
msgstr "Secuencia de Evaluaciones de Barthel"

msgctxt ""
"field:gnuhealth.sequences.lawton_brody_evaluation_sequence,lawton_brody_evaluation_sequence:"
msgid "Lawton - Brody Evaluation Sequence"
msgstr "Secuencia de la Evaluación de Lawton y Brody"

msgctxt ""
"field:gnuhealth.sequences.lejeune_attention_sequence,lejeune_attention_sequence:"
msgid "Lejeune Attention Sequence"
msgstr "Secuencia de Atenciones Lejeune"

msgctxt "field:gnuhealth.surgical_history,health_institution:"
msgid "Health Institution"
msgstr "Institución de Salud"

msgctxt "field:gnuhealth.surgical_history,healthprof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.surgical_history,notes:"
msgid "Notes"
msgstr "Notas"

msgctxt "field:gnuhealth.surgical_history,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt "field:gnuhealth.surgical_history,procedures:"
msgid "Operation"
msgstr "Operación"

msgctxt "field:gnuhealth.surgical_history,surgery_date:"
msgid "Surgery date"
msgstr "Fecha de cirugía"

msgctxt "field:gnuhealth.surgical_history,surgery_kind:"
msgid "Surgery kind"
msgstr "Tipo de cirugía"

msgctxt "field:gnuhealth.surgical_history.operation,main_procedure:"
msgid "Main procedure"
msgstr "Procedimiento principal"

msgctxt "field:gnuhealth.surgical_history.operation,name:"
msgid "Surgical History"
msgstr "Historial de cirugías"

msgctxt "field:gnuhealth.surgical_history.operation,notes:"
msgid "Notes"
msgstr "Notas"

msgctxt "field:gnuhealth.surgical_history.operation,procedure:"
msgid "Code"
msgstr "Código"

msgctxt "field:gnuhealth.trisomy21.appointment_report.start,end_date:"
msgid "End Date"
msgstr "Fecha finalización"

msgctxt "field:gnuhealth.trisomy21.appointment_report.start,start_date:"
msgid "Start Date"
msgstr "Fecha Inicio"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,age_wich_sat:"
msgid "Age at which he/she sat down"
msgstr "Edad a la que se sentó"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,age_wich_talked:"
msgid "Age at which he/she started to talk"
msgstr "Edad a la que comenzó a hablar"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,age_wich_walked:"
msgid "Age at which he/she started to walk"
msgstr "Edad a la que comenzó a caminar"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,agenesia:"
msgid "Agenesia"
msgstr "Agenesia"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_depression:"
msgid "Depression"
msgstr "Depresión"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_fatigue:"
msgid "Fatigue"
msgstr "Fatiga"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_headache:"
msgid "Headache"
msgstr "Cefalea (Dolor de cabeza)"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_irritability:"
msgid "Irritability"
msgstr "Irritabilidad"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_mood_changes:"
msgid "Mood changes"
msgstr "Cambios de humor"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_sweat:"
msgid "Sweat"
msgstr "Sudor"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_weight_inc:"
msgid "Weight increment"
msgstr "Incremento de peso"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,annual_testicular_control:"
msgid "Annual testicular control"
msgstr "Control  anual  testicular"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,annual_testicular_us:"
msgid "Annual testicular ultrasound"
msgstr "Ultrasonido testicular anual"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,annual_tsh:"
msgid "Annual tsh"
msgstr "TSH anual"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,anticonceptive:"
msgid "Contraceptive Method"
msgstr "Método anticonceptivo"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,apnea:"
msgid "Apnea"
msgstr "Apnea"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,autoimmune_disease:"
msgid "Autoimmune disease"
msgstr "Enfermedad autoinmune"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,behaviour_disorder:"
msgid "Behaviour disorder"
msgstr "Trastorno de conducta"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,bone_densitometry:"
msgid "Bone densitometry"
msgstr "Densitometría osea"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,bowel_habit:"
msgid "Bowel habit"
msgstr "Hábito intestinal"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,cardiov_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,cardiov_healthprof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,carpal_tunnel_synd:"
msgid "Carpal tunnel syndrome"
msgstr "Síndrome del túnel carpiano"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,celiac_screening:"
msgid "Celiac screening"
msgstr "Screening Celiaquía"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,cephalic_support:"
msgid "Cephalic support"
msgstr "Soporte cefálico"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,cholesterol:"
msgid "Cholesterol"
msgstr "Colesterol"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,chronic_cervical_myel:"
msgid "Chronic cervical myelopathy"
msgstr "Mielopatía cervical crónica"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,conditions:"
msgid "Conditions"
msgstr "Condiciones"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,dementia_assessment:"
msgid "Dementia assessment"
msgstr "Evaluación de la demencia"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,dental_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,dental_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,dental_orthosis:"
msgid "Dental Orthesis"
msgstr "Ortesis dental"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,dental_orthosis_description:"
msgid "Dental orthosis description"
msgstr "Descripción de la ortesis dental"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,derma_health_inst:"
msgid "Institution"
msgstr "Institución"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,derma_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,derma_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,disab_assesments:"
msgid "Disabilities assessments"
msgstr "Evaluaciones de discapacidad"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,disab_health_inst:"
msgid "Institution"
msgstr "Institución"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,disab_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_dec_libido:"
msgid "Decreased libido"
msgstr "Líbido disminuido"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_headache:"
msgid "Headaches"
msgstr "Dolores de cabeza"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_irritability:"
msgid "Irritability and depresion periods"
msgstr "Irritabilidad y períodos de depresión"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_menstrual_change:"
msgid "Changes in the menstrual cycle"
msgstr "Cambios en el ciclo menstrual"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_mood_changes:"
msgid "Mood changes"
msgstr "Cambios de humor"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_night_sweats:"
msgid "Night sweats"
msgstr "Sudores nocturnos"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_suffocation:"
msgid "Suffocation"
msgstr "Asfixia"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,ecgs:"
msgid "ECG"
msgstr "ECG"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,endoc_health_inst:"
msgid "Institution"
msgstr "Institución"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,endoc_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,fecal_occult_blood:"
msgid "Fecal occult blood"
msgstr "Materia fecal oculta en sangre"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,feeding:"
msgid "Feeding"
msgstr "Alimentación"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,feeding_intolerance:"
msgid "Feeding intolerance"
msgstr "Intolerancia alimentaria"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,feeding_intolerance_description:"
msgid "Feeding intolerance"
msgstr "Intolerancia alimentaria"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,fert_health_inst:"
msgid "Institution"
msgstr "Institución"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,fert_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,flatfoot:"
msgid "Flatfoot"
msgstr "Pie plano"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,gastroint_health_inst:"
msgid "Health Institution"
msgstr "Institución de Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,gastroint_health_prof:"
msgid "Health Professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,gastroint_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,gender:"
msgid "Gender"
msgstr "Sexo"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,gerd_symptoms:"
msgid "GERD symptoms"
msgstr "Síntomas de ERGE"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,glycemia:"
msgid "Glycemia"
msgstr "Glucemia"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,gyneco_health_inst:"
msgid "Health Institution"
msgstr "Institución de Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,gyneco_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,hla_dq2_dq8:"
msgid "HLA-DQ2-DQ8"
msgstr "HLA-DQ2-DQ8"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,hospitalizations:"
msgid "Hospitalizations"
msgstr "Internaciones/Hospitalizaciones"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,hospitalizations_description:"
msgid "Hospitalizations"
msgstr "Internaciones/Hospitalizaciones"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,imaging_results:"
msgid "Images"
msgstr "Imágenes"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,last_audiometry:"
msgid "Last audiometry"
msgstr "Última audiometría"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,last_biannual_ophthalm_assessment:"
msgid "Last biannual ophthalmologic assessment"
msgstr "Ultima evaluación oftalmológica bianual"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,last_dental_control:"
msgid "Last dental control"
msgstr "Último control  dental"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,late_dentition:"
msgid "Late dentition"
msgstr "Dentición tardía"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,menarche:"
msgid "Menarche"
msgstr "Edad de menarca"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,menopause:"
msgid "Menopause"
msgstr "Edad de menopausia"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,menstrual_history:"
msgid "Menstrual history"
msgstr "Historial de menstruación"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,metabolic_anormal:"
msgid "Metabolic anormalities"
msgstr "Anormalidad metabólicas"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,metabolic_anormal_description:"
msgid "Metabolic anormalities description"
msgstr "Descripción de anormalidades metabólicas"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,name:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,neumo_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,neumo_health_prof:"
msgid "Health Professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,neuro_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,neuro_healthprof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,neuro_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,neuropsyc_assessment:"
msgid "Neuropsychological assessment"
msgstr "Evaluación Neuropsicológica"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,oea:"
msgid "OEA"
msgstr "OEA"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,onco_health_inst:"
msgid "Institution"
msgstr "Institución"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,onco_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,onco_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,ophthalm_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,ophthalm_healthprof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,ophthalmology_assessments:"
msgid "Ophtalmology assessments"
msgstr "Evaluaciones oftalmológicas"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,orl_health_inst:"
msgid "Health institution"
msgstr "Institución"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,orl_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,orl_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,orth_health_inst:"
msgid "Institution"
msgstr "Institución"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,orth_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,orthesis_prosthesis:"
msgid "Orthesis/prosthesis"
msgstr "Órtesis/Prótesis"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,patella_inestability:"
msgid "Risk factors or inestability on the patella"
msgstr "Factores de riesgo o inestabilidad en la rótula"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,patella_inestability_description:"
msgid "Risk factors or inestability on the patella description"
msgstr "Descripción de los factores de riesgo o inestabilidad en la rótula"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,periodic_phys_examination:"
msgid "Periodic physical examination"
msgstr "Examen físico periódico"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,periodontal_disease_counseling:"
msgid "Periodontal disease counseling"
msgstr "Asesoramiento en enfermedades periodontales"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,polysomnography:"
msgid "Polysomnography"
msgstr "Polisomnografía"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,polysomnography_date:"
msgid "Last date of polysomnography"
msgstr "Ultima fecha de polisomnografía"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,psych_health_inst:"
msgid "Health Institution"
msgstr "Institución de Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,psych_health_prof:"
msgid "Health Professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,psych_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,pysch_health_inst:"
msgid "Health Institution"
msgstr "Institución de Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,recurrent_infections:"
msgid "Recurrent infections"
msgstr "Infecciones recurrentes"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,recurrent_infections_description:"
msgid "Recurrent infections"
msgstr "Infecciones recurrentes"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,reumatology:"
msgid "Reumatology"
msgstr "Reumatología"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,scoliosis:"
msgid "Scoliosis"
msgstr "Escoliosis"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,scoliosis_description:"
msgid "Scoliosis description"
msgstr "Descripción de escoliosis"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,self_inflicted_damage:"
msgid "Self inflicted damage"
msgstr "Daño autoinfligido"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,sex_education:"
msgid "Sex education and anticonceptive"
msgstr "Educación sexual y anticonceptiva"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,sexual_relations:"
msgid "Sexual relations"
msgstr "Relaciones sexuales"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,sleep_wake_cycle:"
msgid "Sleep/wake cycle"
msgstr "Ciclo sueño/vigilia"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,snoring:"
msgid "Snoring"
msgstr "Ronquidos"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,sphinters_control:"
msgid "Sphinters control"
msgstr "Control de esfinteres"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,state:"
msgid "State"
msgstr "Estado"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,surgical_histories:"
msgid "Surgical Histories"
msgstr "Historial de cirugías"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,t4:"
msgid "T4"
msgstr "T4"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,tdha:"
msgid "TDHA"
msgstr "TDHA"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,tea:"
msgid "TEA"
msgstr "TEA"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,tha:"
msgid "THA"
msgstr "THA"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,thyroid_ultrasound:"
msgid "Thyroid ultrasound"
msgstr "Ultrasonido de tiroides"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,thyroid_ultrasound_observations:"
msgid "Thyroid ultrasound - observations"
msgstr "Ultrasonido de tiroides - observaciones"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,title:"
msgid "Title"
msgstr "Título"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,uronefro_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,uronefro_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,use_glasses:"
msgid "Use of glasses"
msgstr "Uso de anteojos"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,videocolonoscopy:"
msgid "Videocolonoscopy"
msgstr "Videocolonoscopia"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,xray_cervical:"
msgid "X-ray cervical F and P with Torg index"
msgstr "RX cervical F y P funcional con índice de Torg"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,xray_hip_panoramic:"
msgid "X-ray panoramic hip"
msgstr "RX panorámica de Cadera"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,xray_pavlov:"
msgid "X-ray Pavlov"
msgstr "RX de Pavlov"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,xray_von_rosen:"
msgid "X-ray Von Rosen"
msgstr "RX de Von Rosen"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.barthel,barthel_evaluations:"
msgid "Barthel evaluations"
msgstr "Evaluaciones de Barthel"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.barthel,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.barthel,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.barthel,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.barthel,state:"
msgid "State"
msgstr "Estado"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.barthel,title:"
msgid "Title"
msgstr "Título"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.behaviour,accompanying_persons:"
msgid "Accompanying persons"
msgstr "Personas que acompañan"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.behaviour,accompanying_persons_domain:"
msgid "Accompanying persons domain"
msgstr "Dominio de personas que acompañan"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.behaviour,behaviour:"
msgid "Behaviour during consultation"
msgstr "Comportamiento durante la visita"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.behaviour,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.behaviour,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.behaviour,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.behaviour,state:"
msgid "State"
msgstr "Estado"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.behaviour,title:"
msgid "Title"
msgstr "Título"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,kinesiotherapy:"
msgid "Kinesiotherapy"
msgstr "Kinesioterapia"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,kinesiotherapy_freq:"
msgid "Kinesiotherapy frequency"
msgstr "Frecuencia de kinesioterapia"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,nursing_assistance:"
msgid "Nursing assistance"
msgstr "Asistencia de Enfermería"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,nursing_assistance_notes:"
msgid "Nursing assistance"
msgstr "Asistencia de Enfermería"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,occ_therapy:"
msgid "Occupational therapy"
msgstr "Terapia ocupacional"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,occ_therapy_freq:"
msgid "Occupational Therapy Frequency"
msgstr "Frecuencia  de terapia ocupacional"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,phonoaudiology:"
msgid "Phonoaudiology"
msgstr "Fonoaudiología"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,phonoaudiology_freq:"
msgid "Phonoaudiology frequency"
msgstr "Frecuencia de Fonoaudiología"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,psychiatrist:"
msgid "Psychiatrist"
msgstr "Psiquiatría"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,psychiatrist_freq:"
msgid "Psychiatrist frequency"
msgstr "Frecuencia de Psiquiatraía"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,psycologist:"
msgid "Psycologist"
msgstr "Psicología"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,psycologist_freq:"
msgid "Psycologist frequency"
msgstr "Frecuencia de Psicología"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,state:"
msgid "State"
msgstr "Estado"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,therapeutic_acc:"
msgid "Therapeutic accompaniment"
msgstr "Acompañamiento Terapéutico"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,therapeutic_acc_freq:"
msgid "Therapeutic accompaniment frequency"
msgstr "Frecuencia de Acompañamiento Terapéutico"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,title:"
msgid "Title"
msgstr "Título"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.growth_lines,month:"
msgid "Month"
msgstr "Mes"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.growth_lines,p10_value:"
msgid "P10"
msgstr ""

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.growth_lines,p25_value:"
msgid "P25"
msgstr ""

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.growth_lines,p3_value:"
msgid "P3"
msgstr ""

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.growth_lines,p50_value:"
msgid "P50"
msgstr ""

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.growth_lines,p75_value:"
msgid "P75"
msgstr ""

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.growth_lines,p90_value:"
msgid "P90"
msgstr ""

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.growth_lines,p97_value:"
msgid "P97"
msgstr ""

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.growth_lines,patient_value:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.laboratory,lab_requests:"
msgid "Requests"
msgstr "Pedidos"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.laboratory,lab_results:"
msgid "Results"
msgstr "Resultados"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.laboratory,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.laboratory,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.laboratory,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.laboratory,state:"
msgid "State"
msgstr "Estado"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.laboratory,title:"
msgid "Title"
msgstr "Título"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.lawton_brody,lawton_brody_evaluations:"
msgid "Lawton-Brody evaluations"
msgstr "Evaluaciones de Lawton y Brody"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.lawton_brody,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.lawton_brody,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.lawton_brody,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.lawton_brody,state:"
msgid "State"
msgstr "Estado"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.lawton_brody,title:"
msgid "Title"
msgstr "Título"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.medication,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.medication,medication:"
msgid "Medication"
msgstr "Medicación"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.medication,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.medication,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.medication,state:"
msgid "State"
msgstr "Estado"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.medication,title:"
msgid "Title"
msgstr "Título"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.patient_relative,conditions:"
msgid "Conditions"
msgstr "Condiciones"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.patient_relative,create_lejeune_attention_start:"
msgid "Start"
msgstr "Inicio"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.patient_relative,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.patient_relative,patient_domain:"
msgid "Patients domain"
msgstr ""

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.patient_relative,role:"
msgid "Role"
msgstr "Rol"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,apgar_scores:"
msgid "APGAR scores"
msgstr "Puntuaciones de APGAR"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,caesarean:"
msgid "Caesarean birth"
msgstr "Nacimiento por cesárea"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,cephalic_perimeter:"
msgid "CP (cm)"
msgstr "PC (cm)"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,control_15_days:"
msgid "15 days life control"
msgstr "Control a los 15 días de vida"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,control_30_days:"
msgid "30 days life control"
msgstr "Control a los 30 días de vida"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,controlled_pregnancy:"
msgid "Controlled pregnancy"
msgstr "Embarazo controlado"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,controlled_pregnancy_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,ft_screening:"
msgid "First trimester screening"
msgstr "Screening del primer trimestre"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,ft_screening_check:"
msgid "Screening normal"
msgstr "Screening normal"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,ft_screening_check_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,internment:"
msgid "Internment/hospitalization"
msgstr "Internado/Hospitalizado"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,length:"
msgid "Length (cm)"
msgstr "Longitud (cm)"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,maternal_lactancy_from:"
msgid "Maternal lactancy from"
msgstr "Lactancia materna desde"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,maternal_lactancy_until:"
msgid "Maternal lactancy until"
msgstr "Lactancia materna hasta"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,neonatal_control:"
msgid "Neonatal control"
msgstr "Control neonatal"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,neonatal_control_domain:"
msgid "Neonatal control"
msgstr "Control neonatal"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,newborn:"
msgid "Newborn"
msgstr "Recién nacido"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,newborn_domain:"
msgid "Newborn"
msgstr "Recién nacido"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,state:"
msgid "State"
msgstr "Estado"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,test_metabolic:"
msgid "Metabolic (\"heel stick screening\")"
msgstr "Prueba del talón"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,test_metabolic_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,title:"
msgid "Title"
msgstr "Título"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,weight:"
msgid "Weight (kg)"
msgstr "Peso (kg)"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,bmi:"
msgid "BMI"
msgstr "IMC"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,cardiovascular_system:"
msgid "Cardiovascular System"
msgstr "Sistema Cardiovascular"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,cephalic_perimeter:"
msgid "Cephalic perimeter (cm)"
msgstr "Perímetro cefálico (cm)"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,diastolic:"
msgid "Diastolic Pressure (mmHg)"
msgstr "Presión diastólica (mmHg)"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,digestive_system:"
msgid "Digestive System"
msgstr "Sistema Digestivo"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,ears:"
msgid "Ears"
msgstr "Oídos"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,growth_lines:"
msgid "Growth Lines"
msgstr "Lineas de Crecimiento"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,height:"
msgid "Height (cm)"
msgstr "Altura (cm)"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,indicator:"
msgid "Indicator"
msgstr "Indicador"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,lympha:"
msgid "Lymphatic system"
msgstr "Sistema Linfático"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,mouth:"
msgid "Mouth"
msgstr "Boca"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_diadochokinesia:"
msgid "Diadochokinesia"
msgstr "Diadococinesia"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_index_nose_test:"
msgid "Index-Nose Test"
msgstr "Prueba índice nariz"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_walk_heel:"
msgid "Heel walk"
msgstr "Marcha en talón"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_walk_straight:"
msgid "Walk straight"
msgstr "Caminar en línea recta"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_walk_tiptoes:"
msgid "Tiptoes walk"
msgstr "Caminar en puntas de pie"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_language:"
msgid "Language"
msgstr "Lenguaje"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_orientation_people:"
msgid "Orientation - People"
msgstr "Orientación persona"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_orientation_space:"
msgid "Orientation - Space"
msgstr "Orientación espacio"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_orientation_time:"
msgid "Orientation - Time"
msgstr "Orientación tiempo"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_aquiliano:"
msgid "Aquiliano reflex"
msgstr "Relfejo Aquiliano"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_aquiliano_notes:"
msgid "Aquiliano reflex notes"
msgstr "Notas"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_bicipital:"
msgid "Bicipital reflex"
msgstr "Reflejo bicipital"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_bicipital_notes:"
msgid "Bicipital reflex notes"
msgstr "Notas"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_others:"
msgid "Other reflex"
msgstr "Otros reflejos"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_patella:"
msgid "Patella reflex"
msgstr "Reflejo de rótula"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_patella_notes:"
msgid "Patella reflex notes"
msgstr "Notas"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_tricipital:"
msgid "Tricipital reflex"
msgstr "Reflejo tricipital"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_tricipital_notes:"
msgid "Tricipital reflex notes"
msgstr "Notas"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_static_coord_romberg_sensitized:"
msgid "Romberg sensitized"
msgstr "Romberg sensibilizado"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_static_coord_romberg_simple:"
msgid "Romberg simple"
msgstr "Romberg simple"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_system:"
msgid "Neurologic system"
msgstr "Sistema Neurológico"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_tone_strength_sensitivity:"
msgid "Tone, strength and sensitivity"
msgstr "Tono, fuerza y sensibilidad"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_vigil:"
msgid "Vigil"
msgstr "Vigil"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system:"
msgid "Osteoarthromuscular System"
msgstr "Sistema Osteoartomuscular"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_art_limit:"
msgid "Arthicular limitation"
msgstr "Limitación articular"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_heel_valgus:"
msgid "Heel valgus"
msgstr "Valgo de talón"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_pain:"
msgid "Arthicular Pain"
msgstr "Dolor articular"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_patella_inestability:"
msgid "Patella inestability"
msgstr "Inestabilidad de rótula"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_scoliosis:"
msgid "Scoliosis"
msgstr "Escoliosis"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,respiratory_system:"
msgid "Respiratory System"
msgstr "Sistema Respiratorio"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,skin:"
msgid "Skin and faneras"
msgstr "Piel y faneras"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,state:"
msgid "State"
msgstr "Estado"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,systolic:"
msgid "Systolic Pressure (mmHg)"
msgstr "Presión sistólica (mmHg)"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,title:"
msgid "Title"
msgstr "Título"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,urologic_system:"
msgid "Urologic System"
msgstr "Sistema urológico"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,weight:"
msgid "Weight (kg)"
msgstr "Peso (kg)"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,activity_workshop:"
msgid "Activity workshop"
msgstr "Taller de actividades"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,alcohol:"
msgid "Alcohol consumption"
msgstr "Consumo de alcohol"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,alcohol_frequency:"
msgid "Alcohol frequency"
msgstr "Frecuencia de consumo de alcohol"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,attentional_focus:"
msgid "Maintenance of attentional focus"
msgstr "Mantenimiento de foco atencional"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,bicycle:"
msgid "Bicycle"
msgstr "Bicicleta"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,drawing:"
msgid "Drawing"
msgstr "Dibujar"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,graph_language:"
msgid "Graph language"
msgstr "Lenguaje gráfico"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,group_of_friends:"
msgid "Group of friends"
msgstr "Grupo de amigos/as"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,has_couple:"
msgid "Has couple"
msgstr "Tiene  pareja"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,has_job:"
msgid "Has job?"
msgstr "Tiene trabajo?"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,id_level:"
msgid "ID level"
msgstr "Nivel de DI"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,id_level_legend:"
msgid "ID level legend"
msgstr "Nivel de DI"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,job:"
msgid "Job"
msgstr "Trabajo"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,knitting:"
msgid "Knitting"
msgstr "Tejido"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,language_level:"
msgid "Language level"
msgstr "Nivel del lenguaje"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,mood_personality:"
msgid "Mood / personality"
msgstr "Ánimo/Personalidad"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,occupational_center:"
msgid "Occupational center"
msgstr "Centro ocupacional"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,other_language:"
msgid "Other language"
msgstr "Otros lenguajes"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,other_means_of_mobilization:"
msgid "Other means of mobilization"
msgstr "Otros tipos de movilización"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,read_:"
msgid "Read"
msgstr "Leer"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,scissors:"
msgid "Scissors"
msgstr "Tijeras"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,sewing:"
msgid "Sewing"
msgstr "Costura"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,sign_language:"
msgid "Sign language"
msgstr "Lenguaje de señas"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,skate:"
msgid "Skate"
msgstr "Patineta"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,sports_hobbies:"
msgid "Sports and hobbies"
msgstr "Deportes y hobbies"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,sports_hobbies_center:"
msgid "Sports and hobbies specialized center"
msgstr "Centro especializado de deportes y hobbies"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,state:"
msgid "State"
msgstr "Estado"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,title:"
msgid "Title"
msgstr "Título"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,would_like_job:"
msgid "Would like a job?"
msgstr "Le gustaría trabajar?"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,write_:"
msgid "Write"
msgstr "Escribir"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,zip_closure:"
msgid "Zip closure"
msgstr "Cierre cremallera"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions,requests:"
msgid "Requests"
msgstr "Pedidos"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions,state:"
msgid "State"
msgstr "Estado"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions,suggestions:"
msgid "Suggestions"
msgstr "Sugerencias"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions,title:"
msgid "Title"
msgstr "Título"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.schooling,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.schooling,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.schooling,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.schooling,ses_assessments:"
msgid "SES assessments"
msgstr "Evaluaciones Socioeconómicas"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.schooling,state:"
msgid "State"
msgstr "Estado"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.schooling,title:"
msgid "Title"
msgstr "Título"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.start,families:"
msgid "Family"
msgstr "Familia"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.start,family_relatives:"
msgid "Family relatives"
msgstr "Miembros de la familia"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.start,family_relatives_domain:"
msgid "Family relatives domain"
msgstr ""

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.start,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.start,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.start,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.start,state:"
msgid "State"
msgstr "Estado"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.start,title:"
msgid "Title"
msgstr "Título"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.vaccines,lejeune_attention:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.vaccines,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.vaccines,patient_domain:"
msgid "Patient"
msgstr "Paciente"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.vaccines,state:"
msgid "State"
msgstr "Estado"

msgctxt "field:gnuhealth.trisomy21.create_lejeune_attention.vaccines,title:"
msgid "Title"
msgstr "Título"

msgctxt ""
"field:gnuhealth.trisomy21.create_lejeune_attention.vaccines,vaccines:"
msgid "Vaccines"
msgstr "Vacunas"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,address_city:"
msgid "City"
msgstr "Ciudad"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,address_country:"
msgid "country"
msgstr "País"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,address_street:"
msgid "Street"
msgstr "Calle"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,address_subdivision:"
msgid "Subdivision"
msgstr "Provincia"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,address_zip:"
msgid "Zip"
msgstr "Código Postal"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,citizenship:"
msgid "Citizenship"
msgstr "Ciudadanía"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,create_du:"
msgid "DU"
msgstr "Unidad Domiciliaria"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,dob:"
msgid "dob"
msgstr "FdN"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,du:"
msgid "DU"
msgstr "Unidad Domiciliaria"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,email:"
msgid "E-mail"
msgstr "Correo electrónico"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,family:"
msgid "Family"
msgstr "Familia"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,family_members:"
msgid "Family members"
msgstr "Miembros familiares"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,gender:"
msgid "Gender"
msgstr "Sexo"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,lastname:"
msgid "Lastname"
msgstr "Apellido"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,name:"
msgid "Name"
msgstr "Nombre"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,phone:"
msgid "Phone"
msgstr "Teléfono"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,puid:"
msgid "PUID"
msgstr "DNI del Paciente"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,qr:"
msgid "QR"
msgstr "QR"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,residence:"
msgid "Residence"
msgstr "Residencia"

msgctxt "field:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Role"
msgstr "Rol"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,accompanying_persons:"
msgid "Accompanying persons during consultation"
msgstr "Personas que acompañan durante la consulta"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,accompanying_persons_domain:"
msgid "Accompanying persons during consultation"
msgstr "Personas que acompañan durante la consulta"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,activity_workshop:"
msgid "Activity workshop"
msgstr "Taller de actividades"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,agenesia:"
msgid "Agenesia"
msgstr "Agenesia"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,alcohol:"
msgid "Alcohol consumption"
msgstr "Consumo de alcohol"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,alcohol_frequency:"
msgid "Alcohol frequency"
msgstr "Frecuencia de consumo de alcohol"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,andro_depression:"
msgid "Depression"
msgstr "Depresión"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,andro_fatigue:"
msgid "Fatigue"
msgstr "Fatiga"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,andro_headache:"
msgid "Headache"
msgstr "Cefalea (Dolor de cabeza)"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,andro_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,andro_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,andro_irritability:"
msgid "Irritability"
msgstr "Irritabilidad"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,andro_mood_changes:"
msgid "Mood changes"
msgstr "Cambios de humor"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,andro_sweat:"
msgid "Sweat"
msgstr "Sudor"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,andro_weight_inc:"
msgid "Weight increment"
msgstr "Incremento de peso"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,annual_testicular_control:"
msgid "Annual testicular control"
msgstr "Control  anual  testicular"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,annual_testicular_us:"
msgid "Annual testicular ultrasound"
msgstr "Ultrasonido testicular anual"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,annual_tsh:"
msgid "Annual tsh"
msgstr "TSH anual"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,anticonceptive:"
msgid "Contraceptive Method"
msgstr "Método anticonceptivo"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,apnea:"
msgid "Apnea"
msgstr "Apnea"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,appointment:"
msgid "Appointment"
msgstr "Turno"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,attentional_focus:"
msgid "Maintenance of attentional focus"
msgstr "Mantenimiento de foco atencional"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,autoimmune_disease:"
msgid "Autoimmune disease"
msgstr "Enfermedad autoinmune"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,behaviour:"
msgid "Behaviour during consultation"
msgstr "Comportamiento durante la consulta"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,behaviour_disorder:"
msgid "Behaviour disorder"
msgstr "Trastorno de conducta"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,bicycle:"
msgid "Bicycle"
msgstr "Bicicleta"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,bmi:"
msgid "BMI"
msgstr "IMC"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,bone_densitometry:"
msgid "Bone densitometry"
msgstr "Densitometría osea"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,bowel_habit:"
msgid "Bowel habit"
msgstr "Hábito intestinal"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,cardiov_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,cardiov_healthprof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,cardiovascular_system:"
msgid "Cardiovascular System"
msgstr "Sistema Cardiovascular"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,carpal_tunnel_synd:"
msgid "Carpal tunnel syndrome"
msgstr "Síndrome del túnel carpiano"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,celiac_screening:"
msgid "Celiac screening"
msgstr "Screening Celiaquía"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,cephalic_perimeter:"
msgid "Cephalic perimeter (cm)"
msgstr "Perímetro cefálico (cm)"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,cephalic_support:"
msgid "Cephalic support"
msgstr "Soporte cefálico"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,cholesterol:"
msgid "Cholesterol"
msgstr "Colesterol"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,chronic_cervical_myel:"
msgid "Chronic cervical myelopathy"
msgstr "Mielopatía cervical crónica"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,code:"
msgid "Code"
msgstr "Código"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,computed_age:"
msgid "Patient age at evaluation"
msgstr "Edad del paciente al momento de la evaluación"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,current_ther_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,dementia_assessment:"
msgid "Dementia assessment"
msgstr "Evaluación de la demencia"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,dental_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,dental_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,dental_orthosis:"
msgid "Dental Orthesis"
msgstr "Ortesis dental"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,dental_orthosis_description:"
msgid "Dental orthosis description"
msgstr "Descripción de la ortesis dental"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,derma_health_inst:"
msgid "Institution"
msgstr "Institución"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,derma_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,derma_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,diastolic:"
msgid "Diastolic Pressure (mmHg)"
msgstr "Presión diastólica (mmHg)"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,digest_current:"
msgid "Current Hash"
msgstr "Hash Actual"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,digest_status:"
msgid "Altered"
msgstr "Alterado"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,digestive_system:"
msgid "Digestive System"
msgstr "Sistema Digestivo"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,digital_signature:"
msgid "Digital Signature"
msgstr "Firma Digital"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,disab_health_inst:"
msgid "Institution"
msgstr "Institución"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,disab_health_prof:"
msgid "Health Professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,document_digest:"
msgid "Digest"
msgstr "Resumen"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,drawing:"
msgid "Drawing"
msgstr "Dibujar"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,early_mp_dec_libido:"
msgid "Decreased libido"
msgstr "Líbido disminuido"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,early_mp_headache:"
msgid "Headaches"
msgstr "Dolores de cabeza"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,early_mp_irritability:"
msgid "Irritability and depresion periods"
msgstr "Irritabilidad y períodos de depresión"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,early_mp_menstrual_change:"
msgid "Changes in the menstrual cycle"
msgstr "Cambios en el ciclo menstrual"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,early_mp_mood_changes:"
msgid "Mood changes"
msgstr "Cambios de humor"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,early_mp_night_sweats:"
msgid "Night sweats"
msgstr "Sudores nocturnos"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,early_mp_suffocation:"
msgid "Suffocation"
msgstr "Asfixia"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,ears:"
msgid "Ears"
msgstr "Oídos"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,endoc_health_inst:"
msgid "Institution"
msgstr "Institución"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,endoc_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,evaluation_endtime:"
msgid "End"
msgstr "Finalizar"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,evaluation_length:"
msgid "Evaluation length"
msgstr "Longitud de la evaluación"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,evaluation_start:"
msgid "Start"
msgstr "Inicio"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,fecal_occult_blood:"
msgid "Fecal occult blood"
msgstr "Sangre oculta en materia fecal"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,feeding:"
msgid "Feeding"
msgstr "Alimentación"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,feeding_intolerance:"
msgid "Feeding intolerance"
msgstr "Intolerancia alimentaria"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,feeding_intolerance_description:"
msgid "Feeding intolerance"
msgstr "Intolerancia alimentaria"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,fert_health_inst:"
msgid "Institution"
msgstr "Institución"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,fert_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,flatfoot:"
msgid "Flatfoot"
msgstr "Pie plano"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,gastroint_health_inst:"
msgid "Health Institution"
msgstr "Institución de Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,gastroint_health_prof:"
msgid "Health Professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,gastroint_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,gender:"
msgid "Gender"
msgstr "Sexo"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,gerd_symptoms:"
msgid "GERD symptoms"
msgstr "Síntomas de ERGE"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,glycemia:"
msgid "Glycemia"
msgstr "Glucemia"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,graph_language:"
msgid "Graph language"
msgstr "Lenguaje gráfico"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,group_of_friends:"
msgid "Group of friends"
msgstr "Grupo de amigos/as"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,gyneco_health_inst:"
msgid "Health Institution"
msgstr "Institución de Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,gyneco_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,has_couple:"
msgid "Has couple"
msgstr "Tiene  pareja"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,has_job:"
msgid "Has job?"
msgstr "Tiene trabajo?"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,healthprof:"
msgid "Health Prof"
msgstr "Prof.  Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,height:"
msgid "Height (cm)"
msgstr "Altura (cm)"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,hla_dq2_dq8:"
msgid "HLA-DQ2-DQ8"
msgstr "HLA-DQ2-DQ8"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,hospitalizations:"
msgid "Hospitalizations"
msgstr "Internaciones/Hospitalizaciones"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,hospitalizations_description:"
msgid "Hospitalizations"
msgstr "Internaciones/Hospitalizaciones"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,id_level:"
msgid "ID level"
msgstr "Nivel de DI"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,id_level_legend:"
msgid "ID level legend"
msgstr "Nivel de DI"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,information_source:"
msgid "Source"
msgstr "Original"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,job:"
msgid "Job"
msgstr "Trabajo"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,kinesiotherapy:"
msgid "Kinesiotherapy"
msgstr "Kinesioterapia"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,kinesiotherapy_freq:"
msgid "Kinesiotherapy frequency"
msgstr "Frecuencia de kinesioterapia"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,knitting:"
msgid "Knitting"
msgstr "Tejido"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,language_level:"
msgid "Language level"
msgstr "Nivel del lenguaje"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,last_audiometry:"
msgid "Last audiometry"
msgstr "Última audiometría"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,last_biannual_ophthalm_assessment:"
msgid "Last biannual ophtalmologic assessment"
msgstr "Ultima evaluación bianual oftalmológica"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,last_dental_control:"
msgid "Last dental control"
msgstr "Último control  dental"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,late_dentition:"
msgid "Late dentition"
msgstr "Dentición tardía"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,lympha:"
msgid "Lymphatic system"
msgstr "Sistema Linfático"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,menarche:"
msgid "Menarche"
msgstr "Edad de menarca"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,menopause:"
msgid "Menopause"
msgstr "Edad de menopausia"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,metabolic_anormal:"
msgid "Metabolic anormalities"
msgstr "Anormalidad metabólicas"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,metabolic_anormal_description:"
msgid "Metabolic anormalities description"
msgstr "Descripción de anormalidades metabólicas"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,mood_personality:"
msgid "Mood / personality"
msgstr "Ánimo/Personalidad"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,mouth:"
msgid "Mouth"
msgstr "Boca"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,name:"
msgid "Attention ID"
msgstr "ID de Atención"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neumo_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neumo_health_prof:"
msgid "Health Professional"
msgstr "Profesional de la Salud"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_diadochokinesia:"
msgid "Diadochokinesia"
msgstr "Diadococinesia"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_index_nose_test:"
msgid "Index-Nose Test"
msgstr "Prueba índice nariz"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_walk_heel:"
msgid "Heel walk"
msgstr "Marcha en talón"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_walk_straight:"
msgid "Walk straight"
msgstr "Caminar en línea recta"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_walk_tiptoes:"
msgid "Tiptoes walk"
msgstr "Caminar en puntas de pie"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuro_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuro_healthprof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuro_language:"
msgid "Language"
msgstr "Lenguaje"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuro_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,neuro_orientation_people:"
msgid "Orientation - People"
msgstr "Orientación persona"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuro_orientation_space:"
msgid "Orientation - Space"
msgstr "Orientación espacio"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuro_orientation_time:"
msgid "Orientation - Time"
msgstr "Orientación tiempo"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_aquiliano:"
msgid "Aquiliano reflex"
msgstr "Reflejo Aquiliano"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_aquiliano_notes:"
msgid "Aquiliano reflex notes"
msgstr "Notas"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_bicipital:"
msgid "Bicipital reflex"
msgstr "Reflejo bicipital"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_bicipital_notes:"
msgid "Bicipital reflex notes"
msgstr "Reflejo bicipital"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_others:"
msgid "Other reflex"
msgstr "Otros reflejos"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_patella:"
msgid "Patella reflex"
msgstr "Reflejo de rótula"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_patella_notes:"
msgid "Patella reflex notes"
msgstr "Reflejo de rótula"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_tricipital:"
msgid "Tricipital reflex"
msgstr "Reflejo tricipital"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_tricipital_notes:"
msgid "Tricipital reflex notes"
msgstr "Reflejo tricipital"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,neuro_static_coord_romberg_sensitized:"
msgid "Romberg sensitized"
msgstr "Romberg sensibilizado"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,neuro_static_coord_romberg_simple:"
msgid "Romberg simple"
msgstr "Romberg simple"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuro_system:"
msgid "Neurologic system"
msgstr "Sistema Neurológico"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,neuro_tone_strength_sensitivity:"
msgid "Tone, strength and sensitivity"
msgstr "Tono, fuerza y sensibilidad"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuro_vigil:"
msgid "Vigil"
msgstr "Vigil"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,neuropsyc_assessment:"
msgid "Neuropsychological assessment"
msgstr "Evaluación Neuropsicológica"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,nursing_assistance:"
msgid "Nursing assistance"
msgstr "Asistencia de Enfermería"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,nursing_assistance_notes:"
msgid "Nursing assistance"
msgstr "Asistencia de Enfermería"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,oam_system:"
msgid "Osteoarthromuscular System"
msgstr "Sistema Osteoartomuscular"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,oam_system_art_limit:"
msgid "Arthicular limitation"
msgstr "Limitación articular"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,oam_system_heel_valgus:"
msgid "Heel valgus"
msgstr "Valgo de talón"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,oam_system_pain:"
msgid "Arthicular Pain"
msgstr "Dolor articular"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,oam_system_patella_inestability:"
msgid "Rotula inestability"
msgstr "Inestabilidad de rótula"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,oam_system_scoliosis:"
msgid "Scoliosis"
msgstr "Escoliosis"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,occ_therapy:"
msgid "Occupational therapy"
msgstr "Terapia ocupacional"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,occ_therapy_freq:"
msgid "Occupational Therapy Frequency"
msgstr "Frecuencia  de terapia ocupacional"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,occupational_center:"
msgid "Occupational center"
msgstr "Centro ocupacional"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,oea:"
msgid "OEA"
msgstr "OEA"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,onco_health_inst:"
msgid "Institution"
msgstr "Institución"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,onco_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,onco_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,ophthalm_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,ophthalm_healthprof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,orl_health_inst:"
msgid "Heath institution"
msgstr "Institución"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,orl_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,orl_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,orth_health_inst:"
msgid "Institution"
msgstr "Institución"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,orth_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,orthesis_prosthesis:"
msgid "Orthesis/prosthesis"
msgstr "Órtesis/Prótesis"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,other_language:"
msgid "Other language"
msgstr "Otros lenguajes"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,other_means_of_mobilization:"
msgid "Other means of mobilization"
msgstr "Otros tipos de movilización"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,patella_inestability:"
msgid "Risk factors or inestability on the patella"
msgstr "Factores de riesgo o inestabilidad en la rótula"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,patella_inestability_description:"
msgid "Risk factors or inestability on the patella description"
msgstr "Descripción de los factores de riesgo o inestabilidad en la rótula"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,patient:"
msgid "Patient"
msgstr "Paciente"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,periodic_phys_examination:"
msgid "Periodic physical examination"
msgstr "Examen físico periódico"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,periodontal_disease_counseling:"
msgid "Periodontal disease counseling"
msgstr "Asesoría enfermedad periodental"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,phonoaudiology:"
msgid "Phonoaudiology"
msgstr "Fonoaudiología"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,phonoaudiology_freq:"
msgid "Phonoaudiology frequency"
msgstr "Frecuencia de fonoaudiología"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,polysomnography:"
msgid "Polysomnography"
msgstr "Polisomnografía"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,polysomnography_date:"
msgid "Last date of polysomnography"
msgstr "Utima fecha de polisomnografía"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,psych_health_inst:"
msgid "Health Institution"
msgstr "Institución de Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,psych_health_prof:"
msgid "Health Professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,psych_observations:"
msgid "Observations"
msgstr "Observaciones"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,psychiatrist:"
msgid "Psychiatrist"
msgstr "Psiquiatra"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,psychiatrist_freq:"
msgid "Psychiatrist frequency"
msgstr "Frecuencia de psiquiatra"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,psycologist:"
msgid "Psycologist"
msgstr "Psicología"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,psycologist_freq:"
msgid "Psycologist frequency"
msgstr "Frecuencia de psicología"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,pysch_health_inst:"
msgid "Health Institution"
msgstr "Institución de Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,read_:"
msgid "Read"
msgstr "Leer"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,reaumatology:"
msgid "Reumatology"
msgstr "Reumatología"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,recurrent_infections:"
msgid "Recurrent infections"
msgstr "Infecciones recurrentes"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,recurrent_infections_description:"
msgid "Recurrent infections"
msgstr "Infecciones recurrentes"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,requests:"
msgid "Requests"
msgstr "Pedidos"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,respiratory_system:"
msgid "Respiratory System"
msgstr "Sistema Respiratorio"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,reumatology:"
msgid "Reumatology"
msgstr "Reumatología"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,scissors:"
msgid "Scissors"
msgstr "Tijeras"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,scoliosis:"
msgid "Scoliosis"
msgstr "Escoliosis"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,scoliosis_description:"
msgid "Scoliosis description"
msgstr "Descripción de escoliosis"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,score:"
msgid "Score"
msgstr "Puntaje"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,self_inflicted_damage:"
msgid "Self inflicted damage"
msgstr "Daño autoinfligido"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,serializer:"
msgid "Doc String"
msgstr ""

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,serializer_current:"
msgid "Current Hash"
msgstr "Hash Actual"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,sewing:"
msgid "Sewing"
msgstr "Costura"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,sex_education:"
msgid "Sex education and anticonceptive"
msgstr "Educación sexual y anticonceptiva"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,sexual_relations:"
msgid "Sexual relations"
msgstr "Relaciones sexuales"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,sign_language:"
msgid "Sign language"
msgstr "Lenguaje de señas"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,signed_by:"
msgid "Signed by - Health Prof"
msgstr "Firmado por"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,skate:"
msgid "Skate"
msgstr "Patineta"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,skin:"
msgid "Skin and faneras"
msgstr "Piel y faneras"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,sleep_wake_cycle:"
msgid "Sleep/wake cycle"
msgstr "Ciclo sueño/vigilia"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,snoring:"
msgid "Snoring"
msgstr "Ronquidos"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,specialty:"
msgid "Specialty"
msgstr "Especialidad"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,sphinters_control:"
msgid "Sphinters control"
msgstr "Control de esfinteres"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,sports_hobbies:"
msgid "Sports and hobbies"
msgstr "Deportes y hobbies"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,sports_hobbies_center:"
msgid "Sports and hobbies specialized center"
msgstr "Centro especializado de deportes y hobbies"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,state:"
msgid "State"
msgstr "Estado"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,suggestions:"
msgid "Suggestions"
msgstr "Sugerencias"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,systolic:"
msgid "Systolic Pressure (mmHg)"
msgstr "Presión sistólica (mmHg)"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,t4:"
msgid "T4"
msgstr "T4"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,tdha:"
msgid "TDHA"
msgstr "TDHA"

#, fuzzy
msgctxt "field:gnuhealth.trisomy21.lejeune_attention,tea:"
msgid "TEA"
msgstr "TEA"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,tha:"
msgid "THA"
msgstr "THA"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,the:"
msgid "TEA"
msgstr "TEA"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,therapeutic_acc:"
msgid "Therapeutic accompaniment"
msgstr "Acompañamiento Terapéutico"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,therapeutic_acc_freq:"
msgid "Therapeutic accompaniment frequency"
msgstr "Frecuencia de acompañante terapéutico"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,thyroid_ultrasound:"
msgid "Thyroid ultrasound"
msgstr "Ultrasonido de tiroides"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention,thyroid_ultrasound_observations:"
msgid "Thyroid ultrasound - observations"
msgstr "Ultrasonido de tiroides - observaciones"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,urologic_system:"
msgid "Urologic System"
msgstr "Sistema urológico"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,uronefro_health_inst:"
msgid "Health institution"
msgstr "Institución de Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,uronefro_health_prof:"
msgid "Health professional"
msgstr "Profesional de la Salud"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,use_glasses:"
msgid "Use of glasses"
msgstr "Uso de anteojos"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,videocolonoscopy:"
msgid "Videocolonoscopy"
msgstr "Videocolonoscopia"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,wait_time:"
msgid "Patient wait time"
msgstr "Tiempo de espera del paciente"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,weight:"
msgid "Weight (kg)"
msgstr "Peso (kg)"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,would_like_job:"
msgid "Would like a job?"
msgstr "Le gustaría trabajar?"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,write_:"
msgid "Write"
msgstr "Escribir"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,xray_cervical:"
msgid "X-ray cervical F and P with Torg index"
msgstr "RX cervical F y P funcional con índice de Torg"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,xray_hip_panoramic:"
msgid "X-ray panoramic hip"
msgstr "RX panorámica de Cadera"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,xray_pavlov:"
msgid "X-ray Pavlov"
msgstr "RX de Pavlov"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,xray_von_rosen:"
msgid "X-ray Von Rosen"
msgstr "RX de Von Rosen"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention,zip_closure:"
msgid "Zip closure"
msgstr "Cierre cremallera"

msgctxt ""
"field:gnuhealth.trisomy21.lejeune_attention-family_member,lejeune_attention:"
msgid "Lejeune attention"
msgstr "Atención Lejeune"

msgctxt "field:gnuhealth.trisomy21.lejeune_attention-family_member,member:"
msgid "Member"
msgstr "Miembro"

msgctxt "help:gnuhealth.barthel_evaluation,appointment:"
msgid ""
"Enter or select the date / ID of the appointment related to this evaluation"
msgstr ""
"Introduzca o seleccione la fecha / ID de la cita asociada a esta evaluación"

msgctxt "help:gnuhealth.barthel_evaluation,code:"
msgid "Unique code that         identifies the evaluation"
msgstr "Código único que identifica la evaluación"

msgctxt "help:gnuhealth.barthel_evaluation,digest_status:"
msgid ""
"This field will be set whenever parts of the main original document has been"
" changed. Please note that the verification is done only on selected fields."
msgstr ""
"Este campo se establecerá siempre que se modifiquen partes del documento "
"original principal.  Por favor, tenga en cuenta que la verificación se "
"realiza solo en los campos seleccionados."

msgctxt "help:gnuhealth.barthel_evaluation,document_digest:"
msgid "Original Document Digest"
msgstr "Resumen del Documento Original"

msgctxt "help:gnuhealth.barthel_evaluation,evaluation_length:"
msgid "Duration of the evaluation"
msgstr "Duración de la evaluación"

msgctxt "help:gnuhealth.barthel_evaluation,healthprof:"
msgid ""
"Health professional that initiates the evaluation.This health professional "
"might or might not be the same that signs and finishes the evaluation.The "
"evaluation remains in progress state until it is signed, when it becomes "
"read-only"
msgstr ""
"Doctor/a que inicia la evaluación. El doctor/a que inicie la evaluación no "
"tiene por qué ser el mismo que la firme y dé por terminada. La evaluación "
"seguirá en proceso hasta que sea firmada, luego solo se podrá leer"

msgctxt "help:gnuhealth.barthel_evaluation,information_source:"
msgid "Source ofInformation, eg : Self, relative, friend ..."
msgstr "Fuente de información, ej: uno mismo, pariente, amigo..."

msgctxt "help:gnuhealth.barthel_evaluation,signed_by:"
msgid "Health Professional that finished the patient evaluation"
msgstr "Profesional de la salud que finalizó la evaluación del paciente"

msgctxt "help:gnuhealth.barthel_evaluation,wait_time:"
msgid "How long the patient waited"
msgstr "Cuánto tiempo estuvo esperando el paciente"

msgctxt "help:gnuhealth.lawton_brody_evaluation,appointment:"
msgid ""
"Enter or select the date / ID of the appointment related to this evaluation"
msgstr ""
"Introduzca o seleccione la fecha / ID de la cita asociada a esta evaluación"

msgctxt "help:gnuhealth.lawton_brody_evaluation,code:"
msgid "Unique code that         identifies the evaluation"
msgstr "Código único que identifica la evaluación"

msgctxt "help:gnuhealth.lawton_brody_evaluation,digest_status:"
msgid ""
"This field will be set whenever parts of the main original document has been"
" changed. Please note that the verification is done only on selected fields."
msgstr ""
"Este campo se establecerá siempre que se modifiquen partes del documento "
"original principal.  Por favor, tenga en cuenta que la verificación se "
"realiza solo en los campos seleccionados."

msgctxt "help:gnuhealth.lawton_brody_evaluation,document_digest:"
msgid "Original Document Digest"
msgstr "Resumen del Documento Original"

msgctxt "help:gnuhealth.lawton_brody_evaluation,evaluation_length:"
msgid "Duration of the evaluation"
msgstr "Duración de la evaluación"

msgctxt "help:gnuhealth.lawton_brody_evaluation,healthprof:"
msgid ""
"Health professional that initiates the evaluation.This health professional "
"might or might not be the same that signs and finishes the evaluation.The "
"evaluation remains in progress state until it is signed, when it becomes "
"read-only"
msgstr ""
"Doctor/a que inicia la evaluación. El doctor/a que inicie la evaluación no "
"tiene por qué ser el mismo que la firme y dé por terminada. La evaluación "
"seguirá en proceso hasta que sea firmada, luego solo se podrá leer"

msgctxt "help:gnuhealth.lawton_brody_evaluation,information_source:"
msgid "Source ofInformation, eg : Self, relative, friend ..."
msgstr "Fuente de información, ej: uno mismo, pariente, amigo..."

msgctxt "help:gnuhealth.lawton_brody_evaluation,signed_by:"
msgid "Health Professional that finished the patient evaluation"
msgstr "Profesional de la salud que finalizó la evaluación del paciente"

msgctxt "help:gnuhealth.lawton_brody_evaluation,wait_time:"
msgid "How long the patient waited"
msgstr "Cuánto tiempo estuvo esperando el paciente"

msgctxt "help:gnuhealth.neonatal_control,appointment:"
msgid ""
"Enter or select the date / ID of the appointment related to this evaluation"
msgstr ""
"Introduzca o seleccione la fecha / ID de la cita asociada a esta evaluación"

msgctxt "help:gnuhealth.neonatal_control,code:"
msgid "Unique code that         identifies the evaluation"
msgstr "Código único que identifica la evaluación"

msgctxt "help:gnuhealth.neonatal_control,digest_status:"
msgid ""
"This field will be set whenever parts of the main original document has been"
" changed. Please note that the verification is done only on selected fields."
msgstr ""
"Este campo se establecerá siempre que se modifiquen partes del documento "
"original principal.  Por favor, tenga en cuenta que la verificación se "
"realiza solo en los campos seleccionados."

msgctxt "help:gnuhealth.neonatal_control,document_digest:"
msgid "Original Document Digest"
msgstr "Resumen del Documento Original"

msgctxt "help:gnuhealth.neonatal_control,evaluation_length:"
msgid "Duration of the evaluation"
msgstr "Duración de la evaluación"

msgctxt "help:gnuhealth.neonatal_control,healthprof:"
msgid ""
"Health professional that initiates the evaluation.This health professional "
"might or might not be the same that signs and finishes the evaluation.The "
"evaluation remains in progress state until it is signed, when it becomes "
"read-only"
msgstr ""
"Doctor/a que inicia la evaluación. El doctor/a que inicie la evaluación no "
"tiene por qué ser el mismo que la firme y dé por terminada. La evaluación "
"seguirá en proceso hasta que sea firmada, luego solo se podrá leer"

msgctxt "help:gnuhealth.neonatal_control,information_source:"
msgid "Source ofInformation, eg : Self, relative, friend ..."
msgstr "Fuente de información, ej: uno mismo, pariente, amigo..."

msgctxt "help:gnuhealth.neonatal_control,maternal_lactancy_from:"
msgid "Age expresed in years"
msgstr "Edad expresada en años"

msgctxt "help:gnuhealth.neonatal_control,maternal_lactancy_until:"
msgid "Age expresed in years"
msgstr "Edad expresada en años"

msgctxt "help:gnuhealth.neonatal_control,signed_by:"
msgid "Health Professional that finished the patient evaluation"
msgstr "Profesional de la salud que finalizó la evaluación del paciente"

msgctxt "help:gnuhealth.neonatal_control,wait_time:"
msgid "How long the patient waited"
msgstr "Cuánto tiempo estuvo esperando el paciente"

msgctxt "help:gnuhealth.patient,age_wich_sat:"
msgid "Age in years"
msgstr "Edad (años)"

msgctxt "help:gnuhealth.patient,age_wich_talked:"
msgid "Age in years"
msgstr "Edad (años)"

msgctxt "help:gnuhealth.patient,age_wich_walked:"
msgid "Age in years"
msgstr "Edad (años)"

msgctxt "help:gnuhealth.patient,andro_depression:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "help:gnuhealth.patient,andro_fatigue:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "help:gnuhealth.patient,andro_headache:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "help:gnuhealth.patient,andro_irritability:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "help:gnuhealth.patient,andro_mood_changes:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "help:gnuhealth.patient,andro_sweat:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "help:gnuhealth.patient,andro_weight_inc:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "help:gnuhealth.patient,annual_testicular_control:"
msgid "Up to 45 years"
msgstr "Más de 45 años"

msgctxt "help:gnuhealth.patient,last_appointment_attended:"
msgid "Last appointment checked or done"
msgstr "Último turno chequeado o realizado"

msgctxt "help:gnuhealth.surgical_history.operation,procedure:"
msgid "Procedure Code, for exampl ICD-10-PCS or ICPM"
msgstr "Código del procedimiento (Ej: CIE-10)"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,age_wich_sat:"
msgid "Age in years"
msgstr "Edad (años)"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,age_wich_talked:"
msgid "Age in years"
msgstr "Edad (años)"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,age_wich_walked:"
msgid "Age in years"
msgstr "Edad (años)"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_depression:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_fatigue:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_headache:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_irritability:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_mood_changes:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_sweat:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,andro_weight_inc:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,annual_testicular_control:"
msgid "Up to 45 years"
msgstr "Más de 45 años"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,annual_tsh:"
msgid ""
"Request Ac if the value is between 5 and 10\n"
"(>10 refer to endocrinology with thyroid ultrasound)"
msgstr ""
"Solicitar Ac si el valor es entre 5 y 10 (>10 derivar a endocrinología con "
"ecografía tiroidea)"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,autoimmune_disease:"
msgid "Alopecia areata, vitiligo"
msgstr "Alopecia areata, vitiligo"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,bone_densitometry:"
msgid "From the age of 40"
msgstr "desde 40 años"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,cholesterol:"
msgid "Biennial if BMI>25"
msgstr "Bienal si el IMC>25"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_dec_libido:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_headache:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_irritability:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_menstrual_change:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_mood_changes:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_night_sweats:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,early_mp_suffocation:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,glycemia:"
msgid "Every 6 months if has FR"
msgstr "Cada 6 meses si tiene FR"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,menarche:"
msgid "Age in years"
msgstr "Edad (años)"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,menopause:"
msgid "Age in years"
msgstr "Edad (años)"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,patella_inestability:"
msgid "overweight, flatfoot"
msgstr "sobrepeso, pie plano"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,periodic_phys_examination:"
msgid "Skin, nail, hair"
msgstr "Piel, uñas  y pelo"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,sleep_wake_cycle:"
msgid "Description of sleep/wake cycle"
msgstr "CDescripción del ciclo sueño/vigilia"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.barthel,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.behaviour,accompanying_persons:"
msgid "accompanying person during consultation"
msgstr "Personas que acompañan durante la consulta"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.behaviour,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

#, fuzzy
msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,kinesiotherapy_freq:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,nursing_assistance_notes:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

#, fuzzy
msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,occ_therapy_freq:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

#, fuzzy
msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,phonoaudiology_freq:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

#, fuzzy
msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,psychiatrist_freq:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

#, fuzzy
msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,psycologist_freq:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

#, fuzzy
msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,therapeutic_acc_freq:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.laboratory,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.lawton_brody,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.medication,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,cephalic_perimeter:"
msgid "Cephalic Perimeter in centimeters (cm)"
msgstr "Perímetro Cefálico en centímetros (cm)"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

msgctxt "help:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,length:"
msgid "Length in centimeters (cm)"
msgstr "Longitud en centímetros (cm)"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,maternal_lactancy_from:"
msgid "Age expresed in years"
msgstr "Edad expresada en años"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,maternal_lactancy_until:"
msgid "Age expresed in years"
msgstr "Edad expresada en años"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,test_metabolic:"
msgid ""
"Test for Fenilketonuria, Congenital Hypothyroidism, Quistic Fibrosis, "
"Galactosemia"
msgstr ""
"Prueba para Fenilcetonuria, Hipotiroidismo congénito, Fibrosis Quística y "
"Galactosemia"

msgctxt "help:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,weight:"
msgid "Weight in grams (g)"
msgstr "Peso en gramos (g)"

msgctxt "help:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,bmi:"
msgid "Body mass index"
msgstr "Índice de masa corporal"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,cephalic_perimeter:"
msgid "Perimeter in centimeters"
msgstr "Perímetro en centimetros"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,diastolic:"
msgid "Diastolic pressure (mmHg)"
msgstr "Presión diastólica en mmHg"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,height:"
msgid "Height in centimeters"
msgstr "Altura en centímetros"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,systolic:"
msgid "Systolic pressure (mmHg)"
msgstr "Presión sistólica expresada en mmHg"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,weight:"
msgid "Weight in kilos"
msgstr "Peso en kilogramos"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.schooling,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.start,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

msgctxt ""
"help:gnuhealth.trisomy21.create_lejeune_attention.vaccines,lejeune_attention:"
msgid "Lejeune Attention record"
msgstr "Registro de Atención Lejeune"

msgctxt "help:gnuhealth.trisomy21.create_patient.start,dob:"
msgid "Date of Birth"
msgstr "Fecha de Nacimiento"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,andro_depression:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,andro_fatigue:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,andro_headache:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,andro_irritability:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,andro_mood_changes:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,andro_sweat:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,andro_weight_inc:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt ""
"help:gnuhealth.trisomy21.lejeune_attention,annual_testicular_control:"
msgid "Up to 45 years"
msgstr "Más de 45 años"

#, fuzzy
msgctxt "help:gnuhealth.trisomy21.lejeune_attention,annual_tsh:"
msgid ""
"Request Ac if the value is between 5 and 10\n"
"(>10 refer to endocrinology with thyroid ultrasound)"
msgstr ""
"Solicitar Ac si el valor es entre 5 y 10 (>10 derivar a endocrinología con "
"ecografía tiroidea)"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,appointment:"
msgid ""
"Enter or select the date / ID of the appointment related to this evaluation"
msgstr ""
"Introduzca o seleccione la fecha / ID de la cita asociada a esta evaluación"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,autoimmune_disease:"
msgid "Alopecia areata, vitiligo"
msgstr "Alopecia areata, vitiligo"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,bmi:"
msgid "Body mass index"
msgstr "Índice de masa corporal"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,bone_densitometry:"
msgid "From the age of 40"
msgstr "desde 40 años"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,cephalic_perimeter:"
msgid "Perimeter in centimeters"
msgstr "Perímetro en centimetros"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,cholesterol:"
msgid "Biennial if BMI>25"
msgstr "Bienal si el IMC>25"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,code:"
msgid "Unique code that         identifies the evaluation"
msgstr "Código único que identifica la evaluación"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,diastolic:"
msgid "Diastolic pressure (mmHg)"
msgstr "Presión diastólica en mmHg"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,digest_status:"
msgid ""
"This field will be set whenever parts of the main original document has been"
" changed. Please note that the verification is done only on selected fields."
msgstr ""
"Este campo se establecerá siempre que se modifiquen partes del documento "
"original principal.  Por favor, tenga en cuenta que la verificación se "
"realiza solo en los campos seleccionados."

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,document_digest:"
msgid "Original Document Digest"
msgstr "Resumen del Documento Original"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,early_mp_dec_libido:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,early_mp_headache:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,early_mp_irritability:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt ""
"help:gnuhealth.trisomy21.lejeune_attention,early_mp_menstrual_change:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,early_mp_mood_changes:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,early_mp_night_sweats:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,early_mp_suffocation:"
msgid "As an early menopause symptom"
msgstr "Signos de menopausia precoz"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,evaluation_length:"
msgid "Duration of the evaluation"
msgstr "Duración de la evaluación"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,glycemia:"
msgid "Every 6 months if has FR"
msgstr "Cada 6 meses si tiene FR"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,healthprof:"
msgid ""
"Health professional that initiates the evaluation.This health professional "
"might or might not be the same that signs and finishes the evaluation.The "
"evaluation remains in progress state until it is signed, when it becomes "
"read-only"
msgstr ""
"Doctor/a que inicia la evaluación. El doctor/a que inicie la evaluación no "
"tiene por qué ser el mismo que la firme y dé por terminada. La evaluación "
"seguirá en proceso hasta que sea firmada, luego solo se podrá leer"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,height:"
msgid "Height in centimeters"
msgstr "Altura en centímetros"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,information_source:"
msgid "Source ofInformation, eg : Self, relative, friend ..."
msgstr "Fuente de información, ej: uno mismo, pariente, amigo..."

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,kinesiotherapy_freq:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,menarche:"
msgid "Age in years"
msgstr "Edad (años)"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,menopause:"
msgid "Age in years"
msgstr "Edad (años)"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,nursing_assistance_notes:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,occ_therapy_freq:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,patella_inestability:"
msgid "overweight, flatfoot"
msgstr "sobrepeso, pie plano"

msgctxt ""
"help:gnuhealth.trisomy21.lejeune_attention,periodic_phys_examination:"
msgid "Skin, nail, hair"
msgstr "Piel, uñas  y pelo"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,phonoaudiology_freq:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,psychiatrist_freq:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,psycologist_freq:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,signed_by:"
msgid "Health Professional that finished the patient evaluation"
msgstr "Profesional de la salud que finalizó la evaluación del paciente"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,sleep_wake_cycle:"
msgid "Description of sleep/wake cycle"
msgstr "Descripción del ciclo sueño/vigilia"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,systolic:"
msgid "Systolic pressure (mmHg)"
msgstr "Presión sistólica expresada en mmHg"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,therapeutic_acc_freq:"
msgid "Weekly frequency"
msgstr "Frecuencia  Mensual"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,wait_time:"
msgid "How long the patient waited"
msgstr "Cuánto tiempo estuvo esperando el paciente"

msgctxt "help:gnuhealth.trisomy21.lejeune_attention,weight:"
msgid "Weight in kilos"
msgstr "Peso en kilogramos"

msgctxt "model:gnuhealth.barthel_evaluation,name:"
msgid "Barthel Evaluation"
msgstr "Evaluación de Barthel"

msgctxt "model:gnuhealth.lawton_brody_evaluation,name:"
msgid "Lawton and Brody Evaluation"
msgstr "Evaluación de Lawton y Brody"

msgctxt "model:gnuhealth.neonatal_control,name:"
msgid "Neonatal Control"
msgstr "Control neonatal"

msgctxt "model:gnuhealth.sequences.barthel_evaluation_sequence,name:"
msgid "Barthel Evaluation Sequence setup"
msgstr "Configuracion de Secuencia de Evaluación de Barthel"

msgctxt "model:gnuhealth.sequences.lawton_brody_evaluation_sequence,name:"
msgid "Barthel Evaluation Sequence setup"
msgstr "Configuracion de Secuencia de Evaluación de Barthel"

msgctxt "model:gnuhealth.sequences.lejeune_attention_sequence,name:"
msgid "Barthel Evaluation Sequence setup"
msgstr "Configuracion de Secuencia de Evaluación de Barthel"

msgctxt "model:gnuhealth.surgical_history,name:"
msgid "Surgical History"
msgstr "Historial de cirugías"

msgctxt "model:gnuhealth.surgical_history.operation,name:"
msgid "Surgical History - Operation"
msgstr "Historial de cirugías - Operaciones"

msgctxt "model:gnuhealth.trisomy21.appointment_report.start,name:"
msgid "Appointment Report - Start"
msgstr "Inicio de Reporte de Turnos"

msgctxt "model:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,name:"
msgid "Create Lejeune Attention - Anamnesis"
msgstr "Creación de Atención Lejeune - Anamnesis"

msgctxt "model:gnuhealth.trisomy21.create_lejeune_attention.barthel,name:"
msgid "Create Lejeune Attention - Aspects of Everyday Life"
msgstr "Creación de Atención Lejeune - Aspectos de la vida diaria"

msgctxt "model:gnuhealth.trisomy21.create_lejeune_attention.behaviour,name:"
msgid "Create Lejeune Attention - Behaviour during consultation"
msgstr "Creación de Atención Lejeune - Comportamiento durante la consulta"

msgctxt ""
"model:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,name:"
msgid "Create Lejeune Attention - Current Therapies"
msgstr "Creación de Atención Lejeune - Terapias actuales"

msgctxt ""
"model:gnuhealth.trisomy21.create_lejeune_attention.growth_lines,name:"
msgid "Growth Line"
msgstr "Linea de crecimiento"

msgctxt "model:gnuhealth.trisomy21.create_lejeune_attention.laboratory,name:"
msgid "Create Lejeune Attention - Laboratory"
msgstr "Creación de Atención Lejeune - Laboratorio"

msgctxt ""
"model:gnuhealth.trisomy21.create_lejeune_attention.lawton_brody,name:"
msgid "Create Lejeune Attention - Aspects of Everyday Life"
msgstr "Creación de Atención Lejeune - Aspectos de la vida diaria"

msgctxt "model:gnuhealth.trisomy21.create_lejeune_attention.medication,name:"
msgid "Create Lejeune Attention - Medication"
msgstr "Creación de Atención Lejeune - Medicación"

msgctxt ""
"model:gnuhealth.trisomy21.create_lejeune_attention.patient_relative,name:"
msgid "Patient Relative"
msgstr "Familiar del paciente"

msgctxt "model:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,name:"
msgid "Create Lejeune Attention - Pediatrics"
msgstr "Creación de Atención Lejeune - Perinatología"

msgctxt ""
"model:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,name:"
msgid "Create Lejeune Attention - Physical Exam"
msgstr "Creación de Atención Lejeune - Examen Físico"

msgctxt ""
"model:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,name:"
msgid "Create Lejeune Attention - Aspects of Everyday Life"
msgstr "Creación de Atención Lejeune - Aspectos de la vida diaria"

msgctxt ""
"model:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions,name:"
msgid "Create Lejeune Attention - Requests and suggestions"
msgstr "Creación de Atención Lejeune - Pedidos y sugerencias"

msgctxt "model:gnuhealth.trisomy21.create_lejeune_attention.schooling,name:"
msgid "Create Lejeune Attention - Schooling"
msgstr ""
"Creación de Atención Lejeune - Escolarización y Evaluación Socioecónomica"

msgctxt "model:gnuhealth.trisomy21.create_lejeune_attention.start,name:"
msgid "Create Lejeune Attention - Start"
msgstr "Creación de Atención Lejeune - Inicio"

msgctxt "model:gnuhealth.trisomy21.create_lejeune_attention.vaccines,name:"
msgid "Create Lejeune Attention - Vaccines"
msgstr "Creación de Atención Lejeune - Vacunas"

msgctxt "model:gnuhealth.trisomy21.create_patient.start,name:"
msgid "Create Patient - Start"
msgstr "Crear Paciente - Inicio"

msgctxt "model:gnuhealth.trisomy21.lejeune_attention,name:"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt "model:gnuhealth.trisomy21.lejeune_attention-family_member,name:"
msgid "Lejeune Attention - Family Member"
msgstr "Atención Lejeune- Miembros Familiares"

msgctxt "model:ir.action,name:act_appointment_report"
msgid "Appointment Report"
msgstr "Informe de Turnos"

msgctxt "model:ir.action,name:act_barthel_evaluation_form1"
msgid "Barthel Evaluation"
msgstr "Evaluación de Barthel"

msgctxt "model:ir.action,name:act_barthel_evaluation_score"
msgid "Barthel Evaluation Score"
msgstr "Evaluación de Barthel - Puntaje"

msgctxt "model:ir.action,name:act_create_lejeune_attention_wizard"
msgid "Create Lejeune Attention"
msgstr "Crear Atención Lejeune"

msgctxt "model:ir.action,name:act_create_patient_wizard"
msgid "Create Patient"
msgstr "Crear Paciente"

msgctxt "model:ir.action,name:act_lawton_brody_evaluation_form1"
msgid "Lawton - Brody Evaluation"
msgstr "Evaluación de Lawton y Brody"

msgctxt "model:ir.action,name:act_lawton_brody_evaluation_score"
msgid "Lawton - Brody Evaluation Score"
msgstr "Evaluación de Lawton y Brody - Puntaje"

msgctxt "model:ir.action,name:act_lejeune_attention_clinic_report"
msgid "Lejeune Attention - Clinic Report"
msgstr "Atención Lejeune - Reporte Clínico"

msgctxt "model:ir.action,name:act_lejeune_attention_pediatric_report"
msgid "Lejeune Attention - Pediatric Report"
msgstr "Atención Lejeune - Reporte pediátrico"

msgctxt "model:ir.action,name:act_lejeune_attention_requests_report"
msgid "Lejeune Attention - Requests"
msgstr ""

msgctxt "model:ir.action,name:act_wizard_shortcut_patient_family1"
msgid "Family"
msgstr "Familia"

msgctxt "model:ir.action,name:appointment_report_wizard"
msgid "Trisomy21 Appointment Report"
msgstr "Reporte de Atenciones  FJL"

msgctxt "model:ir.action,name:gnuhealth_action_barthel"
msgid "Barthel Evaluation"
msgstr "Evaluación de Barthel"

msgctxt "model:ir.action,name:gnuhealth_action_lawton_brody"
msgid "Lawton and Brody Evaluation"
msgstr "Evaluación de Lawton y Brody"

msgctxt "model:ir.action,name:gnuhealth_action_lejeune_attention"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt "model:ir.action,name:gnuhealth_action_neonatal_control"
msgid "Neonatal controls"
msgstr "Controles neonatales"

msgctxt ""
"model:ir.action.act_window.domain,name:act_gnuhealth_appointment_domain_to_confirm"
msgid "To confirm"
msgstr "A confirmar"

msgctxt "model:ir.message,text:msg_get_anamnesis_title"
msgid "Anamnesis"
msgstr "Anamnesis"

msgctxt "model:ir.message,text:msg_get_aspects_everyday_life_title"
msgid "Aspects of everyday life - Barthel"
msgstr "Aspectos de la vida diaria - Barthel"

msgctxt "model:ir.message,text:msg_get_autonomy_conquest_title"
msgid "Autonomy Conquest - Lawton and Brody"
msgstr "Conquista de la autonomía - Lawton y Brody"

msgctxt "model:ir.message,text:msg_get_behaviour_title"
msgid "Behaviour during consultation"
msgstr "Comportamiento durante la consulta"

msgctxt "model:ir.message,text:msg_get_current_therapies_title"
msgid "Current Therapies"
msgstr "Terapias actuales"

msgctxt "model:ir.message,text:msg_get_laboratory_title"
msgid "Laboratory"
msgstr "Laboratorio"

msgctxt "model:ir.message,text:msg_get_medication_title"
msgid "Medication"
msgstr "Medicación"

msgctxt "model:ir.message,text:msg_get_pediatrics_title"
msgid "Pediatrics"
msgstr "Pediatría"

msgctxt "model:ir.message,text:msg_get_physical_exam_title"
msgid "Physical exam"
msgstr "Examen Físico"

msgctxt "model:ir.message,text:msg_get_psyc_assessment_title"
msgid "Psycological assessment"
msgstr "Evaluación Psicológica"

msgctxt "model:ir.message,text:msg_get_requests_suggestions_title"
msgid "Requests and Suggestions"
msgstr "Pedidos y sugerencias"

msgctxt "model:ir.message,text:msg_get_schooling_title"
msgid "Schooling"
msgstr "Escolarización"

msgctxt "model:ir.message,text:msg_get_start_title"
msgid "Start"
msgstr "Inicio"

msgctxt "model:ir.message,text:msg_get_vaccines_title"
msgid "Vaccines"
msgstr "Vacunas"

msgctxt "model:ir.message,text:msg_period_too_long"
msgid "SM-CALENDAR-003: The schedule period should be less than 365 days"
msgstr ""
"SM-CALENDAR-003: El periodo agendado debe ser menor a 365 días (un año)"

msgctxt "model:ir.sequence,name:seq_gnuhealth_barthel_evaluation"
msgid "Barthel Evaluation"
msgstr "Evaluación de Barthel"

msgctxt "model:ir.sequence,name:seq_gnuhealth_lawton_brody_evaluation"
msgid "Barthel Evaluation"
msgstr "Evaluación de Barthel"

msgctxt "model:ir.sequence,name:seq_gnuhealth_lejeune_attention"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt "model:ir.sequence.type,name:seq_type_gnuhealth_barthel_evaluation"
msgid "Barthel Evaluation"
msgstr "Evaluación de Barthel"

msgctxt ""
"model:ir.sequence.type,name:seq_type_gnuhealth_lawton_brody_evaluation"
msgid "Lawton - Brody Evaluation"
msgstr "Evaluación de Lawton y Brody"

msgctxt "model:ir.sequence.type,name:seq_type_gnuhealth_lejeune_attention"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt "model:ir.ui.menu,name:appointment_report_menu"
msgid "Trisomy21 Appointment Report"
msgstr "Reporte de Atenciones  FJL"

msgctxt "model:ir.ui.menu,name:gnuhealth_lejeune_attention"
msgid "Lejeune Attention"
msgstr "Atención Lejeune"

msgctxt "model:ir.ui.menu,name:gnuhealth_neonatal_control"
msgid "Neonatal control"
msgstr "Control neonatal"

msgctxt "model:ir.ui.menu,name:gnuhealth_patient_create_menu"
msgid "New Patient"
msgstr "Nuevo Paciente"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "-"
msgstr "-"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "0"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Age"
msgstr "Edad"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Appointment Date"
msgstr "Fecha de la cita"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Attentions Report"
msgstr "Reporte de atenciones"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Attentions by Professional"
msgstr "Atenciones por Profesional"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Av. Velez Sarsfield 1460 3° Piso, Córdoba,"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Córdoba, Argentina"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "DoB"
msgstr "Fecha de nacimiento"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Fecha de impresión:"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Fundación Jerome Lejeune Argentina"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Gender"
msgstr "Sexo"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "P"
msgstr "P"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Patient"
msgstr "Paciente"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Percentage of patients subsized:"
msgstr "Porcentaje de pacientes subsidiados"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Percentage subsized:"
msgstr "Porcentaje subsidiado"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Professional:"
msgstr "Profesional:"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Quantity of patients subsized:"
msgstr "Cantidad de pacientes subsidiados"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Stadistic report from:"
msgstr "Reporte estadístico desde:"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "Subsidized"
msgstr "Subsidiado"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "XXXXXX"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "er"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "i"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "info@fundacionlejeune.org.ar"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "od"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "to:"
msgstr "hasta:"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "www.fundacionlejeune.org/ar"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "–"
msgstr "-"

msgctxt "report:gnuhealth.trisomy21.appointment_report:"
msgid "– Tel 351 - 4542601"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "(times/week):"
msgstr "(veces/semana):"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "*"
msgstr "*"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "* Behaviour during consultation:"
msgstr "* Comportamiento durante la consulta:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "* Clinical examination:"
msgstr "* Evaluación clínica"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "* Osteoarthromuscular System"
msgstr "* Sistema osteoartromuscular"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "* Requests and suggestions:"
msgstr "* Pedidos y sugerencias"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "-"
msgstr "-"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "- Cardiovascular System:"
msgstr "- Sistema Cardiovascular:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "- Digestive System:"
msgstr "- Sistema Digestivo:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "- Ears:"
msgstr "- Orejas:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "- Lynphatic System:"
msgstr "- Sistema Linfático:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "- Neurological"
msgstr "- Neurológico"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "- Respiratory System:"
msgstr "- Sistema Respiratorio"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "- Urogenital System:"
msgstr "- Sistema Urogenital"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "."
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "0"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "1"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "10"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "5"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid ":"
msgstr ":"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "???????"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "A"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "A."
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "A. Eating"
msgstr "A. Comer"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "ACTIVITY"
msgstr "ACTIVIDAD"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "ADMINISTRATION DATE"
msgstr "FECHA DE ADMINISTRACIÓN"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "ASPECT"
msgstr "ASPECTO"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Accompanying persons:"
msgstr "Personas acompañantes"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Activity workshop:"
msgstr "Taller de actividades:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Agenesia:"
msgstr "Agenesia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Alcohol consumption:"
msgstr "Consumo de alcohol"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Annual tsh:"
msgstr "TSH anual:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Anthropometry:"
msgstr "Antropometría:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Aquiliano:"
msgstr "Aquiliano:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Aspects of daily life / Conquest of autonomy"
msgstr "Apesctos de la vida diaria / Conquista de la autonomía"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Autoinmune disease:"
msgstr "Enfermedad autoinmune:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Av. Velez Sarsfield 1460 3° Piso, Córdoba,"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "B"
msgstr "B"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "B. Bathe"
msgstr "B. Bañarse"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "B. Shopping"
msgstr "B. Compras"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Behaviour:"
msgstr "Comportamiento:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Bicipital:"
msgstr "Bicipital:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Birth:"
msgstr "Nacimiento:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Bone densitometry:"
msgstr "Densitometría osea:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Bowel habit:"
msgstr "Habito intestinal:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Bycicle:"
msgstr "Bicicleta:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "C. Cooking"
msgstr "C. Cocinar"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "C. Dressing"
msgstr "C. Vestirse"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "CONCLUSION:"
msgstr "CONCLUSIÓN:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Cardiovascular Health"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Cardiovascular Health prof:"
msgstr "Profesional Cardiovascular:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Cardiovascular System:"
msgstr "Sistema Cardiovascular:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Carpal tunnel syndrome:"
msgstr "Síndrome de tunel carpiano:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Celiac screening:"
msgstr "Screening celíaco:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Cholesterol:"
msgstr "Colesterol:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Chronic cervical myelopathy:"
msgstr "Mielopatía cervical crónica:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Clinic"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Current medication:"
msgstr "Medicación actual:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Córdoba, Argentina"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "D"
msgstr "D"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "D. Hygiene"
msgstr "D. Higiene"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "DATE"
msgstr "FECHA"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "DOSE"
msgstr "DOSIS"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Dementia assessment:"
msgstr "Evaluación de demencia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Dental"
msgstr "Dental"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Dental orthesis:"
msgstr "Ortesis dental:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Dermatology"
msgstr "Dermatología"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Dermatology:"
msgstr "Dermatología"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Development and Behaviour:"
msgstr "Desarrollo y comportamiento:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Diadocokinesia:"
msgstr "Diadoquinesia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Diastolic pressure;"
msgstr "Presión diastólica:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Drawing:"
msgstr "Pintura:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Dynamic coordination."
msgstr "Coordinación dinámica."

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "E"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "E. Depositions"
msgstr "E. Deposiciones"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "END TREATMENT"
msgstr "FIN TRATAMIENTO"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Endocrinal"
msgstr "Sistema endocrino:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "F"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "F. Urination"
msgstr "F. Micción"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "FAMILY"
msgstr "FAMILIA"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Family background:"
msgstr "Antecedentes familiares:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Feeding intolerance:"
msgstr "Intolerancia de alimentación:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Feeding:"
msgstr "Alimentación:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Fine motor activity:"
msgstr "Actividad motora fina:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Flatfoot:"
msgstr "Pie plano:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Freq:"
msgstr "Frecuencia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Frequency"
msgstr "Frecuencia"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Frequency (times/week):"
msgstr "Frequencia (veces/semana):"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Fundación Jerome Lejeune Argentina"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "G"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "G. Toilet use"
msgstr "G. Uso del baño"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "GERD symptoms:"
msgstr "Sintomas de ERGE:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Gastrointestinal"
msgstr "Gastrointestinal"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Gastrointestinal System:"
msgstr "Sistema gastrointestinal:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Graph language:"
msgstr "Lenguaje por gráficos:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Group of friends:"
msgstr "Grupo de amigos:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "H"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "H. Moving"
msgstr "H. Moverse"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "HLA-DQ2-DQ8:"
msgstr "HLA-DQ2-DQ8:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Has couple?:"
msgstr "¿Tiene pareja?:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Has job?:"
msgstr "¿Tiene trabajo?:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Health"
msgstr "Salud"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Health prof:"
msgstr "Profesional de salud:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Heel walk:"
msgstr "Marcha en talón:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Height:"
msgstr "Altura:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Hematology"
msgstr "Hematología"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Hematology:"
msgstr "Hematología"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Home Care"
msgstr "Cuidado de la casa"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Hospitalizations:"
msgstr "Hospitalizaciones:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "I. Wandering"
msgstr "I. Deambular"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "ID level:"
msgstr "Nivel de DI:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Index-Nose test:"
msgstr "Prueba índice-nariz:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "J. Climb stairs"
msgstr "J. Subir escaleras"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Job:"
msgstr "Trabajo:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Kinesiotherapy:"
msgstr "Kinesioterapia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Knitting:"
msgstr "Tejer:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Language level:"
msgstr "Nivel de lenguaje:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Language:"
msgstr "Lenguaje:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Last audiometry:"
msgstr "Ultima audiometría:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Last dental control:"
msgstr "Ultimo control dental:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Late dentition:"
msgstr "Dentición tardía:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Laundry"
msgstr "Lavar la ropa"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid ""
"Lawton and Brody scale for instrumental activities of daily living (IADL)"
msgstr ""
"Escala de Lawton y Brody para las actividades instrumentales de la vida "
"diaria (AIVD)"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Liability of medication"
msgstr "Confiabilidad de la medicación"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "M"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "MEDICAMENT"
msgstr "MEDICAMENTO"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "MENT"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "MI:"
msgstr "IMC:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Maintenance of attentional focus:"
msgstr "Mantenimiento de foco atencional:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Means of mobilization:"
msgstr "Medios de movilización:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Model Form"
msgstr "Formulario Modelo Clínico"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Money handling"
msgstr "Capacidad de utilizar dinero"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Mouth:"
msgstr "Boca:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "N"
msgstr "N"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Neumonology"
msgstr "Neumonología"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Neurologic System:"
msgstr "Sistema neurológico"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Neurological"
msgstr "Neurologico"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Neuropsychological assessment:"
msgstr "Evaluación neuropsicológica"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "No"
msgstr "No"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Nouse: ………"
msgstr "Nariz: ....."

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "O"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "ORL:"
msgstr "ORL:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "ORL: ……"
msgstr "ORL: ....."

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Occupational center:"
msgstr "Centro ocupacional:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Ophtalmologics: ????"
msgstr "Oftalmología: ?????"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Orientation:"
msgstr "Orientación:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Original ADL scores included in the Barthel Index"
msgstr "Puntuaciones originales de las AVD incluidas en el Índice de Barthel"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Orthesis/prosthesis:"
msgstr "Ortesis/protesis:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Other:"
msgstr "Otro:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Others:"
msgstr "Otros:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "P"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "People:"
msgstr "Gente:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Periodic physical examination:"
msgstr "Examinación física períodica"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Periodontal disease counseling:"
msgstr "Asesoría enfermedad periodental:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Phone use"
msgstr "Uso del teléfono"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Phonoaudiology:"
msgstr "Fonoaudiología:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Pregnancy:"
msgstr "Embarazo:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Print date"
msgstr "Fecha de impresión"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Psychiatris"
msgstr "Psiquiatría:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Psycologist:"
msgstr "Psicología:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "RL health prof:"
msgstr "Profesional de salud de ORL:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Read:"
msgstr "Leer:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Recurrent infections:"
msgstr "Infecciones recurrentes:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Reflexes"
msgstr "Reflejos"

#, fuzzy
msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Requests"
msgstr "Pedidos"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Respiratory System:"
msgstr "Sistema Respiratorio:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Risk factors or inestability on the patella:"
msgstr "Factores de riesgo o inestabilidad en la rotula:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Romberg sensitized:"
msgstr "Romberg sensibilizado:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Romberg simple:"
msgstr "Romber simple:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "S"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "SCORE"
msgstr "Puntaje"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "START TREA"
msgstr "COMIENZO TRATAMIENTO"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Scissors:"
msgstr "Tijeras:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Scoliosis:"
msgstr "Escoliosis:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Score"
msgstr "Puntuación"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Sewing:"
msgstr "Coser:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Sign language:"
msgstr "Lenguaje por señas:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Skate:"
msgstr "Patineta:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Skin and faneras:"
msgstr "Piel y faneras:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Sleep/wake cycle:"
msgstr "Ciclo de vigilia/sueño:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Snoring:"
msgstr "Ronquidos:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Space:"
msgstr "Espacio:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Sports and hobbies specialized center:"
msgstr "Centro especializado para deportes y hobbies:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Sports and hobbies:"
msgstr "Deportes y hobbies:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Static coordination"
msgstr "Coordinación estática"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Suggestions:"
msgstr "Sugerencias:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "System:"
msgstr "Sistema Neurológico:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Systolic pressure:"
msgstr "Presión sistólica:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "T"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Therapeutic accompaniment"
msgstr "Acompañamiento Terapéutico"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Thyroid ultrasound:"
msgstr "Ultrasonido de tiroides:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Time:"
msgstr "Tiempo:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Tiptoes walk:"
msgstr "Marcha en puntas de pie:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Tone, strength and sensitivity:"
msgstr "Tono, fuerza y sensibilidad;"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Traumatology"
msgstr "Traumatología"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Tricipital:"
msgstr "Tricipital:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Urogenital System:"
msgstr "Sistema Urogenital:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Use of transportation"
msgstr "Uso del transporte"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "VACCINE"
msgstr "VACUNA"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Vaccines:"
msgstr "Vacunas:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Videocolonoscopy:"
msgstr "Videocolonoscopia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Vigil:"
msgstr "Vigil:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Vital signs:"
msgstr "Signos vitales:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "W"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Walk straight:"
msgstr "Marcha recta:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Would like a job?:"
msgstr "¿Quisiera trabajar?:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Write:"
msgstr "Escribe:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "X-Ray Cervical:"
msgstr "RX cervical:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "X-Ray Pavlov:"
msgstr "RX Pavlov:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "X-Ray panoramic hip:"
msgstr "RX  de cadera panorámica:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Yes"
msgstr "Sí"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "Zip closure:"
msgstr "Cremallera:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "and orthopedics"
msgstr "y ortopedia"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "atella:"
msgstr "Rotula:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "backgrou"
msgstr "antecedentes:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "ccupational therapy:"
msgstr "Terapia ocupacional:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "dontostomatology:"
msgstr "Odontoestomatología:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "ds:"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "e-mail"
msgstr "correo electrónico"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "ecal occult blood:"
msgstr "SOMF:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "eight:"
msgstr "Peso:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "enetic tests:"
msgstr "Pruebas genéticas:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "erinatal background"
msgstr "Antecedentes perinatales"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "etabolic anormalities:"
msgstr "Anormalidades metabólicas"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "herapeutic"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "info@fundacionlejeune.org.ar"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "institution"
msgstr "institución"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "intitution"
msgstr "institución"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "lycemia:"
msgstr "Glucemia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "n"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "olysomnography:"
msgstr "Polisomnografía:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "ood overall condition."
msgstr "Buen estado general."

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "ood/personality:"
msgstr "Animo/personalidad:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "pnea:"
msgstr "Apnea:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "t:"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "upport and"
msgstr "Apoyo y"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "upport:"
msgstr "soporte terapeútico:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "ursing assistance:"
msgstr "Asistencia de enfermería/cuidador:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "www.fundacionlejeune.org/ar"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "ystem:"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "–"
msgstr "-"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.clinic.report:"
msgid "– Tel 351 - 4542601"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "(times/week):"
msgstr "(veces/semana):"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "*"
msgstr "*"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "* Behaviour during consultation:"
msgstr "* Comportamiento durante la consulta:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "* Clinical examination:"
msgstr "* Evaluación clínica"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "* Osteoarthromuscular System"
msgstr "* Sistema osteoartromuscular"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "* Requests and suggestions:"
msgstr "* Pedidos y sugerencias"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid ""
", and being able to carry out his comprehensive follow-up within the JL "
"Foundation, in the context of his diagnosis of Trisomy 21."
msgstr ""
", y poder realizar su seguimiento integral dentro de la Fundación JL, en el "
"contexto de su diagnóstico de Trisomía 21."

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "-"
msgstr "-"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "- Cardiovascular System:"
msgstr "- Sistema Cardiovascular:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "- Digestive System:"
msgstr "- Sistema Digestivo:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "- Ears:"
msgstr "- Orejas:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "- Lynphatic System:"
msgstr "- Sistema Linfático:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "- Neurological"
msgstr "- Neurológico"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "- Respiratory System:"
msgstr "- Sistema Respiratorio"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "- Urogenital System:"
msgstr "- Sistema Urogenital"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid ":"
msgstr ":"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "???????"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "A"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "ADMINISTRATION DATE"
msgstr "FECHA DE ADMINISTRACIÓN"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Accompanying persons:"
msgstr "Personas acompañantes"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Activity workshop:"
msgstr "Taller de actividades:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Agenesia:"
msgstr "Agenesia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Alcohol consumption:"
msgstr "Consumo de alcohol"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Annual tsh:"
msgstr "TSH anual:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Anthropometry:"
msgstr "Antropometría:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Aquiliano:"
msgstr "Aquiliano:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Autoinmune disease:"
msgstr "Enfermedad autoinmune:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Av. Velez Sarsfield 1460 3° Piso, Córdoba,"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "B"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Behaviour:"
msgstr "Comportamiento:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Bicipital:"
msgstr "Bicipital:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Birth:"
msgstr "Nacimiento:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Bone densitometry:"
msgstr "Densitometría osea:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Bowel habit:"
msgstr "Habito intestinal:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Bycicle:"
msgstr "Bicicleta:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "CONCLUSION:"
msgstr "CONCLUSIÓN:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Cardiovascular Health"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Cardiovascular Health prof:"
msgstr "Profesional Cardiovascular:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Cardiovascular System:"
msgstr "Sistema Cardiovascular:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Carpal tunnel syndrome:"
msgstr "Síndrome de tunel carpiano:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Celiac screening:"
msgstr "Screening celíaco:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Cholesterol:"
msgstr "Colesterol:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Chronic cervical myelopathy:"
msgstr "Mielopatía cervical crónica:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Current medication:"
msgstr "Medicación actual:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Córdoba, Argentina"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "DATE"
msgstr "FECHA"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "DOSE"
msgstr "DOSIS"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Dear family, Dear colleagues"
msgstr "Estimada familia, Estimados colegas"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Dementia assessment:"
msgstr "Evalución de demencia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Dental"
msgstr "Dental"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Dental orthesis:"
msgstr "Ortesis dental:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Dermatology"
msgstr "Dermatología"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Dermatology:"
msgstr "Dermatología"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Development and Behaviour:"
msgstr "Desarrollo y comportamiento:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Diadocokinesia:"
msgstr "Diadoquinesia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Diastolic pressure;"
msgstr "Presión diastólica:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Drawing:"
msgstr "Pintura:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Dynamic coordination."
msgstr "Coordinación dinámica."

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "END TREATMENT"
msgstr "FIN TRATAMIENTO"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Endocrinal"
msgstr "Sistema endocrino:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "F"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "FAMILY"
msgstr "FAMILIA"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Family background:"
msgstr "Antecedentes familiares:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Feeding intolerance:"
msgstr "Intolerancia de alimentación:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Feeding:"
msgstr "Alimentación:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Fine motor activity:"
msgstr "Actividad motora fina:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Flatfoot:"
msgstr "Pie plano:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Freq:"
msgstr "Frecuencia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Frequency"
msgstr "Frecuencia"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Frequency (times/week):"
msgstr "Frequencia (veces/semana):"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Fundación Jerome Lejeune Argentina"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "G"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "GERD symptoms:"
msgstr "Sintomas de ERGE:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Gastrointestinal"
msgstr "Gastrointestinal"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Gastrointestinal System:"
msgstr "Sistema gastrointestinal:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Graph language:"
msgstr "Lenguaje por gráficos:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Group of friends:"
msgstr "Grupo de amigos:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "HLA-DQ2-DQ8:"
msgstr "HLA-DQ2-DQ8:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Has couple?:"
msgstr "¿Tiene pareja?:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Has job?:"
msgstr "¿Tiene trabajo?:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Health"
msgstr "Salud"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Health prof:"
msgstr "Profesional de salud:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Heel walk:"
msgstr "Marcha en talón:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Height:"
msgstr "Altura:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Hematology"
msgstr "Hematología"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Hematology:"
msgstr "Hematología"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Hospitalizations:"
msgstr "Hospitalizaciones:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "ID level:"
msgstr "Nivel de DI:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Index-Nose test:"
msgstr "Prueba índice-nariz:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid ""
"It is with great pleasure that I write to you. after having had the first "
"consultation of"
msgstr ""
"Con mucho agrado me dirijo a uds. después de haber tenido la primera "
"consultade"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Job:"
msgstr "Trabajo:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Kinesiotherapy:"
msgstr "Kinesioterapia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Knitting:"
msgstr "Tejer:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Language level:"
msgstr "Nivel de lenguaje:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Language:"
msgstr "Lenguaje:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Last audiometry:"
msgstr "Ultima audiometría:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Last dental control:"
msgstr "Ultimo control dental:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Late dentition:"
msgstr "Dentición tardía:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "M"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "MEDICAMENT"
msgstr "MEDICAMENTO"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "MENT"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "MI:"
msgstr "IMC:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Maintenance of attentional focus:"
msgstr "Mantenimiento de foco atencional:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Means of mobilization:"
msgstr "Medios de movilización:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Mouth:"
msgstr "Boca:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "N"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Neumonology"
msgstr "Neumonología"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Neurologic System:"
msgstr "Sistema neurológico"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Neurological"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Neuropsychological assessment:"
msgstr "Evaluación neuropsicológica"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "No"
msgstr "No"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Nouse: ………"
msgstr "Nariz: ....."

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "O"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "ORL:"
msgstr "ORL:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "ORL: ……"
msgstr "ORL: ....."

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Occupational center:"
msgstr "Centro ocupacional:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Ophtalmologics: ????"
msgstr "Oftalmología: ?????"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Orientation:"
msgstr "Orientación:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Orthesis/prosthesis:"
msgstr "Ortesis/protesis:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Other:"
msgstr "Otro:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Others:"
msgstr "Otros:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "P"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Pediatric Model Form"
msgstr "Formulario de Modelo Pediatrico"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "People:"
msgstr "Gente:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Periodic physical examination:"
msgstr "Examinación física períodica"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Periodontal disease counseling:"
msgstr "Asesoría enfermedad periodental:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Phonoaudiology:"
msgstr "Fonoaudiología:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Pregnancy:"
msgstr "Embarazo:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Print"
msgstr "Imprimir"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Psychiatris"
msgstr "Psiquiatría:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Psycologist:"
msgstr "Psicología:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "RL health prof:"
msgstr "Profesional de salud de ORL:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Read:"
msgstr "Leer:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Recurrent infections:"
msgstr "Infecciones recurrentes:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Reflexes"
msgstr "Reflejos"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Requests:"
msgstr "Pedidos:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Respiratory System:"
msgstr "Sistema Respiratorio:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Risk factors or inestability on the patella:"
msgstr "Factores de riesgo o inestabilidad en la rotula:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Romberg sensitized:"
msgstr "Romberg sensibilizado:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Romberg simple:"
msgstr "Romber simple:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "S"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "START TREA"
msgstr "COMIENZO DE TRATAMIENTO"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Scissors:"
msgstr "Tijeras:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Scoliosis:"
msgstr "Escoliosis:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Sewing:"
msgstr "Coser:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Sign language:"
msgstr "Lenguage por señas:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Skate:"
msgstr "Patineta:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Skin and faneras:"
msgstr "Piel y faneras:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Sleep/wake cycle:"
msgstr "Ciclo de vigilia/sueño:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Snoring:"
msgstr "Ronquidos:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Space:"
msgstr "Espacio:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Sports and hobbies specialized center:"
msgstr "Centro especializado para deportes y hobbies:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Sports and hobbies:"
msgstr "Deportes y hobbies:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Static coordination"
msgstr "Coordinación estática"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Suggestions:"
msgstr "Sugerencias:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "System:"
msgstr "Sistema Neurológico:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Systolic pressure:"
msgstr "Presión sistólica:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "T"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid ""
"Thanking you again for your visit to our center, I greet you attentively, "
"remaining at your entire disposal"
msgstr ""
"Agradeciendo nuevamente su visita a nuestro centro, saludo a Uds "
"atentamente, quedando a su entera disposición"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Therapeutic accompaniment"
msgstr "Acompañamiento terapéutico"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Thyroid ultrasound:"
msgstr "Ultrasonido de tiroides:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Time:"
msgstr "Tiempo:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Tiptoes walk:"
msgstr "Marcha en puntas de pie:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Tone, strength and sensitivity:"
msgstr "Tono, fuerza y sensibilidad;"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Traumatology"
msgstr "Traumatología"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Tricipital:"
msgstr "Tricipital:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Urogenital System:"
msgstr "Sistema Urogenital:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "VACCINE"
msgstr "VACUNA"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Vaccines:"
msgstr "Vacunas:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Videocolonoscopy:"
msgstr "Videocolonoscopia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Vigil:"
msgstr "Vigil:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Vital signs:"
msgstr "Signos vitales:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "W"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Walk straight:"
msgstr "Marcha recta:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Would like a job?:"
msgstr "¿Quisiera trabajar?:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Write:"
msgstr "Escribe:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "X-Ray Cervical:"
msgstr "RX cervical:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "X-Ray Pavlov:"
msgstr "RX Pavlov:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "X-Ray panoramic hip:"
msgstr "RX  de cadera panorámica:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Yes"
msgstr "Sí"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "Zip closure:"
msgstr "Cremallera:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "and orthopedics"
msgstr "y ortopedia"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "atella:"
msgstr "Rotula:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "backgrou"
msgstr "antecedentes:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "ccupational therapy:"
msgstr "Terapia ocupacional:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "date"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "dontostomatology:"
msgstr "Odontoestomatología:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "ds:"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "e-mail"
msgstr "correo electrónico"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "ecal occult blood:"
msgstr "SOMF:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "eight:"
msgstr "Peso:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "enetic tests:"
msgstr "Pruebas genéticas:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "erinatal background"
msgstr "Antecedentes perinatales"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "etabolic anormalities:"
msgstr "Anormalidades metabólicas"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "herapeutic"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "info@fundacionlejeune.org.ar"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "institution"
msgstr "institución"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "institution"
msgstr "institución"

#, fuzzy
msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "intitution"
msgstr "institución"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "lycemia:"
msgstr "Glucemia:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "n"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "olysomnography:"
msgstr "Polisomnografía:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "ood overall condition."
msgstr "Buen estado general."

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "ood/personality:"
msgstr "Animo/personalidad:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "pnea:"
msgstr "Apnea:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "t:"
msgstr " "

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "upport and"
msgstr "Apoyo y"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "upport:"
msgstr "soporte terapeútico:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "ursing assistance:"
msgstr "Asistencia de enfermería/cuidador:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "www.fundacionlejeune.org/ar"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "ystem:"
msgstr " "

#, fuzzy
msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "–"
msgstr "-"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.pediatric.report:"
msgid "– Tel 351 - 4542601"
msgstr ""

#, fuzzy
msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid "* Requests and suggestions:"
msgstr "* Pedidos y sugerencias"

#, fuzzy
msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid "-"
msgstr "-"

#, fuzzy
msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid ":"
msgstr ":"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid "Av. Velez Sarsfield 1460 3° Piso, Córdoba,"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid "Córdoba, Argentina"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid "Fundación Jerome Lejeune Argentina"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid "Patient:"
msgstr ""

#, fuzzy
msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid "Print date"
msgstr "Fecha de impresión"

#, fuzzy
msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid "Requests"
msgstr "Pedidos"

#, fuzzy
msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid "Suggestions:"
msgstr "Sugerencias:"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid "info@fundacionlejeune.org.ar"
msgstr ""

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid "www.fundacionlejeune.org/ar"
msgstr ""

#, fuzzy
msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid "–"
msgstr "-"

msgctxt "report:gnuhealth.trisomy21.lejeune_attention.requests.report:"
msgid "– Tel 351 - 4542601"
msgstr ""

msgctxt "selection:gnuhealth.appointment,state:"
msgid "To confirm"
msgstr "A confirmar"

msgctxt "selection:gnuhealth.appointment,visit_type:"
msgid "First consultation"
msgstr "Primera consulta"

msgctxt "selection:gnuhealth.appointment,visit_type:"
msgid "Referral"
msgstr "Derivación"

msgctxt "selection:gnuhealth.appointment,visit_type:"
msgid "Second consultation"
msgstr "Segunda consulta"

msgctxt "selection:gnuhealth.barthel_evaluation,bathe:"
msgid "1. Independent"
msgstr "1. Independiente"

msgctxt "selection:gnuhealth.barthel_evaluation,bathe:"
msgid "2. Dependent"
msgstr "2. Dependiente"

msgctxt "selection:gnuhealth.barthel_evaluation,climb_stairs:"
msgid "1. Independent to go up and down stairs"
msgstr "1. Independiente para bajar y subir escalera"

msgctxt "selection:gnuhealth.barthel_evaluation,climb_stairs:"
msgid "2. Needs physical help or supervision to do it"
msgstr "2. Necesita ayuda física o supervisión para hacerlo"

msgctxt "selection:gnuhealth.barthel_evaluation,climb_stairs:"
msgid "3. Dependent"
msgstr "3. Dependiente"

msgctxt "selection:gnuhealth.barthel_evaluation,depositions:"
msgid "1. Normal continence"
msgstr ""
"1. Continencia normal, o es capaz de cuidarse de la sonda si tiene una "
"puesta"

msgctxt "selection:gnuhealth.barthel_evaluation,depositions:"
msgid ""
"2. Occasionally an episode of incontinence, or\n"
"need help to administer suppositories or enemas"
msgstr ""
"2. Un episodio diario como máximo de incontinencia, o necesita ayuda para "
"cuidar de la sonda"

msgctxt "selection:gnuhealth.barthel_evaluation,depositions:"
msgid "3. Incontenence"
msgstr "3. Incontinencia o incapaz de cambiarse la sonda"

msgctxt "selection:gnuhealth.barthel_evaluation,dressing:"
msgid ""
"1. Independent: able to put on and take off clothes\n"
",button up, tie shoes."
msgstr ""
"1. Independiente: capaz de ponerse y de quitarse la ropa, abotonarse, atarse"
" los zapatos "

msgctxt "selection:gnuhealth.barthel_evaluation,dressing:"
msgid "2. Need help (can do half)"
msgstr "2. Necesita ayuda(puede hacer la mitad)"

msgctxt "selection:gnuhealth.barthel_evaluation,dressing:"
msgid "3. Dependent"
msgstr "3. Dependiente"

msgctxt "selection:gnuhealth.barthel_evaluation,eating:"
msgid "1. Totally independent"
msgstr "1. Totalmente independiente"

msgctxt "selection:gnuhealth.barthel_evaluation,eating:"
msgid "2. Needs help to cut meat, bread, spread butter etc."
msgstr ""
"2. Necesita ayuda para cortar carne, el pan, extender la mantequilla etc"

msgctxt "selection:gnuhealth.barthel_evaluation,eating:"
msgid "3. Dependent"
msgstr "3. Dependiente"

msgctxt "selection:gnuhealth.barthel_evaluation,gender:"
msgid "Female"
msgstr "Femenino"

msgctxt "selection:gnuhealth.barthel_evaluation,gender:"
msgid "Female -> Male"
msgstr "Femenino -> Masculino"

msgctxt "selection:gnuhealth.barthel_evaluation,gender:"
msgid "Male"
msgstr "Masculino"

msgctxt "selection:gnuhealth.barthel_evaluation,gender:"
msgid "Male -> Female"
msgstr "Masculino -> Femenino"

msgctxt "selection:gnuhealth.barthel_evaluation,hygiene:"
msgid ""
"1. Independently to wash face, hands, comb, shave,\n"
"put on makeup, etc."
msgstr ""
"1. Independiente para lavarse la cara, las manos, peinarse, afeitarse, "
"maquillarse, etc"

msgctxt "selection:gnuhealth.barthel_evaluation,hygiene:"
msgid "2. Dependent"
msgstr "2. Dependiente"

msgctxt "selection:gnuhealth.barthel_evaluation,moving:"
msgid "1. Independent to go from chair to bed"
msgstr "1. Independiente para ir de la silla a la cama"

msgctxt "selection:gnuhealth.barthel_evaluation,moving:"
msgid "2. Minimal physical help/supervision to do so"
msgstr "2. Mínima ayuda física/supervisión para hacerlo"

msgctxt "selection:gnuhealth.barthel_evaluation,moving:"
msgid ""
"3. Needs a lot of help (1-2 people), but\n"
"is able to sit alone"
msgstr ""
"3. Necesita gran ayuda( 1-2 personas), pero es capaz de mantenerse sentado "
"solo "

msgctxt "selection:gnuhealth.barthel_evaluation,moving:"
msgid "4. Dependent, does not remain seated"
msgstr "4. Dependiente , no se mantiene sentado"

msgctxt "selection:gnuhealth.barthel_evaluation,score_dependency:"
msgid "Independency"
msgstr "Independiente"

msgctxt "selection:gnuhealth.barthel_evaluation,score_dependency:"
msgid "Low dependency"
msgstr "Baja dependencya"

msgctxt "selection:gnuhealth.barthel_evaluation,score_dependency:"
msgid "Moderate dependency"
msgstr "Moderada dependencia"

msgctxt "selection:gnuhealth.barthel_evaluation,score_dependency:"
msgid "Severe dependency"
msgstr "Severa dependencia"

msgctxt "selection:gnuhealth.barthel_evaluation,score_dependency:"
msgid "Total dependency"
msgstr "Total dependencia"

msgctxt "selection:gnuhealth.barthel_evaluation,state:"
msgid "Done"
msgstr "Realizado"

msgctxt "selection:gnuhealth.barthel_evaluation,state:"
msgid "In progress"
msgstr "En progreso"

msgctxt "selection:gnuhealth.barthel_evaluation,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt "selection:gnuhealth.barthel_evaluation,toilet_use:"
msgid ""
"1. Independent to go to the bathroom, take off\n"
"and put on clothes"
msgstr ""
"1. Independiente para ir al cuarto de aseo, quitarse y ponerse la ropa"

msgctxt "selection:gnuhealth.barthel_evaluation,toilet_use:"
msgid "2. Needs help going to the toilet, but cleans himself"
msgstr "2. Necesita ayuda para ir al retrete, pero se limpia solo"

msgctxt "selection:gnuhealth.barthel_evaluation,toilet_use:"
msgid "3. Dependent"
msgstr "3. Dependiente"

msgctxt "selection:gnuhealth.barthel_evaluation,urination:"
msgid ""
"1. Normal continence, or is able to take care of the\n"
"tube if it has one in place"
msgstr ""
"1. Continencia normal, o es capaz de cuidarse de la sonda si tiene una "
"puesta"

msgctxt "selection:gnuhealth.barthel_evaluation,urination:"
msgid ""
"2. A maximum of one episode of incontinence per\n"
"day, or needs help to care for the catheter"
msgstr ""
"2. Un episodio diario como máximo de incontinencia, o necesita ayuda para "
"cuidar de la sonda"

msgctxt "selection:gnuhealth.barthel_evaluation,urination:"
msgid "3. Incontinence or unable to change catheter"
msgstr "3. Incontinencia o incapaz de cambiarse la sonda"

msgctxt "selection:gnuhealth.barthel_evaluation,wander:"
msgid ""
"1. Independent, walks alone or with any type of\n"
"crutch (not a walker) 50 meters"
msgstr ""
"1. Independiente, camina solo o con cualquier tipo de muleta( no andador) 50"
" metros"

msgctxt "selection:gnuhealth.barthel_evaluation,wander:"
msgid "2. Needs physical help or supervision to walk 50 metres"
msgstr "2. Necesita ayuda física o supervisión para caminar 50 metros"

msgctxt "selection:gnuhealth.barthel_evaluation,wander:"
msgid "3. Independent in a wheelchair without assistance"
msgstr "3. Independiente en silla de ruedas sin ayuda"

msgctxt "selection:gnuhealth.barthel_evaluation,wander:"
msgid "4. Dependent"
msgstr "4. Dependiente"

msgctxt "selection:gnuhealth.calendar.create.appointment.start,visit_type:"
msgid "First consultation"
msgstr "Primera consulta"

msgctxt "selection:gnuhealth.calendar.create.appointment.start,visit_type:"
msgid "Referral"
msgstr "Derivación"

msgctxt "selection:gnuhealth.calendar.create.appointment.start,visit_type:"
msgid "Second consultation"
msgstr "Segunda consulta"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,cooking:"
msgid "1. Plans, prepares, and serves appropriate meals independently"
msgstr "1. Planea, prepara y sirve las comidas adecuadas con independencia"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,cooking:"
msgid "2. Prepares meals if given ingredients"
msgstr "2. Prepara las comidas si se le dan los ingredientes"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,cooking:"
msgid "3. Heats and serves meals but does not maintain an adequate diet"
msgstr "3. Calienta y sirve las comidas pero no mantiene una dieta adecuada"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,cooking:"
msgid "4. Needs food to be prepared and served"
msgstr "4. Necesita que se le prepare y sirva la comida"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,gender:"
msgid "Female"
msgstr "Femenino"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,gender:"
msgid "Female -> Male"
msgstr "Femenino -> Masculino"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,gender:"
msgid "Male"
msgstr "Masculino"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,gender:"
msgid "Male -> Female"
msgstr "Masculino -> Femenino"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,home_care:"
msgid ""
"1. Takes care of the house alone or with occasional help (eg Heavy work)"
msgstr "1. Cuida la casa sólo o con ayuda ocasional (ej. Trabajos pesados)"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,home_care:"
msgid "2. Does light household chores like washing dishes or making bed"
msgstr ""
"2. Realiza tareas domésticas ligeras como lavar los platos o hacer cama"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,home_care:"
msgid ""
"3. Does light housework but can't keep up an acceptable level of cleanliness"
msgstr ""
"3. Realiza tareas domésticas ligeras pero no puede mantener un nivel de "
"limpieza aceptable"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,home_care:"
msgid "4. Need help with all the housework"
msgstr "4. Necesita ayuda en todas las tareas de la casa"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,home_care:"
msgid "5. Does not participate in any household chores"
msgstr "5. No participa en ninguna tarea doméstica"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,laundry:"
msgid "1. Completely carry out personal laundry"
msgstr "1. Realiza completamente el lavado de ropa personal"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,laundry:"
msgid "2. Wash small items"
msgstr "2. Lava pequeñas prendas"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,laundry:"
msgid "3. Need someone else to do the washing"
msgstr "3. Necesita que otro se ocupe del lavado"

msgctxt ""
"selection:gnuhealth.lawton_brody_evaluation,liability_for_medication:"
msgid "1. It is responsible for the use of medication, dose and hours correct"
msgstr "1. Es responsable en el uso de la medicación, dosis y horas correctas"

msgctxt ""
"selection:gnuhealth.lawton_brody_evaluation,liability_for_medication:"
msgid ""
"2. Take the medication responsibly if it is prepared in advance in doses "
"ready"
msgstr ""
"2. Toma responsablemente la medicación si se le prepara con anticipación en "
"dosis preparadas"

msgctxt ""
"selection:gnuhealth.lawton_brody_evaluation,liability_for_medication:"
msgid "3. He is not able to take responsibility for his own medication"
msgstr "3. No es capaz de responsabilizarse de su propia medicación"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,money_handling:"
msgid ""
"1. Manages financial affairs independently, collects and know its income"
msgstr ""
"1. Maneja los asuntos financieros con independencia, recoge y conoce sus "
"ingresos"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,money_handling:"
msgid ""
"2. Manages daily expenses but needs help going to the bank, large expenses"
msgstr ""
"2. Maneja los gastos cotidianos pero necesita ayuda para ir al banco, "
"grandes gastos"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,money_handling:"
msgid "3. Unable to handle money"
msgstr "3. Incapaz de manejar el dinero"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,phone_use:"
msgid ""
"1. Use the telephone on its own initiative, search and dial numbers, etc."
msgstr ""
"1. Utiliza el teléfono a iniciativa propia, busca y marca los números, etc"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,phone_use:"
msgid "2. Dial a few well-known numbers"
msgstr "2. Marca unos cuantos números bien conocidos"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,phone_use:"
msgid "3. Answer the phone but don't dial"
msgstr "3. Contesta el teléfono pero no marca"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,phone_use:"
msgid "4. Does not use the phone"
msgstr "4. No usa el teléfono"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,score_dependency:"
msgid "Autonomous"
msgstr "Autonomo"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,score_dependency:"
msgid "Low"
msgstr "Bajo"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,score_dependency:"
msgid "Moderate"
msgstr "Moderado"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,score_dependency:"
msgid "Severe"
msgstr "Severo"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,score_dependency:"
msgid "Total"
msgstr "Total"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,shopping:"
msgid "1. Make all necessary purchases independently"
msgstr "1. Realiza todas las compras necesarias con independencia"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,shopping:"
msgid "2. Buy small things independently"
msgstr "2. Compra con independencia pequeñas cosas"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,shopping:"
msgid "3. You need company to make any purchase"
msgstr "3. Necesita compañía para realizar cualquier compra"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,shopping:"
msgid "4. Completely unable to shop"
msgstr "4. Completamente incapaz de ir de compras"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,state:"
msgid "Done"
msgstr "Realizado"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,state:"
msgid "In progress"
msgstr "En progreso"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,use_of_transportation:"
msgid "1. Travel independently on public transport or drive your car"
msgstr "1. Viaja con independencia en transportes públicos o conduce su coche"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,use_of_transportation:"
msgid ""
"2. Able to arrange own taxi transportation, but does not use other means of "
"transport"
msgstr ""
"2. Capaz de organizar su propio transporte en taxi, pero no usa otro medio "
"de transporte"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,use_of_transportation:"
msgid "3. Take public transport if accompanied by another person"
msgstr "3. Viaja en transportes públicos si le acompaña otra persona"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,use_of_transportation:"
msgid "4. Only travel by taxi or car with the help of others"
msgstr "4. Sólo viaja en taxi o automóvil con ayuda de otros"

msgctxt "selection:gnuhealth.lawton_brody_evaluation,use_of_transportation:"
msgid "5. Does not travel"
msgstr "5. No viaja"

msgctxt "selection:gnuhealth.neonatal_control,gender:"
msgid "Female"
msgstr "Femenino"

msgctxt "selection:gnuhealth.neonatal_control,gender:"
msgid "Female -> Male"
msgstr "Femenino -> Masculino"

msgctxt "selection:gnuhealth.neonatal_control,gender:"
msgid "Male"
msgstr "Masculino"

msgctxt "selection:gnuhealth.neonatal_control,gender:"
msgid "Male -> Female"
msgstr "Masculino -> Femenino"

msgctxt "selection:gnuhealth.neonatal_control,state:"
msgid "Done"
msgstr "Realizado"

msgctxt "selection:gnuhealth.neonatal_control,state:"
msgid "In progress"
msgstr "En progreso"

msgctxt "selection:gnuhealth.neonatal_control,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt "selection:gnuhealth.newborn,ft_screening:"
msgid "No"
msgstr "No"

msgctxt "selection:gnuhealth.newborn,ft_screening:"
msgid "Yes"
msgstr "Sí"

msgctxt "selection:gnuhealth.patient.ecg,ecg_age:"
msgid "20 years"
msgstr "20 años"

msgctxt "selection:gnuhealth.patient.ecg,ecg_age:"
msgid "40 years"
msgstr "40  años"

msgctxt "selection:gnuhealth.patient.ecg,ecg_age:"
msgid "Every 5 years"
msgstr "Cada 5 años"

msgctxt "selection:gnuhealth.patient.evaluation,state:"
msgid "To confirm"
msgstr "A confirmar"

msgctxt "selection:gnuhealth.patient.evaluation,visit_type:"
msgid "First consultation"
msgstr "Primera consulta"

msgctxt "selection:gnuhealth.patient.evaluation,visit_type:"
msgid "Referral"
msgstr "Derivación"

msgctxt "selection:gnuhealth.patient.evaluation,visit_type:"
msgid "Second consultation"
msgstr "Segunda consulta"

msgctxt "selection:gnuhealth.surgical_history,surgery_kind:"
msgid "Cardiovascular"
msgstr "Cardiovascular"

msgctxt "selection:gnuhealth.surgical_history,surgery_kind:"
msgid "Dermatology"
msgstr "Dermatología"

msgctxt "selection:gnuhealth.surgical_history,surgery_kind:"
msgid "ENT"
msgstr "Otorrinolaringología"

msgctxt "selection:gnuhealth.surgical_history,surgery_kind:"
msgid "Endocrine"
msgstr "Endócrino"

msgctxt "selection:gnuhealth.surgical_history,surgery_kind:"
msgid "Gastrointestinal"
msgstr "Gastrointestinal"

msgctxt "selection:gnuhealth.surgical_history,surgery_kind:"
msgid "Gynecology"
msgstr "Ginecología"

msgctxt "selection:gnuhealth.surgical_history,surgery_kind:"
msgid "Neurovascular"
msgstr "Neurovacular"

msgctxt "selection:gnuhealth.surgical_history,surgery_kind:"
msgid "Odontostamolotology"
msgstr "Odontoestomatología"

msgctxt "selection:gnuhealth.surgical_history,surgery_kind:"
msgid "Oncohematology"
msgstr "Oncohematología"

msgctxt "selection:gnuhealth.surgical_history,surgery_kind:"
msgid "Ophtalic"
msgstr "Oftálmico"

msgctxt "selection:gnuhealth.surgical_history,surgery_kind:"
msgid "Pulmological"
msgstr "Neumología"

msgctxt "selection:gnuhealth.surgical_history,surgery_kind:"
msgid "Traumatology"
msgstr "Traumatología"

msgctxt "selection:gnuhealth.surgical_history,surgery_kind:"
msgid "Uronefrology"
msgstr "Uronefrología"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,anticonceptive:"
msgid "Contraceptive injection"
msgstr "Inyectable anticonceptivo"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,anticonceptive:"
msgid "Female condom"
msgstr "Preservativo femenino"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,anticonceptive:"
msgid "Female sterilisation"
msgstr "Esterilización femenina"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,anticonceptive:"
msgid "Fertility cycle awareness"
msgstr "Conciencia del ciclo de fertilidad"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,anticonceptive:"
msgid "Intra-uterine device"
msgstr "DIU"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,anticonceptive:"
msgid "Male condom"
msgstr "Preservativo masculino"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,anticonceptive:"
msgid "None"
msgstr "Ninguno"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,anticonceptive:"
msgid "Pill / Minipill"
msgstr "Pastilla/Minipastilla"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,anticonceptive:"
msgid "Skin Patch"
msgstr "Parche de piel"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,anticonceptive:"
msgid "Vasectomy"
msgstr "Vasectomía"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,anticonceptive:"
msgid "Withdrawal method"
msgstr "Método del retiro"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,celiac_screening:"
msgid "+"
msgstr "+"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,celiac_screening:"
msgid "-"
msgstr "-"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,hla_dq2_dq8:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,hla_dq2_dq8:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,oea:"
msgid "Do not pass test"
msgstr "No pasa el test"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,oea:"
msgid "Pass test"
msgstr "Pasa el test"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.anamnesis,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.barthel,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.behaviour,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,kinesiotherapy:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,kinesiotherapy:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,nursing_assistance:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,nursing_assistance:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,occ_therapy:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,occ_therapy:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,phonoaudiology:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,phonoaudiology:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,psychiatrist:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,psychiatrist:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,psycologist:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,psycologist:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,therapeutic_acc:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.current_therapies,therapeutic_acc:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.laboratory,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.lawton_brody,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.medication,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,ft_screening:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,ft_screening:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.pediatrics,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,indicator:"
msgid "Cephalic perimeter for age"
msgstr "Perímetro cefálico para la edad"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,indicator:"
msgid "Length/height for age"
msgstr "Altura para la edad"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,indicator:"
msgid "Weight for age"
msgstr "Peso para la edad"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_diadochokinesia:"
msgid "Failed"
msgstr "Fallido/da"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_diadochokinesia:"
msgid "Succeded"
msgstr "Exitoso/a"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_index_nose_test:"
msgid "Failed"
msgstr "Fallido/da"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_index_nose_test:"
msgid "Succeded"
msgstr "Exitoso/a"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_walk_heel:"
msgid "Failed"
msgstr "Fallido/da"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_walk_heel:"
msgid "Succeded"
msgstr "Exitoso/a"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_walk_straight:"
msgid "Failed"
msgstr "Fallido/da"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_walk_straight:"
msgid "Succeded"
msgstr "Exitoso/a"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_walk_tiptoes:"
msgid "Failed"
msgstr "Fallido/da"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_dyn_coord_walk_tiptoes:"
msgid "Succeded"
msgstr "Exitoso/a"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_aquiliano:"
msgid "Absent"
msgstr "Ausente"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_aquiliano:"
msgid "Present"
msgstr "Presente"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_bicipital:"
msgid "Absent"
msgstr "Ausente"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_bicipital:"
msgid "Present"
msgstr "Presente"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_patella:"
msgid "Absent"
msgstr "Ausente"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_patella:"
msgid "Present"
msgstr "Presente"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_tricipital:"
msgid "Absent"
msgstr "Ausente"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_reflex_tricipital:"
msgid "Present"
msgstr "Presente"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_static_coord_romberg_sensitized:"
msgid "Negative"
msgstr "Negativo"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_static_coord_romberg_sensitized:"
msgid "Positive"
msgstr "Positivo"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_static_coord_romberg_simple:"
msgid "Negative"
msgstr "Negativo"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,neuro_static_coord_romberg_simple:"
msgid "Positive"
msgstr "Positivo"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_art_limit:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_art_limit:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_heel_valgus:"
msgid "Bilateral"
msgstr "Bilateral"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_heel_valgus:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_heel_valgus:"
msgid "Unilateral"
msgstr "Unilateral"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_pain:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_pain:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_patella_inestability:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_patella_inestability:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_scoliosis:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,oam_system_scoliosis:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.physical_exam,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,alcohol_frequency:"
msgid "Daily"
msgstr "Diariamente"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,alcohol_frequency:"
msgid "Social"
msgstr "Social"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,alcohol_frequency:"
msgid "Weekly"
msgstr "Semanalmente"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,id_level_legend:"
msgid "Above average"
msgstr "Por encima del promedio"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,id_level_legend:"
msgid "Mild mental retardation"
msgstr "Retraso mental leve"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,id_level_legend:"
msgid "Moderate mental retardation"
msgstr "Retraso mental moderado"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,id_level_legend:"
msgid "Severe mental retardation"
msgstr "Retraso mental severo"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,write_:"
msgid "Alone"
msgstr "Solo/a"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment,write_:"
msgid "With support"
msgstr "Con ayuda"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.schooling,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt "selection:gnuhealth.trisomy21.create_lejeune_attention.start,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt ""
"selection:gnuhealth.trisomy21.create_lejeune_attention.vaccines,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,create_du:"
msgid "Create"
msgstr "Crear"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,create_du:"
msgid "Use a created one"
msgstr "Usar uno ya creado"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,gender:"
msgid "Female"
msgstr "Femenino"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,gender:"
msgid "Female -> Male"
msgstr "Femenino -> Masculino"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,gender:"
msgid "Male"
msgstr "Masculino"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,gender:"
msgid "Male -> Female"
msgstr "Masculino -> Femenino"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,gender:"
msgid "Other"
msgstr "Otro"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Aunt"
msgstr "Tía"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Brother"
msgstr "Hermano"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Cousin"
msgstr "Primo/a"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Father"
msgstr "Padre"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Grandfather"
msgstr "Abuelo"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Grandmother"
msgstr "Abuela"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Main patient"
msgstr "Paciente principal"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Mother"
msgstr "Madre"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Nephew"
msgstr "Sobrino"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Niece"
msgstr "Sobrina"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Other"
msgstr "Otro"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Sister"
msgstr "Hermana"

msgctxt "selection:gnuhealth.trisomy21.create_patient.start,role:"
msgid "Uncle"
msgstr "Tío"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,alcohol_frequency:"
msgid "Daily"
msgstr "Diariamente"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,alcohol_frequency:"
msgid "Social"
msgstr "Social"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,alcohol_frequency:"
msgid "Weekly"
msgstr "Semanalmente"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,anticonceptive:"
msgid "Contraceptive injection"
msgstr "Inyectable anticonceptivo"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,anticonceptive:"
msgid "Female condom"
msgstr "Preservativo femenino"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,anticonceptive:"
msgid "Female sterilisation"
msgstr "Esterilización femenina"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,anticonceptive:"
msgid "Fertility cycle awareness"
msgstr "Conocimiento del ciclo menstrual"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,anticonceptive:"
msgid "Intra-uterine device"
msgstr "DIU"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,anticonceptive:"
msgid "Male condom"
msgstr "Preservativo masculino"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,anticonceptive:"
msgid "None"
msgstr "Ninguno"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,anticonceptive:"
msgid "Pill / Minipill"
msgstr "Píldora / Minipíldora"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,anticonceptive:"
msgid "Skin Patch"
msgstr "Parche dérmico"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,anticonceptive:"
msgid "Vasectomy"
msgstr "Vasectomía"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,anticonceptive:"
msgid "Withdrawal method"
msgstr "Método del retiro"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,celiac_screening:"
msgid "+"
msgstr "+"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,celiac_screening:"
msgid "-"
msgstr "-"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,gender:"
msgid "Female"
msgstr "Femenino"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,gender:"
msgid "Female -> Male"
msgstr "Femenino -> Masculino"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,gender:"
msgid "Male"
msgstr "Masculino"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,gender:"
msgid "Male -> Female"
msgstr "Masculino -> Femenino"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,hla_dq2_dq8:"
msgid "No"
msgstr "No"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,hla_dq2_dq8:"
msgid "Yes"
msgstr "Sí"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,id_level_legend:"
msgid "Above average"
msgstr "Por encima del promedio"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,id_level_legend:"
msgid "Mild mental retardation"
msgstr "Retraso mental leve"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,id_level_legend:"
msgid "Moderate mental retardation"
msgstr "Retraso mental moderado"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,id_level_legend:"
msgid "Severe mental retardation"
msgstr "Retraso mental severo"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,kinesiotherapy:"
msgid "No"
msgstr "No"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,kinesiotherapy:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_diadochokinesia:"
msgid "Failed"
msgstr "Fallido/da"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_diadochokinesia:"
msgid "Succeded"
msgstr "Exitoso/a"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_index_nose_test:"
msgid "Failed"
msgstr "Fallido/da"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_index_nose_test:"
msgid "Succeded"
msgstr "Exitoso/a"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_walk_heel:"
msgid "Failed"
msgstr "Fallido/da"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_walk_heel:"
msgid "Succeded"
msgstr "Exitoso/a"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_walk_straight:"
msgid "Failed"
msgstr "Fallido/da"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_walk_straight:"
msgid "Succeded"
msgstr "Exitoso/a"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_walk_tiptoes:"
msgid "Failed"
msgstr "Fallido/da"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_dyn_coord_walk_tiptoes:"
msgid "Succeded"
msgstr "Exitoso/a"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_aquiliano:"
msgid "Absent"
msgstr "Ausente"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_aquiliano:"
msgid "Present"
msgstr "Presente"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_bicipital:"
msgid "Absent"
msgstr "Ausente"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_bicipital:"
msgid "Present"
msgstr "Presente"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_patella:"
msgid "Absent"
msgstr "Ausente"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_patella:"
msgid "Present"
msgstr "Presente"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_tricipital:"
msgid "Absent"
msgstr "Ausente"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_reflex_tricipital:"
msgid "Present"
msgstr "Presente"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_static_coord_romberg_sensitized:"
msgid "Negative"
msgstr "Negativo"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_static_coord_romberg_sensitized:"
msgid "Positive"
msgstr "Positivo"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_static_coord_romberg_simple:"
msgid "Negative"
msgstr "Negativo"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,neuro_static_coord_romberg_simple:"
msgid "Positive"
msgstr "Positivo"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,nursing_assistance:"
msgid "No"
msgstr "No"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,nursing_assistance:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,oam_system_art_limit:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,oam_system_art_limit:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,oam_system_heel_valgus:"
msgid "Bilateral"
msgstr "Bilateral"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,oam_system_heel_valgus:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,oam_system_heel_valgus:"
msgid "Unilateral"
msgstr "Unilateral"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,oam_system_pain:"
msgid "No"
msgstr "No"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,oam_system_pain:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,oam_system_patella_inestability:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,oam_system_patella_inestability:"
msgid "Yes"
msgstr "Sí"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,oam_system_scoliosis:"
msgid "No"
msgstr "No"

msgctxt ""
"selection:gnuhealth.trisomy21.lejeune_attention,oam_system_scoliosis:"
msgid "Yes"
msgstr "Sí"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,occ_therapy:"
msgid "No"
msgstr "No"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,occ_therapy:"
msgid "Yes"
msgstr "Sí"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,oea:"
msgid "Do not pass test"
msgstr "No pasa el test"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,oea:"
msgid "Pass test"
msgstr "Pasa el test"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,phonoaudiology:"
msgid "No"
msgstr "No"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,phonoaudiology:"
msgid "Yes"
msgstr "Sí"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,psychiatrist:"
msgid "No"
msgstr "No"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,psychiatrist:"
msgid "Yes"
msgstr "Sí"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,psycologist:"
msgid "No"
msgstr "No"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,psycologist:"
msgid "Yes"
msgstr "Sí"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,state:"
msgid "Done"
msgstr "Realizado"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,state:"
msgid "In progress"
msgstr "En progreso"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,state:"
msgid "Signed"
msgstr "Firmado"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,therapeutic_acc:"
msgid "No"
msgstr "No"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,therapeutic_acc:"
msgid "Yes"
msgstr "Sí"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,write_:"
msgid "Alone"
msgstr "Solo"

msgctxt "selection:gnuhealth.trisomy21.lejeune_attention,write_:"
msgid "With support"
msgstr "Con apoyo"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Altered / current String"
msgstr "Alterado / cadena actual"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Close Evaluation"
msgstr "Cerrar evaluación"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Close and Sign evaluation ?"
msgstr "¿Cerrar y firmar la evaluación?"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Digital Signature"
msgstr "Firma Digital"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Discharge"
msgstr "Cerrar"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Discharge patient / End this evaluation ?"
msgstr "Cerrar / Finalizar esta evaluación de paciente?"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Discharge patient / end this evaluation"
msgstr "Cerrar / finalizar esta evaluación de paciente"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Document validation"
msgstr "Validación de documento"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Hashes"
msgstr "Hashes"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Info"
msgstr "Info"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Original String"
msgstr "Cadena Original"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Scoring"
msgstr "Puntuación"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Sign this patient evaluation"
msgstr "Firmar esta evaluación del paciente"

msgctxt "view:gnuhealth.barthel_evaluation:"
msgid "Validation"
msgstr "Validación "

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Altered / current String"
msgstr "Alterado / cadena actual"

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Close Evaluation"
msgstr "Cerrar evaluación"

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Close and Sign evaluation ?"
msgstr "¿Cerrar y firmar la evaluación?"

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Digital Signature"
msgstr "Firma Digital"

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Discharge"
msgstr "Cerrar"

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Discharge patient / End this evaluation ?"
msgstr "Cerrar / Finalizar esta evaluación de paciente?"

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Discharge patient / end this evaluation"
msgstr "Cerrar / finalizar esta evaluación de paciente"

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Document validation"
msgstr "Validación de documento"

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Hashes"
msgstr "Hashes"

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Info"
msgstr "Info"

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Original String"
msgstr "Cadena Original"

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Scoring"
msgstr "Puntuación"

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Sign this patient evaluation"
msgstr "Firmar esta evaluación del paciente"

msgctxt "view:gnuhealth.lawton_brody_evaluation:"
msgid "Validation"
msgstr "Validación "

msgctxt "view:gnuhealth.newborn:"
msgid "Controlled pregnancy - observations"
msgstr "Embarazo controlado - observaciones"

msgctxt "view:gnuhealth.newborn:"
msgid "Ft. screening check - observations"
msgstr "Chequeo de screening de primter trimestre - observaciones"

msgctxt "view:gnuhealth.newborn:"
msgid "Other findings"
msgstr "Otros hallazgos"

msgctxt "view:gnuhealth.newborn:"
msgid "Test metabolic - observations"
msgstr "Test metabólico - observaciones"

msgctxt "view:gnuhealth.patient:"
msgid "Appointment report"
msgstr "Reporte de citas"

msgctxt "view:gnuhealth.surgical_history:"
msgid "Notes"
msgstr "Notas"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Andrology"
msgstr "Andrología"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Associated records"
msgstr "Registros asociados"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Bowel habit"
msgstr "Hábito intestinal"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Cardiovascular"
msgstr "Cardiovascular"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Conditions"
msgstr "Condiciones"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Conditions and files"
msgstr "Condiciones y archivos"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Dermatology"
msgstr "Dermatología"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Disability assessment"
msgstr "Evaluaciones de discapacidad"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Early menopause symptoms"
msgstr "Síntomas de menopausia precoz"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Endocrinology"
msgstr "Endocrinología"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Fecal occult blood"
msgstr "Sangre oculta en materia fecal"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Feeding"
msgstr "Alimentación"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Feeding intolerance"
msgstr "Intolerancia a la comida"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Fertility"
msgstr "Fertilidad"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Gastrointestinal"
msgstr "Gastrointestinal"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Gynecology"
msgstr "Ginecología"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Hospitalizations"
msgstr "Hospitalizaciones"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Imaging Result"
msgstr "Resultado de imagenes"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Main info"
msgstr "Información principal"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Metabolic anormalities"
msgstr "Anormalidades metabólicas"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Neumonology"
msgstr "Neumonología"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Neurological"
msgstr "Neurologico"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Observations"
msgstr "Observaciones"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Odontostomatology"
msgstr "Odontostomatología"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Oncohematology"
msgstr "Oncohematología"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Ophthalmology"
msgstr "Oftalmología"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Orthopedics and traumatology"
msgstr "Ortopedía y traumatología"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Orthosis"
msgstr "Ortesis"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Otorhinolaryngology"
msgstr "Otorrinolaringología"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Patella inestability"
msgstr "Inestabilidad de la rótula"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Periodontal disease counseling"
msgstr "Asesoría enfermedad periodental"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Psychiatric"
msgstr "Psiquiatrico"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Recurrent infections"
msgstr "Infecciones recurrentes"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Reumatology"
msgstr "Reumatología"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Scoliosis"
msgstr "Escoliosis"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Screening Ca. rectum"
msgstr "Screening Ca. recto"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Sleep/wake cycle"
msgstr "Ciclo de vigilia/sueño"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Surgical history"
msgstr "Historial de cirugías"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "T4"
msgstr "T4"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Thyroid ultrasound - observations"
msgstr "Ultrasonido de tiroides - observaciones"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Uronefrology"
msgstr "Uronefrología"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.anamnesis:"
msgid "Videocolonoscopy"
msgstr "Videocolonoscopia"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.barthel:"
msgid "Associated_records"
msgstr "Registros asociados"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.barthel:"
msgid "Barthel Evaluation"
msgstr "Evaluación de Barthel"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.behaviour:"
msgid "Accompanying Persons"
msgstr "Personas acompañantes"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.behaviour:"
msgid "Associated records"
msgstr "Registros asociados"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.behaviour:"
msgid "Behaviour"
msgstr "Comportamiento"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.current_therapies:"
msgid "Associated records"
msgstr "Registros asociados"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.current_therapies:"
msgid "Current Therapies"
msgstr "Terapias Actuales"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.current_therapies:"
msgid "Nursing assistance"
msgstr "Asistencia de enfermería"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.current_therapies:"
msgid "Observations"
msgstr "Observaciones"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.laboratory:"
msgid "Associated records"
msgstr "Registros asociados"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.laboratory:"
msgid "Laboratory"
msgstr "Laboratorio"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.lawton_brody:"
msgid "Associated records"
msgstr "Registros asociados"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.lawton_brody:"
msgid "Lawton-Brody Evaluations"
msgstr "Evaluaciones de Lawton-Brody"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.medication:"
msgid "Associated records"
msgstr "Registros asociados"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.medication:"
msgid "Medication"
msgstr "Medicación"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.pediatrics:"
msgid "15 days control"
msgstr "Control de 15 días"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.pediatrics:"
msgid "30 days control"
msgstr "Control de 30 días"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.pediatrics:"
msgid "Apgar scores"
msgstr "Puntuación APGAR"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.pediatrics:"
msgid "Associated records"
msgstr "Registros asociados"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.pediatrics:"
msgid "Controlled pregnancy - observations"
msgstr "Embarazo controlado - observaciones"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.pediatrics:"
msgid "Ft. screening check - observations"
msgstr "Chequeo de screening de primer trimestre - observaciones"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.pediatrics:"
msgid "Internment/hospitalization"
msgstr "Internación/hospitalización"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.pediatrics:"
msgid "Main Info"
msgstr "Información Principal"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.pediatrics:"
msgid "Test metabolic - Observations"
msgstr "Test metabólico - observaciones"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Aquiliano"
msgstr "Aquiliano"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Associated records"
msgstr "Registros asociados"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Bicipital"
msgstr "Bicipital"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Cardiovascular"
msgstr "Cardiovascular"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Digestive"
msgstr "Digestivo"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Dynamic coordination"
msgstr "Coordinación dinámica"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Ears"
msgstr "Oídos"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Growth lines"
msgstr "Lineas de crecimiento"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Language"
msgstr "Lenguaje"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Lympha"
msgstr "Linfático"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Measurements"
msgstr "Medidas"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Mouth"
msgstr "Boca"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Neurologic"
msgstr "Neurológico"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Notes"
msgstr "Notas"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Orientation"
msgstr "Orientación"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "OsteoArthroMuscular"
msgstr "OsteoArtroMuscular"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Other"
msgstr "Otro"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Patella"
msgstr "Rotuliano"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "People"
msgstr "Personas"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Reflex"
msgstr "Reflejos"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Respiratory"
msgstr "Respiratorio"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Skin"
msgstr "Piel"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Space"
msgstr "Espacio"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Static coordination"
msgstr "Coordinación estática"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Time"
msgstr "Tiempo"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Tone, strength and sensitivity"
msgstr "Tono, fuerza y sensibilidad"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Tricipital"
msgstr "Tricipital"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Urologic"
msgstr "Urológico"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Vigil"
msgstr "Vigil"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.physical_exam:"
msgid "Vital signs"
msgstr "Signos vitales"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment:"
msgid "Associated records"
msgstr "Registros asociados"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment:"
msgid "Communication"
msgstr "Comunicación"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment:"
msgid "Fine motor activity"
msgstr "Actividad motora fina"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment:"
msgid "Language level"
msgstr "Nivel de lenguaje"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment:"
msgid "Maintenance of attentional focus"
msgstr "Mantenimiento de foco atencional"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment:"
msgid "Means of mobilization"
msgstr "Medios de movilización"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment:"
msgid "Mood / personality"
msgstr "Ánimo / personalidad"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment:"
msgid "Psyc assessment"
msgstr "Evaluacion psicológica"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment:"
msgid "Sociability"
msgstr "Sociabilidad"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.psyc_assessment:"
msgid "Sports and hobbies"
msgstr "Deportes y hobbies"

msgctxt ""
"view:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions:"
msgid "Associated records"
msgstr "Registros asociados"

msgctxt ""
"view:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions:"
msgid "Request and Suggestions"
msgstr "Pedidos y Sugerencias"

msgctxt ""
"view:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions:"
msgid "Requests"
msgstr "Pedidos"

msgctxt ""
"view:gnuhealth.trisomy21.create_lejeune_attention.requests_suggestions:"
msgid "Suggestions"
msgstr "Sugerencias"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.schooling:"
msgid "Associated records"
msgstr "Registros asociados"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.schooling:"
msgid "SES assessment"
msgstr "Evaluación socioecónomica"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.start:"
msgid "Associated records"
msgstr "Registros asociados"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.start:"
msgid "Relatives history"
msgstr "Antecedentes familiares"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.vaccines:"
msgid "Associated records"
msgstr "Registros asociados"

msgctxt "view:gnuhealth.trisomy21.create_lejeune_attention.vaccines:"
msgid "Vaccines"
msgstr "Vacunas"

msgctxt "view:gnuhealth.trisomy21.create_patient.start:"
msgid "Address"
msgstr "Dirección/Contacto"

msgctxt "view:gnuhealth.trisomy21.create_patient.start:"
msgid "Contact mechanisms"
msgstr "Mecánismos de contacto"

msgctxt "view:gnuhealth.trisomy21.create_patient.start:"
msgid "Family"
msgstr "Familia"

msgctxt "view:gnuhealth.trisomy21.create_patient.start:"
msgid "Nationality"
msgstr "Nacionalidad"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Altered / current String"
msgstr "Alterado / cadena actual"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Anamnesis"
msgstr "Anamnesis"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Andrology"
msgstr "Andrología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Andropause signs"
msgstr "Signos de andropausia"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Aquiliano"
msgstr "Aquiliano"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Behaviour"
msgstr "Comportamiento"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Bicipital"
msgstr "Bicipital"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Bowel habit"
msgstr "Habito intestinal"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Cardiovascular"
msgstr "Cardiovascular"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Close Evaluation"
msgstr "Cerrar evaluación"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Close and Sign evaluation ?"
msgstr "¿Cerrar y firmar la evaluación?"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Communication"
msgstr "Comunicación"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Current therapies"
msgstr "Terapias actuales"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Dermatology"
msgstr "Dermatología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Digestive"
msgstr "Digestivo"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Digital Signature"
msgstr "Firma Digital"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Disability"
msgstr "Discapacidad"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Discharge"
msgstr "Cerrar"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Discharge patient / End this evaluation ?"
msgstr "Cerrar / Finalizar esta evaluación de paciente?"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Discharge patient / end this evaluation"
msgstr "Cerrar / finalizar esta evaluación de paciente"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Document validation"
msgstr "Validación de documento"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Dynamic coordination"
msgstr "Coordinación dinámica"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Early menopause symptoms"
msgstr "Síntomas de menopausia precoz"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Ears"
msgstr "Orejas"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Endocrinology"
msgstr "Endocrinología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Fecal occult blood"
msgstr "Sangre oculta en materia fecal"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Feeding"
msgstr "Alimentación"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Feeding intolerance"
msgstr "Intolerancias"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Fertility"
msgstr "Fertilidad"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Fertility"
msgstr "Fertilidad"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Fine motor activity"
msgstr "Actividad motora fina"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Gastrointestinal"
msgstr "Gastrointestinal"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Gynecology"
msgstr "Ginecología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Gynecology"
msgstr "Ginecología"

#, fuzzy
msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Hashes"
msgstr "Hashes"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Hospitalizations"
msgstr "Hospitalizaciones"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Language"
msgstr "Lenguaje"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Language level"
msgstr "Nivel de lenguaje"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Lympha"
msgstr "Linfático"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Maintenance of attentional focus"
msgstr "Mantenimiento de foco atencional"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Means of mobilization"
msgstr "Medios de movilización"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Metabolic anormalities"
msgstr "Anormalidades metabólicas"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Mood / personality"
msgstr "Animo / personalidad"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Mouth"
msgstr "Boca"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Neumonology"
msgstr "Neumonologia"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Neurologic"
msgstr "Neurologico"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Neurological"
msgstr "Neurológico"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Neurological"
msgstr "Neurológico"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Notes"
msgstr "Notas"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Nursing assistance"
msgstr "Asistencia de enfermería"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Observations"
msgstr "Observaciones"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Odontostomatology"
msgstr "Odontoestomatología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Oncohematology"
msgstr "Oncohematología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Oncohematology"
msgstr "Oncohematología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Ophthalmology"
msgstr "Oftalmología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Orientation"
msgstr "Orientación"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Original String"
msgstr "Cadena Original"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Orthopedics and traumatology"
msgstr "Ortopedía y traumatología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Orthopedics and traumatology"
msgstr "Ortopedía y traumatología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Orthosis"
msgstr "Ortesis"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "OsteoArthroMuscular"
msgstr "OsteoArtroMuscular"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Other"
msgstr "Otro"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Otorhinolaryngology"
msgstr "Otorrinolaringología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Patella"
msgstr "Rotula"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Patella inestability"
msgstr "Inestabilidad de rotula"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "People"
msgstr "Gente"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Periodontal disease counseling"
msgstr "Asesoría enfermedad periodental"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Physical exam"
msgstr "Examén físico"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Psychiatric"
msgstr "Psiquiatrico"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Psycological assessment"
msgstr "Evaluación psicológica"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Recurrent infections"
msgstr "Infecciones recurrentes"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Reflex"
msgstr "Reflejos"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Requests"
msgstr "Pedidos"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Requests and Suggestions"
msgstr "Pedidos y Sugerencias"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Respiratory"
msgstr "Respiratorio"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Reumatology"
msgstr "Reumatología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Scoliosis"
msgstr "Escoliosis"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Screening Ca. rectum"
msgstr "Screening Cancer recto"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Sign this patient evaluation"
msgstr "Firmar esta evaluación del paciente"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Skin"
msgstr "Piel"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Sleep/wake cycle"
msgstr "Ciclo de vigilia/sueño"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Sociability"
msgstr "Sociabilidad"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Space"
msgstr "Espacio"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Sports and hobbies"
msgstr "Deportes y hobbies"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Static coordination"
msgstr "Coordinación estática"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Suggestions"
msgstr "Sugerencias"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "T4"
msgstr "T4"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Thyroid ultrasound - observations"
msgstr "Ultrasonido de tiroides - observaciones"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Time"
msgstr "Tiempo"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Tone, strength and sensitivity"
msgstr "Tono, fuerza y sensibilidad"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Tricipital"
msgstr "Tricipital"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Urologic"
msgstr "Urológico"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Uronefrology"
msgstr "Uronefrología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Uronefrology"
msgstr "Uronefrología"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Validation"
msgstr "Validación"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Videocolonoscopy"
msgstr "Videocolonoscopia"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Vigil"
msgstr "Vigilia"

msgctxt "view:gnuhealth.trisomy21.lejeune_attention:"
msgid "Vital signs"
msgstr "Signos vitales"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.appointment_report.wizard,start,end:"
msgid "Cancel"
msgstr "Cancelar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.appointment_report.wizard,start,open_:"
msgid "Open"
msgstr "Abrir"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,anamnesis,anamnesis2pediatrics:"
msgid "Previous"
msgstr "Anterior"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,anamnesis,anamnesis2vaccines:"
msgid "Next"
msgstr "Siguiente"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,anamnesis,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,barthel,barthel2lawton_brody:"
msgid "Next"
msgstr "Siguiente"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,barthel,barthel2psy_assessment:"
msgid "Previous"
msgstr "Anterior"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,barthel,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,behaviour,behaviour2lawton_brody:"
msgid "Previous"
msgstr "Anterior"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,behaviour,behaviour2requests_suggestions:"
msgid "Next"
msgstr "Siguiente"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,behaviour,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,current_therapies,current_therapies2medication:"
msgid "Previous"
msgstr "Anterior"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,current_therapies,current_therapies2physical_exam:"
msgid "Next"
msgstr "Siguiente"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,current_therapies,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,laboratory,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,laboratory,lab2end_sign:"
msgid "End and sign"
msgstr "Finalizar y firmar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,laboratory,lab2reqs_sugs:"
msgid "Previous"
msgstr "Anterior"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,lawton_brody,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,lawton_brody,lawton_brody2barthel:"
msgid "Previous"
msgstr "Anterior"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,lawton_brody,lawton_brody2behaviour:"
msgid "Next"
msgstr "Siguiente"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,medication,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,medication,medication2current_therapies:"
msgid "Next"
msgstr "Siguiente"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,medication,medication2vaccines:"
msgid "Previous"
msgstr "Anterior"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,pediatrics,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,pediatrics,pediatrics2anamnesis:"
msgid "Next"
msgstr "Siguiente"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,pediatrics,pediatrics2start:"
msgid "Previous"
msgstr "Anterior"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,physical_exam,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,physical_exam,physical_exam2current_therapies:"
msgid "Previous"
msgstr "Anterior"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,physical_exam,physical_exam2schooling:"
msgid "Next"
msgstr "Siguiente"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,psyc_assessment,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,psyc_assessment,psyc2barthel:"
msgid "Next"
msgstr "Siguiente"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,psyc_assessment,psyc2schooling:"
msgid "Previous"
msgstr "Anterior"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,requests_suggestions,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,requests_suggestions,reqs_suggs2behaviour:"
msgid "Previous"
msgstr "Anterior"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,requests_suggestions,reqs_suggs2laboratory:"
msgid "Next"
msgstr "Siguiente"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,schooling,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,schooling,schooling2physical_exam:"
msgid "Previous"
msgstr "Anterior"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,schooling,schooling2psyc_assessment:"
msgid "Next"
msgstr "Siguiente"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,start,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,start,start2pediatrics:"
msgid "Next"
msgstr "Siguiente"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,vaccines,end:"
msgid "Close"
msgstr "Cerrar"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,vaccines,vaccines2anamnesis:"
msgid "Previous"
msgstr "Anterior"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_lejeune_attention.wizard,vaccines,vaccines2medication:"
msgid "Next"
msgstr "Siguiente"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_patient.wizard,start,create_patient:"
msgid "Create"
msgstr "Crear"

msgctxt ""
"wizard_button:gnuhealth.trisomy21.create_patient.wizard,start,create_return:"
msgid "Create and return"
msgstr "Crear y volver al asistente"

msgctxt "wizard_button:gnuhealth.trisomy21.create_patient.wizard,start,end:"
msgid "Cancel"
msgstr "Cancelar"
