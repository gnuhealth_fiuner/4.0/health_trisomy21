from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.modules.health.core import (get_institution, compute_age_from_dates,
                   get_health_professional)
from trytond.modules.health_crypto.health_crypto import HealthCrypto
from trytond.pool import Pool
from trytond.pyson import Eval, Equal, Or, Not, Bool
from trytond.rpc import RPC

from datetime import datetime
from uuid import uuid4
import hashlib
import json


class GrowthLine(ModelView):
    'Growth Line'
    __name__ = 'gnuhealth.trisomy21.create_lejeune_attention.growth_lines'

    month = fields.Integer('Month')
    p3_value = fields.Float('P3')
    p10_value = fields.Float('P10')
    p25_value = fields.Float('P25')
    p50_value = fields.Float('P50')
    p75_value = fields.Float('P75')
    p90_value = fields.Float('P90')
    p97_value = fields.Float('P97')
    patient_value = fields.Float('Patient')


class Evaluation():
    STATES = {'readonly': Eval('state') == 'signed'}

    code = fields.Char('Code', help="Unique code that \
        identifies the evaluation", states=STATES)
    patient = fields.Many2One('gnuhealth.patient', 'Patient', states=STATES)
    computed_age = fields.Function(
        fields.Float('Patient age at evaluation'),
        'on_change_with_computed_age')
    gender = fields.Function(fields.Selection([
        (None, ''),
        ('m', 'Male'),
        ('f', 'Female'),
        #('f-m', 'Female -> Male'),
        #('m-f', 'Male -> Female'),
        ], 'Gender'), 'on_change_with_gender', searcher='search_patient_gender')
    appointment = fields.Many2One('gnuhealth.appointment', 'Appointment',
        domain=[('patient', '=', Eval('patient'))], depends=['patient'],
        help='Enter or select the date / ID of the appointment related to'
        ' this evaluation', states=STATES)
    healthprof = fields.Many2One(
        'gnuhealth.healthprofessional', 'Health Prof',
        help="Health professional that initiates the evaluation."
        "This health professional might or might not be the same that"
        " signs and finishes the evaluation."
        "The evaluation remains in progress state until it is signed"
        ", when it becomes read-only", readonly=True)
    specialty = fields.Many2One('gnuhealth.specialty', 'Specialty',
            states=STATES)
    signed_by = fields.Many2One(
        'gnuhealth.healthprofessional', 'Signed by - Health Prof',
        readonly=True,
        states={'invisible': Equal(Eval('state'), 'in_progress')},
        help="Health Professional that finished the patient evaluation")
    information_source = fields.Char(
        'Source', help="Source of"
        "Information, eg : Self, relative, friend ...",
        states=STATES)
    evaluation_start = fields.DateTime('Start', required=True, states=STATES)
    evaluation_endtime = fields.DateTime('End', states=STATES)
    evaluation_length = fields.Function(
        fields.TimeDelta(
            'Evaluation length',
            help="Duration of the evaluation"),
        'on_change_with_evaluation_length')

    wait_time = fields.Function(
        fields.TimeDelta('Patient wait time',
                         help="How long the patient waited"),
        'on_change_with_wait_time')
    score = fields.Function(
        fields.Integer('Score'),
        'on_change_with_score')
    state = fields.Selection([
        (None, ''),
        ('in_progress', 'In progress'),
        ('done', 'Done'),
        ('signed', 'Signed'),
        ], 'State', readonly=True, sort=False)
    serializer = fields.Text('Doc String', readonly=True)
    document_digest = fields.Char('Digest', readonly=True,
                                help="Original Document Digest")
    digest_status = fields.Function(
        fields.Boolean('Altered',
            states={
                'invisible': Not(Equal(Eval('state'), 'signed')),
                },
            help="This field will be set whenever parts of"
                " the main original document has been changed."
                " Please note that the verification is done"
                " only on selected fields."),
        'check_digest')
    serializer_current = fields.Function(
        fields.Char('Current Hash',
            states={
                'invisible': Not(Bool(Eval('digest_status'))),
                    }),
        'check_digest')
    digest_current = fields.Function(
        fields.Char('Current Hash',
            states={
                'invisible': Not(Bool(Eval('digest_status'))),
                }),
        'check_digest')

    digital_signature = fields.Text('Digital Signature', readonly=True)


    @fields.depends('patient', 'evaluation_start')
    def on_change_with_computed_age(self, name=None):
        if self.patient and self.evaluation_start and self.patient.dob:
            age = compute_age_from_dates(self.patient.dob, None, None, None,
                        'raw_age', self.evaluation_start.date())
            if (age[0] + age[1] + age[2]) > 0:
                return round(age[0] + age[1]/12 + age[2]/365, 2)
        return None

    @fields.depends('patient')
    def on_change_with_gender(self, name=None):
        if self.patient:
            gender = self.patient.name.gender
            sex = self.patient.biological_sex
            if sex:
                if (gender != sex):
                    res = sex + '-' + gender
                else:
                    res = gender
            else:
                res = gender
            return res
        return None

    @fields.depends('evaluation_endtime', 'evaluation_start')
    def on_change_with_evaluation_length(self, name=None):
        if (self.evaluation_endtime and self.evaluation_start):
            return self.evaluation_endtime - self.evaluation_start

    @fields.depends('appointment', 'evaluation_start')
    def on_change_with_wait_time(self, name=None):
        # Compute wait time between checked-in and start of evaluation
        if self.appointment:
            if self.appointment.checked_in_date:
                if self.appointment.checked_in_date < self.evaluation_start:
                    return self.evaluation_start - \
                        self.appointment.checked_in_date

    def on_change_with_score(self, name=None):
        res = 0
        return res

    @classmethod
    def search_patient_gender(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('patient.name.gender', clause[1], value))
        return res

    @staticmethod
    def default_evaluation_start():
        return datetime.today()

    @staticmethod
    def default_healthprof():
        return get_health_professional()

    @staticmethod
    def default_score():
        return 0

    # End the evaluation and discharge the patient
    @classmethod
    @ModelView.button
    def end_evaluation(cls, evaluations):
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')

        evaluation_id = evaluations[0]

        patient_app = []

        # Change the state of the evaluation to "Done"
        signing_hp = get_health_professional()

        cls.write(evaluations, {
            'state': 'done',
            'signed_by': signing_hp,
            'evaluation_endtime': datetime.now()
            })

        # If there is an appointment associated to this evaluation
        # set it to state "Done"
        if evaluations[0].appointment:
            patient_app.append(evaluations[0].appointment)
            Appointment.write(patient_app, {
                'state': 'done',
                })

        # Create an entry in the page of life
        # It will create the entry at the moment of
        # discharging the patient
        # The patient needs to have a federation account
        if (evaluation_id.patient.name.federation_account):
            cls.create_evaluation_pol(evaluation_id)

    @classmethod
    @ModelView.button
    def sign_evaluation(cls, evaluations):
        evaluation = evaluations[0]

        # Change the state of the evaluation to "Signed"

        serial_doc = cls.get_serial(evaluation)

        cls.write(evaluations, {
            'serializer': serial_doc,
            'document_digest': HealthCrypto().gen_hash(serial_doc),
            'state': 'signed', })

    @classmethod
    def get_serial(cls, evaluation):
        pass

    @classmethod
    def set_signature(cls, data, signature):
        """
        Set the clearsigned signature
        """
        doc_id = data['id']

        cls.write([cls(doc_id)], {
            'digital_signature': signature,
            })

    def check_digest(self, name):
        result = ''
        serial_doc = str(self.get_serial(self))
        if (name == 'digest_status' and self.document_digest):
            if (HealthCrypto().gen_hash(serial_doc) == self.document_digest):
                result = False
            else:
                ''' Return true if the document has been altered'''
                result = True
        if (name == 'digest_current'):
            result = HealthCrypto().gen_hash(serial_doc)

        if (name == 'serializer_current'):
            result = serial_doc

        return result

    @classmethod
    def create_evaluation_pol(cls, evaluation):
        """ Adds an entry in the person Page of Life
            related to this medical evaluation.
        """
        Pol = Pool().get('gnuhealth.pol')
        pol = []

        # Create a dictionary with vital signs and other measurements
        measurements = {}
        bp = {}
        #if evaluation.systolic:
            #bp['systolic'] = evaluation.systolic
        #if evaluation.diastolic:
            #bp['diastolic'] = evaluation.diastolic

        #if bp:
            #measurements['bp'] = bp
        #if evaluation.temperature:
            #measurements['t'] = evaluation.temperature
        #if evaluation.bpm:
            #measurements['hr'] = evaluation.bpm
        #if evaluation.respiratory_rate:
            #measurements['rr'] = evaluation.respiratory_rate
        #if evaluation.osat:
            #measurements['osat'] = evaluation.osat
        #if evaluation.glycemia:
            #measurements['bg'] = evaluation.glycemia
        #if evaluation.weight:
            #measurements['wt'] = evaluation.weight
        #if evaluation.height:
            #measurements['ht'] = evaluation.height
        #if evaluation.bmi:
            #measurements['bmi'] = evaluation.bmi
        #if evaluation.head_circumference:
            #measurements['hc'] = evaluation.head_circumference

        #assessment = (evaluation.diagnosis and
                      #evaluation.diagnosis.rec_name) or ''

        measures = str(measurements)
        # Summarize the encounter note taking as SOAP
        #soap = \
            #f"S: {evaluation.chief_complaint}\n--\n"
        #f"{evaluation.present_illness}\n"
        #f"O: {evaluation.evaluation_summary}\n"
        #f"        {measures}\n"
        #f"A: {assessment}\n"
        #f"P: {evaluation.directions}"

        vals = {
            'page': str(uuid4()),
            'person': evaluation.patient.name.id,
            'page_date': evaluation.evaluation_start,
            'age': evaluation.computed_age,
            'federation_account': evaluation.patient.name.federation_account,
            'page_type': 'medical',
            'medical_context': 'encounter',
            'relevance': 'important',
            #'summary': evaluation.chief_complaint,
            #'info': soap,
            #'measurements': measures,
            'author': evaluation.healthprof.name.rec_name,
            'author_acct': evaluation.healthprof.name.federation_account,
            #'node': evaluation.institution.name.name,
            }
        #if (evaluation.diagnosis):
            #vals['health_condition_text'] = evaluation.diagnosis.rec_name
            #vals['health_condition_code'] = evaluation.diagnosis.code

        pol.append(vals)
        Pol.create(pol)


class BarthelEvaluation(ModelSQL, ModelView, Evaluation):
    'Barthel Evaluation'
    __name__ = 'gnuhealth.barthel_evaluation'

    STATES = {'readonly': Eval('state') == 'signed'}

    eating = fields.Selection([
        (None, ''),
        ('independent', '1. Totally independent'),
        ('needs_help', '2. Needs help to cut meat, bread, spread butter etc.'),
        ('dependent', '3. Dependent')
        ], 'Eating', sort=False, required=True, states=STATES)
    eating_string = eating.translated('eating')
    bathe = fields.Selection([
        (None, ''),
        ('independent', '1. Independent'),
        ('dependent', '2. Dependent')
        ], 'Bathe', sort=False, required=True, states=STATES)
    bathe_string = bathe.translated('bathe')
    dressing = fields.Selection([
        (None, ''),
        ('independent', '1. Independent: able to put on and take off clothes\n,'
                    'button up, tie shoes.'),
        ('needs_help', '2. Need help (can do half)'),
        ('dependent', '3. Dependent')
        ], 'Dressing', sort=False, required=True, states=STATES)
    dressing_string = dressing.translated('dressing')
    hygiene = fields.Selection([
        (None, ''),
        ('independent', '1. Independently to wash face, hands, comb, shave,\n'
                    'put on makeup, etc.'),
        ('dependent', '2. Dependent')
        ], 'Hygiene', sort=False, required=True, states=STATES)
    hygiene_string = hygiene.translated('hygiene')
    depositions = fields.Selection([
        (None, ''),
        ('normal', '1. Normal continence'),
        ('occasionally', '2. Occasionally an episode of incontinence, or\n'
                    'need help to administer suppositories or enemas'),
        ('incontinence', '3. Incontenence')
        ], 'Depositions', sort=False, required=True, states=STATES)
    depositions_string = depositions.translated('depositions')
    urination = fields.Selection([
        (None, ''),
        ('normal', '1. Normal continence, or is able to take care of the\n'
                    'tube if it has one in place'),
        ('occasionally', '2. A maximum of one episode of incontinence per\n'
                    'day, or needs help to care for the catheter'),
        ('incontenence', '3. Incontinence or unable to change catheter')
        ], 'Urination', sort=False, required=True, states=STATES)
    urination_string = urination.translated('urination')
    toilet_use = fields.Selection([
        (None, ''),
        ('independent', '1. Independent to go to the bathroom, take off\n'
                    'and put on clothes'),
        ('needs_help', '2. Needs help going to the toilet, but cleans himself'),
        ('dependent', '3. Dependent'),
        ], 'Toilet use', sort=False, required=True, states=STATES)
    toilet_use_string = toilet_use.translated('toilet_use')
    moving = fields.Selection([
        (None, ''),
        ('independent', '1. Independent to go from chair to bed'),
        ('needs_some_help', '2. Minimal physical help/supervision to do so'),
        ('needs_lot_help', '3. Needs a lot of help (1-2 people), but\n'
                        'is able to sit alone'),
        ('dependent', '4. Dependent, does not remain seated'),
        ], 'Moving', sort=False, required=True, states=STATES)
    moving_string = moving.translated('moving')
    wander = fields.Selection([
        (None, ''),
        ('independent', '1. Independent, walks alone or with any type of\n'
                        'crutch (not a walker) 50 meters'),
        ('dependent', '2. Needs physical help or supervision to walk 50 metres'),
        ('independent_wc', '3. Independent in a wheelchair without assistance'),
        ('dependent_wc', '4. Dependent'),
        ], 'Wander', sort=False, required=True, states=STATES)
    wander_string = wander.translated('wander')
    climb_stairs = fields.Selection([
        (None, ''),
        ('independent', '1. Independent to go up and down stairs'),
        ('needs_help', '2. Needs physical help or supervision to do it'),
        ('dependent', '3. Dependent'),
        ], 'Climb Stairs', sort=False, required=True, states=STATES)
    climb_stairs_string = climb_stairs.translated('climb_stairs')

    score_dependency = fields.Function(
        fields.Selection([
            ('0', 'Total dependency'),
            ('1', 'Severe dependency'),
            ('2', 'Moderate dependency'),
            ('3', 'Low dependency'),
            ('4', 'Independency'),
            ],'Score dependency'),
        'on_change_with_score_dependency')
    score_dependency_string = score_dependency.translated('score_dependency')

    @fields.depends('eating', 'bathe', 'dressing', 'hygiene', 'depositions', 'urination',
            'toilet_use', 'moving', 'wander', 'climb_stairs')
    def on_change_with_score(self, name=None):
        res = 0
        res += 10 if self.eating == 'independent' \
            else 5 if self.eating == 'needs_help' \
            else 0
        res += 5 if self.bathe == 'independent' else 0
        res += 10 if self.dressing == 'independent' \
            else 5 if self.dressing == 'needs_help' \
            else 0
        res += 5 if self.hygiene == 'independent' else 0
        res += 10 if self.depositions == 'normal' \
            else 5 if self.depositions == 'occasionally' \
            else 0
        res += 10 if self.urination == 'normal' \
            else 5 if self.urination == 'occasionally' \
            else 0
        res += 10 if self.toilet_use == 'independent' \
            else 5 if self.toilet_use == 'needs_help' \
            else 0
        res += 15 if self.moving == 'independent' \
            else 10 if self.moving == 'needs_some_help' \
            else 5 if self.moving == 'needs_lot_help' \
            else 0
        res += 15 if self.wander == 'independent' \
            else 10 if self.wander == 'dependent' \
            else 5 if self.wander == 'independent_wc' \
            else 0
        res += 10 if self.climb_stairs == 'independent' \
            else 5 if self.climb_stairs == 'needs_help' \
            else 0
        return res

    @fields.depends('score')
    def on_change_with_score_dependency(self, name=None):
        if self.score:
            if self.score < 21:
                return '0'
            elif 20 < self.score < 61:
                return '1'
            elif 60 < self.score < 91:
                return '2'
            elif 90 < self.score < 100:
                return '3'
            else:
                return '4'
        return '0'

    @classmethod
    def generate_code(cls, **pattern):
        Config = Pool().get('gnuhealth.sequences')
        config = Config(1)
        sequence = config.get_multivalue(
            'barthel_evaluation_sequence', **pattern)
        if sequence:
            return sequence.get()

    @classmethod
    def create(cls, vlist):
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('code'):
                values['code'] = cls.generate_code()
        return super().create(vlist)

    @classmethod
    @ModelView.button
    def end_evaluation(cls, evaluations):
        super().end_evaluation(evaluations)

    @classmethod
    @ModelView.button
    def sign_evaluation(cls, evaluations):
        super().sign_evaluation(evaluations)

    @classmethod
    def get_serial(cls, evaluation):
        data_to_serialize = {
            'Patient': str(evaluation.patient.rec_name) or '',
            'Start': str(evaluation.evaluation_start) or '',
            'End': str(evaluation.evaluation_endtime) or '',
            'Initiated_by': str(evaluation.healthprof.rec_name),
            'Signed_by': evaluation.signed_by and
            str(evaluation.signed_by.rec_name) or '',
            'Specialty': evaluation.specialty and
            str(evaluation.specialty.rec_name) or '',
            'Eating': evaluation.eating,
            'bathe': evaluation.bathe,
            'dressing': evaluation.dressing,
            'hygiene': evaluation.hygiene,
            'depositions': evaluation.depositions,
            'urination': evaluation.urination,
            'toilet_use': evaluation.toilet_use,
            'moving': evaluation.moving,
            'wander': evaluation.wander,
            'climb_stairs': evaluation.climb_stairs
             }

        serialized_doc = str(HealthCrypto().serialize(data_to_serialize))

        return serialized_doc

    @classmethod
    def __setup__(cls):
        super().__setup__()

        t = cls.__table__()
        cls._sql_constraints = [
            ('code_unique', Unique(t, t.code),
                'The evaluation code must be unique !'),
            ]

        cls._order.insert(0, ('evaluation_start', 'DESC'))
        cls._buttons.update({
            'end_evaluation': {'invisible': Or(Equal(Eval('state'), 'signed'),
                                               Equal(Eval('state'), 'done'))
                            },
            'sign_evaluation': {
                'invisible': Not(Equal(Eval('state'), 'done'))
                            },
            })
        cls.__rpc__.update({
                'set_signature': RPC(readonly=False),
                })

    @classmethod
    def view_attributes(cls):
        return [('//group[@id="group_digital_signature"]', 'states', {
                'invisible': ~Eval('digital_signature')}),
                ('//group[@id="group_current_string"]', 'states', {
                 'invisible': ~Eval('digest_status'),
                 })]


class LawtonBrodyEvaluation(ModelSQL, ModelView, Evaluation):
    'Lawton and Brody Evaluation'
    __name__ = 'gnuhealth.lawton_brody_evaluation'

    STATES = {'readonly': Eval('state') == 'signed'}

    # Start scoring
    phone_use = fields.Selection([
        (None, ''),
        ('1', '1. Use the telephone on its own initiative, '
            'search and dial numbers, etc.'),
        ('2', '2. Dial a few well-known numbers'),
        ('3', '3. Answer the phone but don\'t dial'),
        ('4', '4. Does not use the phone'),
        ], 'Phone use', sort=False, required=True, states=STATES)
    phone_use_string = phone_use.translated('phone_use')
    shopping = fields.Selection([
        (None, ''),
        ('1', '1. Make all necessary purchases independently'),
        ('2', '2. Buy small things independently'),
        ('3', '3. You need company to make any purchase'),
        ('4', '4. Completely unable to shop'),
        ], 'Shopping', sort=False, required=True, states=STATES)
    shopping_string = shopping.translated('shopping')
    cooking = fields.Selection([
        (None, ''),
        ('1', '1. Plans, prepares, and serves appropriate meals '
            'independently'),
        ('2', '2. Prepares meals if given ingredients'),
        ('3', '3. Heats and serves meals but does not maintain an '
            'adequate diet'),
        ('4', '4. Needs food to be prepared and served'),
        ], 'Cooking', sort=False, required=True, states=STATES)
    cooking_string = cooking.translated('cooking')
    home_care = fields.Selection([
        (None, ''),
        ('1', '1. Takes care of the house alone or with occasional help '
            '(eg Heavy work)'),
        ('2', '2. Does light household chores like washing dishes '
            'or making bed'),
        ('3', '3. Does light housework but can\'t keep up '
             'an acceptable level of cleanliness'),
        ('4', '4. Need help with all the housework'),
        ('5', '5. Does not participate in any household chores'),
        ], 'Home care', sort=False, required=True, states=STATES)
    home_care_string = home_care.translated('home_care')
    laundry = fields.Selection([
        (None, ''),
        ('1', '1. Completely carry out personal laundry'),
        ('2', '2. Wash small items'),
        ('3', '3. Need someone else to do the washing'),
        ], 'Laundry', sort=False, required=True, states=STATES)
    laundry_string = laundry.translated('laundry')
    use_of_transportation = fields.Selection([
        (None, ''),
        ('1', '1. Travel independently on public transport or '
            'drive your car'),
        ('2', '2. Able to arrange own taxi transportation, but '
            'does not use other means of transport'),
        ('3', '3. Take public transport if accompanied by another person'),
        ('4', '4. Only travel by taxi or car with the help of others'),
        ('5', '5. Does not travel'),
        ], 'Use of transportation', sort=False, required=True, states=STATES)
    use_of_transportation_string = \
        use_of_transportation.translated('use_of_transportation')
    liability_for_medication = fields.Selection([
        (None,''),
        ('1', '1. It is responsible for the use of medication, dose '
            'and hours correct'),
        ('2', '2. Take the medication responsibly if it is prepared '
            'in advance in doses ready'),
        ('3', '3. He is not able to take responsibility for his '
            'own medication'),
        ], 'Liability for medication', sort=False, required=True, states=STATES)
    liability_for_medication_string = \
        liability_for_medication.translated('liability_for_medication')
    money_handling = fields.Selection([
        (None, ''),
        ('1', '1. Manages financial affairs independently, collects '
            'and know its income'),
        ('2', '2. Manages daily expenses but needs help going to the '
            'bank, large expenses'),
        ('3', '3. Unable to handle money'),
        ], 'Money handling', sort=False, required=True, states=STATES)
    money_handling_string = \
        money_handling.translated('money_handling')
    score_dependency = fields.Function(
        fields.Selection([
            (None, ''),
            ('total', 'Total'),
            ('severe', 'Severe'),
            ('moderate', 'Moderate'),
            ('low', 'Low'),
            ('autonomous', 'Autonomous')
            ], 'Score dependency'),
        'on_change_with_score_dependency')
    score_dependency_string = score_dependency.translated('score_dependency')

    @fields.depends('phone_use', 'shopping', 'cooking', 'home_care',
        'laundry', 'use_of_transportation', 'liability_for_medication',
        'money_handling')
    def on_change_with_score(self, name=None):
        res = 0
        res += 1 if self.phone_use != '4' else 0
        res += 1 if self.shopping == '1' else 0
        res += 1 if self.cooking == '1' else 0
        res += 1 if self.home_care in ['1', '2'] else 0
        res += 1 if self.laundry != '3' else 0
        res += 1 if self.use_of_transportation in ['1', '2', '3'] \
                else 0
        res += 1 if self.liability_for_medication == '1' else 0
        res += 1 if self.money_handling != '3' else 0
        return res

    @fields.depends('score')
    def on_change_with_score_dependency(self, name=None):
        if self.score:
            if self.score < 2:
                return 'total'
            elif 1 < self.score < 4:
                return 'severe'
            elif 3 < self.score < 6:
                return 'moderate'
            elif 5 < self.score < 8:
                return 'low'
            else:
                return 'autonomous'
        return ''

    @classmethod
    def generate_code(cls, **pattern):
        Config = Pool().get('gnuhealth.sequences')
        config = Config(1)
        sequence = config.get_multivalue(
            'lawton_brody_evaluation_sequence', **pattern)
        if sequence:
            return sequence.get()

    @classmethod
    def create(cls, vlist):
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('code'):
                values['code'] = cls.generate_code()
        return super().create(vlist)

    # End the evaluation and discharge the patient
    @classmethod
    @ModelView.button
    def end_evaluation(cls, evaluations):
        super().end_evaluation(evaluations)

    @classmethod
    @ModelView.button
    def sign_evaluation(cls, evaluations):
        super().sign_evaluation(evaluations)

    @classmethod
    def get_serial(cls, evaluation):
        data_to_serialize = {
            'Patient': str(evaluation.patient.rec_name) or '',
            'Start': str(evaluation.evaluation_start) or '',
            'End': str(evaluation.evaluation_endtime) or '',
            'Initiated_by': str(evaluation.healthprof.rec_name),
            'Signed_by': evaluation.signed_by and
            str(evaluation.signed_by.rec_name) or '',
            'Specialty': evaluation.specialty and
            str(evaluation.specialty.rec_name) or '',
            'phone_use': evaluation.phone_use,
            'shopping': evaluation.shopping,
            'cooking': evaluation.cooking,
            'home_care': evaluation.home_care,
            'laundry': evaluation.laundry,
            'use_of_transportation': evaluation.use_of_transportation,
            'liability_for_medication': evaluation.liability_for_medication,
            'money_handling': evaluation.money_handling,
             }

        serialized_doc = str(HealthCrypto().serialize(data_to_serialize))

        return serialized_doc

    @classmethod
    def __setup__(cls):
        super().__setup__()

        t = cls.__table__()
        cls._sql_constraints = [
            ('code_unique', Unique(t, t.code),
                'The evaluation code must be unique !'),
            ]

        cls._order.insert(0, ('evaluation_start', 'DESC'))
        cls._buttons.update({
            'end_evaluation': {'invisible': Or(Equal(Eval('state'), 'signed'),
                                               Equal(Eval('state'), 'done'))
                            },
            'sign_evaluation': {
                'invisible': Not(Equal(Eval('state'), 'done'))
                            },
            })
        cls.__rpc__.update({
                'set_signature': RPC(readonly=False),
                })

    @classmethod
    def view_attributes(cls):
        return [('//group[@id="group_digital_signature"]', 'states', {
                'invisible': ~Eval('digital_signature')}),
                ('//group[@id="group_current_string"]', 'states', {
                 'invisible': ~Eval('digest_status'),
                 })]


class LejeuneAttention(ModelView, ModelSQL, Evaluation):
    'Lejeune Attention'
    __name__ = 'gnuhealth.trisomy21.lejeune_attention'

    STATES = {'readonly': Eval('state') == 'signed'}

    name = fields.Char('Attention ID', states=STATES)

    '''Systemic Anamnesis '''
    # cardiovascular
    cardiov_healthprof = fields.Many2One('gnuhealth.healthprofessional',
        'Health professional', states=STATES)
    cardiov_health_inst = fields.Many2One('gnuhealth.institution',
        'Health institution', states=STATES)
    # neurological
    neuropsyc_assessment = fields.Boolean('Neuropsychological assessment', states=STATES)
    dementia_assessment = fields.Boolean('Dementia assessment', states=STATES)
    cephalic_support = fields.Boolean('Cephalic support', states=STATES)
    sphinters_control = fields.Boolean('Sphinters control', states=STATES)
    neuro_observations = fields.Text('Observations', states=STATES)
    neuro_healthprof = fields.Many2One('gnuhealth.healthprofessional',
        'Health professional', states=STATES)
    neuro_health_inst = fields.Many2One('gnuhealth.institution',
        'Health institution', states=STATES)
    # psychiatric
    behaviour_disorder = fields.Boolean('Behaviour disorder', states=STATES)
    self_inflicted_damage = fields.Boolean('Self inflicted damage', states=STATES)
    tea = fields.Boolean('TEA', states=STATES)
    tdha = fields.Boolean('TDHA', states=STATES)
    psych_observations = fields.Text('Observations', states=STATES)
    psych_health_prof = fields.Many2One('gnuhealth.healthprofessional',\
        'Health Professional', states=STATES)
    psych_health_inst = fields.Many2One('gnuhealth.institution', \
        'Health Institution', states=STATES)
    # gastrointestinal
    feeding = fields.Text('Feeding', states=STATES)
    feeding_intolerance = fields.Boolean('Feeding intolerance', states=STATES)
    feeding_intolerance_description = fields.Text('Feeding intolerance',
                states={'readonly': Or(~Eval('feeding_intolerance'), Eval('state')=='signed')})
    bowel_habit = fields.Text('Bowel habit', states=STATES)
    gerd_symptoms = fields.Boolean('GERD symptoms', states=STATES)
    fecal_occult_blood = fields.Text('Fecal occult blood', states=STATES)
    videocolonoscopy = fields.Text('Videocolonoscopy', states=STATES)
    celiac_screening = fields.Selection([
        (None, ''),
        ('positive', '+'),
        ('negative', '-')
        ], 'Celiac screening', sort=False, states=STATES)
    celiac_screening_string = celiac_screening.translated('celiac_screening')
    hla_dq2_dq8 = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'HLA-DQ2-DQ8', sort=False, states=STATES)
    hla_dq2_dq8_string = hla_dq2_dq8.translated('hla_dq2_dq8')
    gastroint_observations = fields.Text('Observations', states=STATES)
    gastroint_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                'Health Professional', states=STATES)
    gastroint_health_inst = fields.Many2One('gnuhealth.institution',
                'Health Institution', states=STATES)
    # neumonology
    recurrent_infections = fields.Boolean('Recurrent infections', states=STATES)
    recurrent_infections_description = fields.Text('Recurrent infections',
                states={'readonly': Or(~Eval('recurrent_infections'),Eval('state')=='signed')})
    hospitalizations = fields.Boolean('Hospitalizations', states=STATES)
    hospitalizations_description = fields.Text('Hospitalizations',
                states={'readonly': Or(~Eval('hospitalizations'),Eval('state')=='signed')})
    neumo_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                'Health Professional', states=STATES)
    neumo_health_inst = fields.Many2One('gnuhealth.institution',
                'Health institution', states=STATES)
    # orl
    last_audiometry = fields.Date('Last audiometry', states=STATES)
    sleep_wake_cycle = fields.Text('Sleep/wake cycle',
                help='Description of sleep/wake cycle', states=STATES)
    apnea = fields.Boolean('Apnea', states=STATES)
    snoring = fields.Boolean('Snoring', states=STATES)
    polysomnography = fields.Boolean('Polysomnography', states=STATES)
    polysomnography_date = fields.Date('Last date of polysomnography',
                states=STATES)
    oea = fields.Selection([
        (None, ''),
        ('pass_test', 'Pass test'),
        ('not_pass_test', 'Do not pass test')
        ], 'OEA', sort=False, states=STATES)
    orl_observations = fields.Text('Observations', states=STATES)
    orl_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                'Health professional', states=STATES)
    orl_health_inst = fields.Many2One('gnuhealth.institution',
                'Heath institution', states=STATES)
    # odontostomatology
    agenesia = fields.Boolean('Agenesia', states=STATES)
    late_dentition = fields.Boolean('Late dentition', states=STATES)
    last_dental_control = fields.Date('Last dental control', states=STATES)
    dental_orthosis = fields.Boolean('Dental Orthesis', states=STATES)
    dental_orthosis_description = fields.Text('Dental orthosis description',
                    states={'readonly': Or(~Eval('dental_orthosis'), Eval('state')=='signed')})
    periodontal_disease_counseling = \
        fields.Text('Periodontal disease counseling', states=STATES)
    dental_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                        'Health professional', states=STATES)
    dental_health_inst = fields.Many2One('gnuhealth.institution',
                        'Health institution', states=STATES)
    # ophthalmology
    last_biannual_ophthalm_assessment = \
        fields.Date('Last biannual ophtalmologic assessment', states=STATES)
    use_glasses = fields.Binary('Use of glasses', states=STATES)
    glasses = fields.Boolean('Glasses', states=STATES)
    ophthalm_healthprof = fields.Many2One('gnuhealth.healthprofessional',
                    'Health professional', states=STATES)
    ophthalm_health_inst = fields.Many2One('gnuhealth.institution',
                    'Health institution', states=STATES)
    # endocrinology
    thyroid_ultrasound = fields.Boolean('Thyroid ultrasound', states=STATES)
    thyroid_ultrasound_observations = fields.Text('Thyroid ultrasound - observations',
                states=STATES)
    annual_tsh = fields.Integer('Annual tsh',
                help="Request Ac if the value is between 5 and 10\n"
                    "(>10 refer to endocrinology with thyroid ultrasound, states=STATES)", states=STATES)
    glycemia = fields.Integer('Glycemia', help="Every 6 months if has FR", states=STATES)
    cholesterol = fields.Integer('Cholesterol', help="Biennial if BMI>25", states=STATES)
    bone_densitometry = fields.Char('Bone densitometry',
                    help="From the age of 40", states=STATES)
    metabolic_anormal = fields.Boolean('Metabolic anormalities', states=STATES)
    metabolic_anormal_description = \
        fields.Text('Metabolic anormalities description',
                    states={'readonly': Or(~Eval('metabolic_anormal'), Eval('state')=='signed')})
    t4 = fields.Text('T4', states=STATES)
    endoc_health_prof = \
        fields.Many2One('gnuhealth.healthprofessional',
                    'Health professional', states=STATES)
    endoc_health_inst = fields.Many2One('gnuhealth.institution',
                                'Institution', states=STATES)
    # gynecology
    menarche = fields.Integer('Menarche', help="Age in years", states=STATES)
    menopause = fields.Integer('Menopause', help="Age in years", states=STATES)
    ### early_menopause
    early_mp_suffocation = fields.Boolean('Suffocation',
                        help="As an early menopause symptom", states=STATES)
    early_mp_night_sweats = fields.Boolean('Night sweats',
                        help="As an early menopause symptom", states=STATES)
    early_mp_headache = fields.Boolean('Headaches',
                        help="As an early menopause symptom", states=STATES)
    early_mp_dec_libido = fields.Boolean('Decreased libido',
                        help="As an early menopause symptom", states=STATES)
    early_mp_menstrual_change = \
            fields.Boolean('Changes in the menstrual cycle',
                       help="As an early menopause symptom", states=STATES)
    early_mp_irritability = \
            fields.Boolean('Irritability and depresion periods',
                        help="As an early menopause symptom", states=STATES)
    early_mp_mood_changes = fields.Boolean('Mood changes',
                        help="As an early menopause symptom", states=STATES)
    gyneco_health_prof= fields.Many2One('gnuhealth.healthprofessional',
                        'Health professional', states=STATES)
    gyneco_health_inst= fields.Many2One('gnuhealth.institution',
                        'Health Institution', states=STATES)
    # fertility
    sex_education = fields.Boolean('Sex education and anticonceptive', states=STATES)
    sexual_relations = fields.Boolean('Sexual relations', states=STATES)
    anticonceptive = fields.Selection([
        (None, ''),
        ('0', 'None'),
        ('1', 'Pill / Minipill'),
        ('2', 'Male condom'),
        ('3', 'Vasectomy'),
        ('4', 'Female sterilisation'),
        ('5', 'Intra-uterine device'),
        ('6', 'Withdrawal method'),
        ('7', 'Fertility cycle awareness'),
        ('8', 'Contraceptive injection'),
        ('9', 'Skin Patch'),
        ('10', 'Female condom'),
        ], 'Contraceptive Method', sort=False, states=STATES)
    fert_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                                    'Health professional', states=STATES)
    fert_health_inst = fields.Many2One('gnuhealth.institution',
                                    'Institution', states=STATES)
    # andrology
    andro_mood_changes = fields.Boolean('Mood changes',
                    help="Andropause signs", states=STATES)
    andro_fatigue = fields.Boolean('Fatigue',
                    help="Andropause signs", states=STATES)
    andro_weight_inc = fields.Boolean('Weight increment',
                    help="Andropause signs", states=STATES)
    andro_irritability = fields.Boolean('Irritability',
                    help="Andropause signs", states=STATES)
    andro_depression = fields.Boolean('Depression',
                    help="Andropause signs", states=STATES)
    andro_sweat = fields.Boolean('Sweat', help="Andropause signs", states=STATES)
    andro_headache = fields.Boolean('Headache', help="Andropause signs", states=STATES)
    annual_testicular_control = fields.Boolean('Annual testicular control',
                                help="Up to 45 years", states=STATES)
    annual_testicular_us = fields.Boolean('Annual testicular ultrasound', states=STATES)
    andro_health_prof = \
        fields.Many2One('gnuhealth.healthprofessional',
                'Health professional', states=STATES)
    andro_health_inst = fields.Many2One('gnuhealth.institution',
                'Health institution', states=STATES)

    #uronefrology
    uronefro_observations = fields.Text('Uronefrology observations',
                states=STATES)
    uronefro_health_prof = \
        fields.Many2One('gnuhealth.healthprofessional',
                'Health professional', states=STATES)
    uronefro_health_inst = fields.Many2One('gnuhealth.institution',
                'Health institution', states=STATES)
    # orthopedics and traumatology
    scoliosis = fields.Boolean('Scoliosis', states=STATES)
    scoliosis_description = fields.Text('Scoliosis description',
                    states={'readonly': Or(~Eval('scoliosis'), Eval('state')=='signed')})
    patella_inestability = \
        fields.Boolean('Risk factors or inestability on the patella',
                help="overweight, flatfoot", states=STATES)
    patella_inestability_description = \
        fields.Text('Risk factors or inestability on the patella description',
                states={'readonly': Or(~Eval('patella_inestability'), Eval('state')=='signed')})
    carpal_tunnel_synd = fields.Boolean('Carpal tunnel syndrome', states=STATES)
    chronic_cervical_myel = fields.Boolean('Chronic cervical myelopathy', states=STATES)
    flatfoot = fields.Boolean('Flatfoot', states=STATES)
    orthesis_prosthesis = fields.Boolean('Orthesis/prosthesis', states=STATES)
    xray_cervical = fields.Boolean('X-ray cervical F and P with Torg index', states=STATES)
    xray_pavlov = fields.Boolean('X-ray Pavlov', states=STATES)
    xray_hip_panoramic = fields.Boolean('X-ray panoramic hip', states=STATES)
    xray_von_rosen = fields.Boolean('X-ray Von Rosen', states=STATES)
    reumatology = fields.Text('Reumatology', states=STATES)
    orth_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                    'Health professional', states=STATES)
    orth_health_inst = fields.Many2One('gnuhealth.institution',
                    'Institution', states=STATES)
    # disabilities
    disab_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                    'Health Professional', states=STATES)
    disab_health_inst = fields.Many2One('gnuhealth.institution',
                    'Institution', states=STATES)
    # dermatology
    periodic_phys_examination = \
        fields.Boolean('Periodic physical examination',
                       help="Skin, nail, hair", states=STATES)
    autoimmune_disease = fields.Boolean('Autoimmune disease',
                        help="Alopecia areata, vitiligo", states=STATES)
    derma_observations = fields.Text('Observations', states=STATES)
    derma_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                    'Health professional', states=STATES)
    derma_health_inst = fields.Many2One('gnuhealth.institution',
                    'Institution', states=STATES)
    # oncohematology
    onco_observations = fields.Text('Observations', states=STATES)
    onco_health_prof = fields.Many2One('gnuhealth.healthprofessional',
                        'Health professional', states=STATES)
    onco_health_inst = fields.Many2One('gnuhealth.institution',
                        'Institution', states=STATES)

    ''' Vaccines '''
    vaccines_observations = fields.Text('Vaccines observations',
                states=STATES)

    ''' Current therapies '''
    phonoaudiology = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Phonoaudiology', sort=False, states=STATES)
    phonoaudiology_freq= fields.Integer('Phonoaudiology frequency',
            help='Monthly frequency',
            states={'readonly': Or(Eval('phonoaudiology')!='yes', Eval('state')=='signed')})
    occ_therapy = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Occupational therapy', sort=False, states=STATES)
    occ_therapy_freq = fields.Integer('Occupational Therapy Frequency',
            help='Monthly frequency',
            states={'readonly': Or(Eval('Occupational Therapy')!='yes', Eval('state')=='signed')})
    psycologist = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Psycologist', sort=False, states=STATES)
    psycologist_freq = fields.Integer('Psycologist frequency',
            help='Monthly frequency',
            states={'readonly': Or(Eval('psycologist')!='yes', Eval('state')=='signed')})
    psychiatrist = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Psychiatrist', sort=False, states=STATES)
    psychiatrist_freq = fields.Integer('Psychiatrist frequency',
            help='Monthly frequency',
            states={'readonly': Or(Eval('psychiatrist')!='yes', Eval('state')=='signed')})
    kinesiotherapy = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Kinesiotherapy', sort=False, states=STATES)
    kinesiotherapy_freq = fields.Integer('Kinesiotherapy frequency',
            help='Monthly frequency',
            states={'readonly': Or(Eval('kinesiotherapy')!='yes', Eval('state')=='signed')})
    therapeutic_acc = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Therapeutic accompaniment', sort=False, states=STATES)
    therapeutic_acc_freq = fields.Integer(
            'Therapeutic accompaniment frequency',
            help='Monthly frequency',
            states={'readonly': Or(Eval('therapeutic_acc')!='yes', Eval('state')=='signed')})
    nursing_assistance = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Nursing assistance', sort=False, states=STATES)
    nursing_assistance_notes = fields.Text('Nursing assistance',
            help='Monthly frequency',
            states={'readonly': Or(Eval('therapeutic_acc')!='yes', Eval('state')=='signed')})
    current_ther_observations = fields.Text('Observations',
            states={'readonly': Eval('state')=='signed'})

    ''' Physical exam '''
    ## vital signs
    systolic = fields.Integer('Systolic Pressure (mmHg)',
        help='Systolic pressure (mmHg)', states=STATES)
    diastolic = fields.Integer('Diastolic Pressure (mmHg)',
        help='Diastolic pressure (mmHg)', states=STATES)
    weight = fields.Float('Weight (kg)', digits=(3, 2),
        help='Weight in kilos', states=STATES)
    height = fields.Float('Height (cm)', digits=(3, 1),
        help='Height in centimeters', states=STATES)
    cephalic_perimeter = fields.Float('Cephalic perimeter (cm)',
        digits=(3, 1),
        help='Perimeter in centimeters', states=STATES)
    bmi = fields.Float(
        'BMI', digits=(2, 2),
        help='Body mass index', states=STATES)
    skin = fields.Text('Skin and faneras', states=STATES)
    mouth = fields.Text('Mouth', states=STATES)
    ears = fields.Text('Ears', states=STATES)
    ## lymphatic system
    lympha = fields.Text('Lymphatic system', states=STATES)
    ## respiratory system
    respiratory_system = fields.Text('Respiratory System', states=STATES)
    ## cardiovascular system
    cardiovascular_system = fields.Text('Cardiovascular System', states=STATES)
    ## digestive system
    digestive_system = fields.Text('Digestive System', states=STATES)
    ## urologic system
    urologic_system = fields.Text('Urologic System', states=STATES)
    ## oam system
    oam_system = fields.Text('Osteoarthromuscular System', states=STATES)
    oam_system_art_limit = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Arthicular limitation', sort=False, states=STATES)
    oam_system_scoliosis = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Scoliosis', sort=False, states=STATES)
    oam_system_pain = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Arthicular Pain', sort=False, states=STATES)
    oam_system_patella_inestability = fields.Selection([
        (None,''),
        ('yes', 'Yes'),
        ('no', 'No')
        ], 'Rotula inestability', sort=False, states=STATES)
    oam_system_heel_valgus = fields.Selection([
        (None,''),
        ('unilateral', 'Unilateral'),
        ('bilateral', 'Bilateral'),
        ('no', 'No')
        ], 'Heel valgus', sort=False, states=STATES)
    ## neuro system
    neuro_system = fields.Text('Neurologic system', states=STATES)
    neuro_vigil = fields.Text('Vigil', states=STATES)
    neuro_orientation_time = fields.Text('Orientation - Time', states=STATES)
    neuro_orientation_space = fields.Text('Orientation - Space', states=STATES)
    neuro_orientation_people = fields.Text('Orientation - People', states=STATES)
    neuro_language = fields.Text('Language', states=STATES)
    neuro_tone_strength_sensitivity = fields.Text(
        'Tone, strength and sensitivity', states=STATES)
    neuro_dyn_coord_diadochokinesia = fields.Selection([
        (None,''),
        ('succeded', 'Succeded'),
        ('failed', 'Failed'),
        ], 'Diadochokinesia', sort=False, states=STATES)
    neuro_dyn_coord_diadochokinesia_string = \
        neuro_dyn_coord_diadochokinesia.translated('neuro_dyn_coord_diadochokinesia')
    neuro_dyn_coord_index_nose_test = fields.Selection([
        (None,''),
        ('succeded', 'Succeded'),
        ('failed', 'Failed'),
        ], 'Index-Nose Test', sort=False, states=STATES)
    neuro_dyn_coord_index_nose_test_string = \
        neuro_dyn_coord_index_nose_test.translated('neuro_dyn_coord_index_nose_test')
    neuro_dyn_coord_walk_straight = fields.Selection([
        (None,''),
        ('succeded', 'Succeded'),
        ('failed', 'Failed'),
        ], 'Walk straight', sort=False, states=STATES)
    neuro_dyn_coord_walk_straight_string = \
        neuro_dyn_coord_walk_straight.translated('neuro_dyn_coord_walk_straight')
    neuro_dyn_coord_walk_heel = fields.Selection([
        (None,''),
        ('succeded', 'Succeded'),
        ('failed', 'Failed'),
        ], 'Heel walk', sort=False, states=STATES)
    neuro_dyn_coord_walk_heel_string = \
        neuro_dyn_coord_walk_heel.translated('neuro_dyn_coord_walk_heel')
    neuro_dyn_coord_walk_tiptoes = fields.Selection([
        (None,''),
        ('succeded', 'Succeded'),
        ('failed', 'Failed'),
        ], 'Tiptoes walk', sort=False, states=STATES)
    neuro_dyn_coord_walk_tiptoes_string = \
        neuro_dyn_coord_walk_tiptoes.translated('neuro_dyn_coord_walk_tiptoes')
    neuro_static_coord_romberg_simple = fields.Selection([
        (None,''),
        ('positive', 'Positive'),
        ('negative', 'Negative'),
        ], 'Romberg simple', sort=False, states=STATES)
    neuro_static_coord_romberg_simple_string = \
        neuro_static_coord_romberg_simple.translated('neuro_static_coord_romberg_simple')
    neuro_static_coord_romberg_sensitized = fields.Selection([
        (None,''),
        ('positive', 'Positive'),
        ('negative', 'Negative'),
        ], 'Romberg sensitized', sort=False, states=STATES)
    neuro_static_coord_romberg_sensitized_string = \
        neuro_static_coord_romberg_sensitized.translated('neuro_static_coord_romberg_sensitized')
    neuro_reflex_bicipital = fields.Selection([
        (None,''),
        ('absent', 'Absent'),
        ('present', 'Present')
        ], 'Bicipital reflex', states=STATES)
    neuro_reflex_bicipital_string = \
        neuro_reflex_bicipital.translated('neuro_reflex_bicipital')
    neuro_reflex_bicipital_notes = fields.Text('Bicipital reflex notes', states=STATES)
    neuro_reflex_tricipital = fields.Selection([
        (None,''),
        ('absent', 'Absent'),
        ('present', 'Present')
        ], 'Tricipital reflex', states=STATES)
    neuro_reflex_tricipital_string = \
        neuro_reflex_tricipital.translated('neuro_reflex_tricipital')
    neuro_reflex_tricipital_notes = fields.Text('Tricipital reflex notes', states=STATES)
    neuro_reflex_patella = fields.Selection([
        (None,''),
        ('absent', 'Absent'),
        ('present', 'Present')
        ], 'Patella reflex', states=STATES)
    neuro_reflex_patella_string = \
        neuro_reflex_patella.translated('neuro_reflex_patella')
    neuro_reflex_patella_notes = fields.Text('Patella reflex notes', states=STATES)
    neuro_reflex_aquiliano = fields.Selection([
        (None,''),
        ('absent', 'Absent'),
        ('present', 'Present')
        ], 'Aquiliano reflex', states=STATES)
    neuro_reflex_aquiliano_string = \
        neuro_reflex_aquiliano.translated('neuro_reflex_aquiliano')
    neuro_reflex_aquiliano_notes = fields.Text('Aquiliano reflex notes', states=STATES)
    neuro_reflex_others = fields.Text('Other reflex', states=STATES)

    ''' Schooling '''

    '''Psycological assessment'''
    id_level = fields.Integer('ID level', states=STATES)
    id_level_legend = fields.Selection([
        (None,''),
        ('mild_mr', 'Mild mental retardation'),
        ('moderate_mr', 'Moderate mental retardation'),
        ('severe_mr', 'Severe mental retardation'),
        ('above_average', 'Above average'),
        ], 'ID level legend', sort=False, states=STATES)
    language_level = fields.Text('Language level', states=STATES)
    mood_personality = fields.Text('Mood / personality', states=STATES)
    # communication
    read_ = fields.Boolean('Read', states=STATES)
    write_ = fields.Selection([
        (None,''),
        ('alone', 'Alone'),
        ('with_support', 'With support')
        ], 'Write', sort=False, states=STATES)
    write_string = write_.translated('write_')
    sign_language = fields.Boolean('Sign language', states=STATES)
    graph_language = fields.Boolean('Graph language', states=STATES)
    other_language = fields.Char('Other language', states=STATES)
    # sociability
    has_couple = fields.Boolean('Has couple', states=STATES)
    group_of_friends = fields.Boolean('Group of friends', states=STATES)
    activity_workshop = fields.Char('Activity workshop', states=STATES)
    alcohol = fields.Boolean('Alcohol consumption', states=STATES)
    alcohol_frequency = fields.Selection([
        (None, ''),
        ('daily', 'Daily'),
        ('weekly', 'Weekly'),
        ('social', 'Social'),
        ], 'Alcohol frequency', sort=False, states=STATES)
    alcohol_frequency_string = alcohol_frequency.translated('alcohol_frequency')
    sports_hobbies = fields.Text('Sports and hobbies', states=STATES)
    sports_hobbies_center = fields.Boolean('Sports and hobbies specialized center', states=STATES)

    occupational_center = fields.Boolean('Occupational center', states=STATES)
    has_job = fields.Boolean('Has job?', states=STATES)
    job = fields.Many2One('gnuhealth.occupation', 'Job',
            states={'readonly': Or(~Eval('has_job'), Eval('state')=='signed')})
    would_like_job = fields.Boolean('Would like a job?', states=STATES)
    # fine motor activity
    sewing = fields.Char('Sewing', states=STATES)
    drawing = fields.Char('Drawing', states=STATES)
    knitting = fields.Char('Knitting', states=STATES)
    scissors = fields.Char('Scissors', states=STATES)
    zip_closure = fields.Char('Zip closure', states=STATES)
    # means of mobilization
    bicycle = fields.Char('Bicycle', states=STATES)
    skate = fields.Char('Skate', states=STATES)
    other_means_of_mobilization = fields.Char('Other means of mobilization', states=STATES)
    attentional_focus = fields.Text('Maintenance of attentional focus', states=STATES)

    ''' Behaviour '''
    behaviour = fields.Text('Behaviour during consultation', states=STATES)
    accompanying_persons = \
        fields.Many2Many('gnuhealth.trisomy21.lejeune_attention-family_member',
                'lejeune_attention', 'member',
                'Accompanying persons during consultation',
                domain=[('id', 'in', Eval('accompanying_persons_domain'))], states=STATES)
    accompanying_persons_domain = fields.Function(
        fields.Many2Many('gnuhealth.family_member',
                None, None,
                'Accompanying persons during consultation'),
        'on_change_with_accompanying_persons_domain')

    ''' Requests and suggestions '''
    requests = fields.Text('Requests', states=STATES)
    suggestions = fields.Text('Suggestions', states=STATES)

    @fields.depends('appointment')
    def on_change_with_patient(self, name=None):
        if self.appointment and self.appointment.patient:
            return self.appointment.patient.id
        return None

    @fields.depends('patient')
    def on_change_with_accompanying_persons_domain(self, name=None):
        pool = Pool()
        Family = pool.get('gnuhealth.family')
        Member = pool.get('gnuhealth.family_member')

        if self.patient:
            families = Family.search([
                    ('members.party.id', '=', self.patient.name.id)
                    ])
            members = Member.search([
                ('name', 'in', [f.id for f in families]),
                ('party.id', '!=', self.patient.name.id)
                ])
            return [m.id for m in members]

    @classmethod
    def search_patient(cls, clause, name):
        return [('id','>',0)]

    @classmethod
    def generate_code(cls, **pattern):
        Config = Pool().get('gnuhealth.sequences')
        config = Config(1)
        sequence = config.get_multivalue(
            'lejeune_attention_sequence', **pattern)
        if sequence:
            return sequence.get()

    @staticmethod
    def default_attention_start():
        return datetime.today()

    @classmethod
    @ModelView.button
    def end_evaluation(cls, evaluations):
        super().end_evaluation(evaluations)

    @classmethod
    @ModelView.button
    def sign_evaluation(cls, evaluations):
        super().sign_evaluation(evaluations)

    @classmethod
    def get_serial(cls, evaluation):
        data_to_serialize = {
            'Patient': str(evaluation.patient.rec_name) or '',
            'Start': str(evaluation.evaluation_start) or '',
            'End': str(evaluation.evaluation_endtime) or '',
            'Initiated_by': str(evaluation.healthprof.rec_name),
            'Signed_by': evaluation.signed_by and
                str(evaluation.signed_by.rec_name) or '',
            'Specialty': evaluation.specialty and
                str(evaluation.specialty.rec_name) or '',
            'name': evaluation.name,
            '''Systemic Anamnesis'''
            # cardiovascular
            'cardiov_healthprof': evaluation.cardiov_healthprof and \
                evaluation.cardiov_healthprof.rec_name or '',
            'cardiov_health_inst': evaluation.cardiov_health_inst and \
                evaluation.cardiov_health_inst.rec_name or '',
            # neurological
            'neuropsyc_assessment': str(evaluation.neuropsyc_assessment),
            'dementia_assessment': str(evaluation.dementia_assessment),
            'cephalic_support': str(evaluation.cephalic_support),
            'sphinters_control': str(evaluation.sphinters_control),
            'neuro_observations': evaluation.neuro_observations,
            'neuro_healthprof': evaluation.neuro_healthprof and \
                evaluation.neuro_healthprof.rec_name or '',
            'neuro_health_inst': evaluation.neuro_health_inst and \
                evaluation.neuro_health_inst.rec_name or '',
            # psychiatric
            'behaviour_disorder': str(evaluation.behaviour_disorder),
            'self_inflicted_damage': str(evaluation.self_inflicted_damage),
            'tea': str(evaluation.tea),
            'tdha': str(evaluation.tdha),
            'psych_observations': evaluation.psych_observations or '',
            'psych_health_prof': evaluation.psych_health_prof and \
                evaluation.psych_health_prof.id or '',
            'psych_health_inst': evaluation.psych_health_inst and \
                evaluation.psych_health_inst.id or '',
            # gastrointestinal
            'feeding': evaluation.feeding or '',
            'feeding_intolerance': str(evaluation.feeding_intolerance),
            'feeding_intolerance_description': \
                evaluation.feeding_intolerance_description or '',
            'bowel_habit': evaluation.bowel_habit or '',
            'gerd_symptoms': str(evaluation.gerd_symptoms),
            'fecal_occult_blood': evaluation.fecal_occult_blood or '',
            'videocolonoscopy': evaluation.videocolonoscopy or '',
            'celiac_screening': evaluation.celiac_screening or '',
            'hla_dq2_dq8': evaluation.hla_dq2_dq8 or '',
            'gastroint_observations': evaluation.gastroint_observations or '',
            'gastroint_health_prof': evaluation.gastroint_health_prof and \
                evaluation.gastroint_health_prof.rec_name or '',
            'gastroint_health_inst': evaluation.gastroint_health_inst and \
                evaluation.gastroint_health_inst.rec_name or '',
            # neumonology
            'recurrent_infections': str(evaluation.recurrent_infections),
            'recurrent_infections_description': \
                evaluation.recurrent_infections_description or '',
            'hospitalizations': str(evaluation.hospitalizations),
            'hospitalizations_description': \
                evaluation.hospitalizations_description or '',
            'neumo_health_prof': evaluation.neumo_health_prof and \
                evaluation.neumo_health_prof.rec_name or '',
            'neumo_health_inst': evaluation.neumo_health_inst and \
                evaluation.neumo_health_inst.rec_name or '',
            # orl
            'last_audiometry': evaluation.last_audiometry and \
                str(evaluation.last_audiometry) or '',
            'sleep_wake_cycle': evaluation.sleep_wake_cycle or '',
            'apnea': str(evaluation.apnea),
            'snoring': str(evaluation.snoring),
            'polysomnography': str(evaluation.polysomnography),
            'oea': evaluation.oea,
            'orl_observations': evaluation.orl_observations or '',
            'orl_health_prof': evaluation.orl_health_prof and \
                evaluation.orl_health_prof.rec_name or '',
            'orl_health_inst': evaluation.orl_health_inst and \
                evaluation.orl_health_inst.rec_name or '',
            # odontostomatology
            'agenesia': str(evaluation.agenesia),
            'late_dentition': str(evaluation.late_dentition),
            'last_dental_control': evaluation.last_dental_control and \
                str(evaluation.last_dental_control) or '',
            'dental_orthosis': str(evaluation.dental_orthosis),
            'dental_orthosis_description': \
                evaluation.dental_orthosis_description or '',
            'periodontal_disease_counseling': \
                evaluation.periodontal_disease_counseling or '',
            'dental_health_prof': evaluation.dental_health_prof and \
                evaluation.dental_health_prof.rec_name or '',
            'dental_health_inst': evaluation.dental_health_inst and \
                evaluation.dental_health_inst.rec_name or '',
            # ophthalmology
            'last_biannual_ophthalm_assessment': \
                evaluation.last_biannual_ophthalm_assessment and \
                str(evaluation.last_biannual_ophthalm_assessment) or '',
            'ophthalm_healthprof': evaluation.ophthalm_healthprof and \
                evaluation.ophthalm_healthprof.rec_name or '',
            'ophthalm_health_inst': evaluation.ophthalm_health_inst and \
                evaluation.ophthalm_health_inst.rec_name or '',
            # endocrinology
            'thyroid_ultrasound': str(evaluation.thyroid_ultrasound),
            'thyroid_ultrasound_observations': evaluation.thyroid_ultrasound_observations or '',
            'annual_tsh': evaluation.annual_tsh and \
                str(evaluation.annual_tsh) or '',
            'glycemia': evaluation.glycemia and \
                str(evaluation.glycemia) or '',
            'cholesterol': evaluation.cholesterol and \
                str(evaluation.cholesterol) or '',
            'bone_densitometry': evaluation.bone_densitometry or '',
            'metabolic_anormal': str(evaluation.metabolic_anormal),
            'metabolic_anormal_description': \
                evaluation.metabolic_anormal_description or '',
            't4': evaluation.t4 or '',
            'endoc_health_prof': evaluation.endoc_health_prof and \
                evaluation.endoc_health_prof.rec_name or '',
            'endoc_health_inst': evaluation.endoc_health_inst and \
                evaluation.endoc_health_inst.rec_name or '',
            # gynecology
            'menarche': evaluation.menarche \
                and str(evaluation.menarche) or '',
            'menopause': evaluation.menopause \
                and str(evaluation.menopause) or '',
            'early_mp_suffocation': str(evaluation.early_mp_suffocation),
            'early_mp_night_sweats': str(evaluation.early_mp_night_sweats),
            'early_mp_headache': str(evaluation.early_mp_headache),
            'early_mp_dec_libido': str(evaluation.early_mp_dec_libido),
            'early_mp_menstrual_change':  \
                str(evaluation.early_mp_menstrual_change),
            'early_mp_irritability': str(evaluation.early_mp_irritability),
            'early_mp_mood_changes': str(evaluation.early_mp_mood_changes),
            'gyneco_health_prof': evaluation.gyneco_health_prof and \
                evaluation.gyneco_health_prof.rec_name or '',
            'gyneco_health_inst': evaluation.gyneco_health_inst and \
                evaluation.gyneco_health_inst.rec_name or '',
            # fertility
            'sex_education': str(evaluation.sex_education),
            'sexual_relations': str(evaluation.sexual_relations),
            'anticonceptive': evaluation.anticonceptive or '',
            'fert_health_prof': evaluation.fert_health_prof and \
                evaluation.fert_health_prof.rec_name or '',
            'fert_health_inst': evaluation.fert_health_inst and \
                evaluation.fert_health_inst.rec_name or '',
            # andrology
            'andro_mood_changes': str(evaluation.andro_mood_changes),
            'andro_fatigue': str(evaluation.andro_fatigue),
            'andro_weight_inc': str(evaluation.andro_weight_inc),
            'andro_irritability': str(evaluation.andro_irritability),
            'andro_depression': str(evaluation.andro_depression),
            'andro_sweat': str(evaluation.andro_sweat),
            'andro_headache': str(evaluation.andro_headache),
            'annual_testicular_control': str(evaluation.annual_testicular_control),
            'annual_testicular_us': str(evaluation.annual_testicular_us),
            'andro_health_prof': evaluation.andro_health_prof and \
                evaluation.andro_health_prof.rec_name or '',
            'andro_health_inst': evaluation.andro_health_inst and \
                evaluation.andro_health_inst.rec_name or '',
            'uronefro_health_prof': evaluation.uronefro_health_prof and \
                evaluation.uronefro_health_prof.rec_name or '',
            'uronefro_health_inst': evaluation.uronefro_health_inst and \
                evaluation.uronefro_health_inst.rec_name or '',
            # orthopedics and traumatology
            'scoliosis': str(evaluation.scoliosis),
            'scoliosis_description': evaluation.scoliosis_description or '',
            'patella_inestability': str(evaluation.patella_inestability),
            'patella_inestability_description': evaluation.patella_inestability_description or '',
            'carpal_tunnel_synd': str(evaluation.carpal_tunnel_synd),
            'chronic_cervical_myel': str(evaluation.chronic_cervical_myel),
            'flatfoot': str(evaluation.flatfoot),
            'orthesis_prosthesis': str(evaluation.orthesis_prosthesis),
            'xray_cervical': str(evaluation.xray_cervical),
            'xray_pavlov': str(evaluation.xray_pavlov),
            'xray_hip_panoramic': str(evaluation.xray_hip_panoramic),
            'xray_von_rosen': str(evaluation.xray_von_rosen),
            'reumatology': evaluation.reumatology or '',
            'orth_health_prof': evaluation.orth_health_prof and \
                evaluation.orth_health_prof.rec_name or '',
            'orth_health_inst': evaluation.orth_health_inst and \
                evaluation.orth_health_inst.rec_name or '',
            # dermatology
            'periodic_phys_examination': str(evaluation.periodic_phys_examination),
            'autoimmune_disease': str(evaluation.autoimmune_disease),
            'derma_observations': evaluation.derma_observations or '',
            'derma_health_prof': evaluation.derma_health_prof and \
                evaluation.derma_health_prof.rec_name or '',
            'derma_health_inst': evaluation.derma_health_inst and \
                evaluation.derma_health_inst.rec_name or '',
            # oncohematology
            'onco_observations': evaluation.onco_observations or '',
            'onco_health_prof': evaluation.onco_health_prof and \
                evaluation.onco_health_prof.rec_name or '',
            'onco_health_inst': evaluation.onco_health_inst and \
                evaluation.onco_health_inst.rec_name or '',
            ''' Current Therapies '''
            'phonoaudiology': evaluation.phonoaudiology or '',
            'phonoaudiology_freq': evaluation.phonoaudiology_freq and \
                str(evaluation.phonoaudiology_freq) or '',
            'occ_therapy': evaluation.occ_therapy or '',
            'occ_therapy_freq': evaluation.occ_therapy and \
                str(evaluation.occ_therapy) or '',
            'psycologist': evaluation.psycologist or '',
            'psycologist_freq': evaluation.psycologist_freq and \
                str(evaluation.psycologist_freq) or '',
            'psychiatrist': evaluation.psychiatrist and \
                str(evaluation.psychiatrist) or '',
            'psychiatrist_freq': evaluation.psychiatrist_freq and \
                str(evaluation.psychiatrist_freq) or '',
            'kinesiotherapy': evaluation.kinesiotherapy or '',
            'kinesiotherapy_freq': evaluation.kinesiotherapy_freq and \
                str(evaluation.kinesiotherapy_freq) or '',
            'therapeutic_acc': evaluation.therapeutic_acc or '',
            'therapeutic_acc_freq': evaluation.therapeutic_acc_freq and \
                str(evaluation.therapeutic_acc_freq) or '',
            'nursing_assistance': evaluation.nursing_assistance or '',
            'nursing_assistance_notes': evaluation.nursing_assistance_notes or '',
            'current_ther_observations': evaluation.current_ther_observations or '',
            ''' Physical exam'''
            # vital signs
            'systolic': evaluation.systolic and \
                str(evaluation.systolic) or '',
            'diastolic': evaluation.diastolic and \
                str(evaluation.diastolic) or '',
            'weight': evaluation.weight and \
                str(evaluation.weight) or '',
            'height': evaluation.height and \
                str(evaluation.height) or '',
            'cephalic_perimeter': evaluation.cephalic_perimeter and \
                str(evaluation.cephalic_perimeter) or '',
            'bmi': evaluation.cephalic_perimeter and \
                str(evaluation.cephalic_perimeter) or '',
            'skin': evaluation.skin or '',
            'mouth': evaluation.mouth or '',
            'ears': evaluation.ears or '',
            # lymphatic system
            'lympha': evaluation.lympha or '',
            # respiratory system
            'respiratory_system': evaluation.respiratory_system or '',
            # cardiovascular system
            'cardiovascular_system': evaluation.cardiovascular_system or '',
            # urologic system
            'urologic_system': evaluation.urologic_system or '',
            # oam system
            'oam_system': evaluation.oam_system or '',
            'oam_system_art_limit': evaluation.oam_system_art_limit or '',
            'oam_system_scoliosis': evaluation.oam_system_scoliosis or '',
            'oam_system_pain': evaluation.oam_system_pain or '',
            'oam_system_patella_inestability': \
                evaluation.oam_system_patella_inestability or '',
            'oam_system_heel_valgus': evaluation.oam_system_heel_valgus or '',
            # neuro system
            'neuro_system': evaluation.neuro_system or '',
            'neuro_vigil': evaluation.neuro_vigil or '',
            'neuro_orientation_time': \
                evaluation.neuro_orientation_time or '',
            'neuro_orientation_space': \
                evaluation.neuro_orientation_space or '',
            'neuro_orientation_people': \
                evaluation.neuro_orientation_people or '',
            'neuro_tone_strength_sensitivity': \
                evaluation.neuro_tone_strength_sensitivity or '',
            'neuro_dyn_coord_diadochokinesia': \
                evaluation.neuro_dyn_coord_diadochokinesia or '',
            'neuro_dyn_coord_index_nose_test': \
                evaluation.neuro_dyn_coord_index_nose_test or '',
            'neuro_dyn_coord_walk_straight': \
                evaluation.neuro_dyn_coord_walk_straight or '',
            'neuro_dyn_coord_walk_heel': \
                evaluation.neuro_dyn_coord_walk_heel or '',
            'neuro_dyn_coord_walk_tiptoes': \
                evaluation.neuro_dyn_coord_walk_tiptoes or '',
            'neuro_static_coord_romberg_simple': \
                evaluation.neuro_static_coord_romberg_simple or '',
            'neuro_static_coord_romberg_sensitized': \
                evaluation.neuro_static_coord_romberg_sensitized or '',
            'neuro_reflex_bicipital': \
                evaluation.neuro_reflex_bicipital or '',
            'neuro_reflex_bicipital_notes': \
                evaluation.neuro_reflex_bicipital_notes or '',
            'neuro_reflex_tricipital': \
                evaluation.neuro_reflex_tricipital or '',
            'neuro_reflex_tricipital_notes': \
                evaluation.neuro_reflex_tricipital_notes or '',
            'neuro_reflex_patella': \
                evaluation.neuro_reflex_patella or '',
            'neuro_reflex_patella_notes': \
                evaluation.neuro_reflex_patella_notes or '',
            'neuro_reflex_aquiliano': \
                evaluation.neuro_reflex_aquiliano or '',
            'neuro_reflex_aquiliano_notes': \
                evaluation.neuro_reflex_aquiliano_notes or '',
            'neuro_reflex_others': \
                evaluation.neuro_reflex_others or '',
            ''' Schooling '''
            ''' Psycological assessment '''
            'id_level': evaluation.id_level and \
                str(evaluation.id_level) or '',
            'id_level_legend': evaluation.id_level_legend or '',
            'language_level': evaluation.language_level or '',
            'mood_personality': evaluation.mood_personality or '',
            # communication
            'read_': str(evaluation.read_),
            'write_': evaluation.write_ or '',
            'sign_language': str(evaluation.sign_language),
            'graph_language': str(evaluation.graph_language),
            'other_language': evaluation.other_language or '',
            # sociability
            'has_couple': str(evaluation.has_couple),
            'group_of_friends': str(evaluation.group_of_friends),
            'activity_workshop': evaluation.activity_workshop or '',
            'alcohol': str(evaluation.alcohol),
            'alcohol_frequency': evaluation.alcohol_frequency or '',
            'sports_hobbies': evaluation.sports_hobbies or '',
            'sports_hobbies_center': evaluation.sports_hobbies_center or '',
            'occupational_center': str(evaluation.occupational_center),
            'has_job': str(evaluation.has_job),
            'job': evaluation.job and evaluation.job.rec_name or '',
            'would_like_job': str(evaluation.would_like_job),
            'sewing': evaluation.sewing or '',
            'drawing': evaluation.drawing or '',
            'knitting': evaluation.knitting or '',
            'scissors': evaluation.scissors or '',
            'zip_closure': evaluation.zip_closure or '',
            # means of mobilization
            'bicycle': evaluation.bicycle or '',
            'skate': evaluation.skate or '',
            'other_means_of_mobilization': \
                evaluation.other_means_of_mobilization or '',
            'attentional_focus': evaluation.attentional_focus or '',
            ''' Behaviour '''
            'behaviour': evaluation.behaviour or '',
            'accompanying_persons': evaluation.accompanying_persons and \
                '; '.join([ap.rec_name for ap
                           in evaluation.accompanying_persons]),
            ''' Requests and suggestions'''
            'requests': evaluation.requests or '',
            'suggestions': evaluation.suggestions,

            }

        serialiazed_doc = str(HealthCrypto().serialize(data_to_serialize))

        return serialiazed_doc

    @classmethod
    def __setup__(cls):
        super().__setup__()

        t = cls.__table__()
        cls._sql_constraints = [
            ('code_unique', Unique(t, t.code),
                'The evaluation code must be unique !'),
            ]

        cls._order.insert(0, ('evaluation_start', 'DESC'))
        cls._buttons.update({
            'end_evaluation': {'invisible': Or(Equal(Eval('state'), 'signed'),
                                               Equal(Eval('state'), 'done'))
                            },
            'sign_evaluation': {
                'invisible': Not(Equal(Eval('state'), 'done'))
                            },
            })
        cls.__rpc__.update({
                'set_signature': RPC(readonly=False),
                })

    @classmethod
    def view_attributes(cls):
        return [('//group[@id="group_digital_signature"]', 'states', {
                'invisible': ~Eval('digital_signature')}),
                ('//group[@id="group_current_string"]', 'states', {
                 'invisible': ~Eval('digest_status'),
                 })] + [
                ('//page[@id="gynecology"]', 'states', {
                'invisible': Eval('gender')=='m'
                })] + [
                ('//page[@id="andrology"]', 'states', {
                'invisible': Eval('gender')=='f'
                })]


class SurgicalHistory(ModelView, ModelSQL):
    'Surgical History'
    __name__ = 'gnuhealth.surgical_history'

    patient = fields.Many2One('gnuhealth.patient', 'Patient')
    healthprof = fields.Many2One('gnuhealth.healthprofessional',
                'Health professional')
    surgery_date = fields.Date('Surgery date')
    health_institution = fields.Many2One('gnuhealth.institution',
                'Health Institution')
    surgery_kind = fields.Selection([
        (None,''),
        ('cardiovascular', 'Cardiovascular'),
        ('neurology', 'Neurovascular'),
        ('gastrointestinal', 'Gastrointestinal'),
        ('pulmology', 'Pulmological'),
        ('ent', 'ENT'),
        ('odontostamolotology', 'Odontostamolotology'),
        ('ophtalmic', 'Ophtalic'),
        ('endocrine', 'Endocrine'),
        ('gynecology', 'Gynecology'),
        ('uronefrology', 'Uronefrology'),
        ('traumatology', 'Traumatology'),
        ('dermatology', 'Dermatology'),
        ('oncohematology', 'Oncohematology'),
        ], 'Surgery kind', sort=False)
    procedures = fields.One2Many('gnuhealth.surgical_history.operation',
                'name', 'Operation')
    notes = fields.Text('Notes')


class SurgicalHistoryOperation(ModelSQL, ModelView):
    'Surgical History - Operation'
    __name__ = 'gnuhealth.surgical_history.operation'

    name = fields.Many2One('gnuhealth.surgical_history', 'Surgical History')
    procedure = fields.Many2One('gnuhealth.procedure',
        'Code', required=True, select=True,
        help="Procedure Code, for exampl ICD-10-PCS or ICPM")
    main_procedure = fields.Boolean('Main procedure')
    notes = fields.Text('Notes')

    def get_rec_name(self, name):
        return self.procedure.rec_name


class LejeuneAttentionFamilyMember(ModelSQL):
    'Lejeune Attention - Family Member'
    __name__ = 'gnuhealth.trisomy21.lejeune_attention-family_member'

    lejeune_attention = \
        fields.Many2One('gnuhealth.trisomy21.lejeune_attention',
                'Lejeune attention', ondelete='CASCADE')
    member = fields.Many2One('gnuhealth.family_member', 'Member',
                ondelete='CASCADE')
