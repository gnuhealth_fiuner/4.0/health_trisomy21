from trytond.model import (ModelView, ModelSingleton, ModelSQL,
                           ValueMixin, MultiValueMixin, fields)
from trytond import backend
from trytond.pyson import Id
from trytond.pool import Pool, PoolMeta
from trytond.tools.multivalue import migrate_property
from trytond.modules.health.sequences import _ConfigurationValue

#barthel_evaluation_sequence
#lawton_brody_evaluation_sequence

barthel_evaluation_sequence = fields.Many2One(
    'ir.sequence', 'Barthel Evaluation Sequence', required=True,
    domain=[('sequence_type', '=', Id(
        'health_trisomy21', 'seq_type_gnuhealth_barthel_evaluation'))])
lawton_brody_evaluation_sequence = fields.Many2One(
    'ir.sequence', 'Lawton - Brody Evaluation Sequence', required=True,
    domain=[('sequence_type', '=', Id(
        'health_trisomy21', 'seq_type_gnuhealth_lawton_brody_evaluation'))])
lejeune_attention_sequence = fields.Many2One(
    'ir.sequence', 'Lejeune Attention Sequence', required=True,
    domain=[('sequence_type', '=', Id(
        'health_trisomy21', 'seq_type_gnuhealth_lejeune_attention'))])


# GNU HEALTH SEQUENCES
class GnuHealthSequences(metaclass=PoolMeta):
    __name__ = 'gnuhealth.sequences'

    barthel_evaluation_sequence = fields.MultiValue(
        barthel_evaluation_sequence)
    lawton_brody_evaluation_sequence = fields.MultiValue(
        lawton_brody_evaluation_sequence)
    lejeune_attention_sequence = fields.MultiValue(
        lejeune_attention_sequence)

    @classmethod
    def default_barthel_evaluation_sequence(cls, **pattern):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('health_trisomy21',
                                    'seq_gnuhealth_barthel_evaluation')
        except KeyError:
            return None

    @classmethod
    def default_lawton_brody_evaluation_sequence(cls, **pattern):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('health_trisomy21',
                                    'seq_gnuhealth_lawton_brody_evaluation')
        except KeyError:
            return None

    @classmethod
    def default_lejeune_attention_sequence(cls, **pattern):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('health_trisomy21',
                                    'seq_gnuhealth_lejeune_attention')
        except KeyError:
            return None


class BarthelEvaluationSequence(_ConfigurationValue, ModelSQL, ValueMixin):
    'Barthel Evaluation Sequence setup'
    __name__ = 'gnuhealth.sequences.barthel_evaluation_sequence'
    barthel_evaluation_sequence = barthel_evaluation_sequence
    _configuration_value_field = 'barthel_evaluation_sequence'

    @classmethod
    def check_xml_record(cls, records, values):
        return True


class LawtonBrodyEvaluationSequence(_ConfigurationValue, ModelSQL, ValueMixin):
    'Barthel Evaluation Sequence setup'
    __name__ = 'gnuhealth.sequences.lawton_brody_evaluation_sequence'
    lawton_brody_evaluation_sequence = lawton_brody_evaluation_sequence
    _configuration_value_field = 'lawton_brody_evaluation_sequence'

    @classmethod
    def check_xml_record(cls, records, values):
        return True


class LejeuneAttentionSequence(_ConfigurationValue, ModelSQL, ValueMixin):
    'Barthel Evaluation Sequence setup'
    __name__ = 'gnuhealth.sequences.lejeune_attention_sequence'
    lejeune_attention_sequence = lejeune_attention_sequence
    _configuration_value_field = 'lejeune_attention_sequence'

    @classmethod
    def check_xml_record(cls, records, values):
        return True
