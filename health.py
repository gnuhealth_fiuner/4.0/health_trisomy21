from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Not, Bool

from datetime import datetime


class PatientData(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient'

    email = fields.Function(
        fields.Char('Email'),
        'on_change_with_email', setter='set_party_email')
    last_confirmed_appointment = fields.Function(
        fields.DateTime('Last confirmed appointment'),
        'get_last_confirmed_appointment')
    last_appointment_attended = fields.Function(
        fields.DateTime('Last appointment attended',
            help='Last appointment checked or done'),
        'get_last_appointment_attended')
    next_appointment_confirmed = fields.Function(
        fields.DateTime('Next appointment confirmed'),
        'get_next_appointment_confirmed')
    next_appointment_to_confirm = fields.Function(
        fields.DateTime('Next appointment to confirm'),
        'get_next_appointment_to_confirm')
    subsidized = fields.Boolean('Subsidized')
    subsidized_percentage = fields.Integer('Subsidized percentage',
            states={
                'invisible': Not(Bool(Eval('subsidized'))),
                'required': Bool(Eval('subsidized'))},
            domain=[
                ('subsidized_percentage', '>=', 0),
                ('subsidized_percentage', '<=', 100)
                ])
    # neurological
    age_wich_sat = fields.Integer('Age at which he/she sat down',
                help="Age in years")
    age_wich_walked = fields.Integer('Age at which he/she started to walk',
                help="Age in years")
    age_wich_talked = fields.Integer('Age at which he/she started to talk',
                help="Age in years")
    neuropsyc_assessment = fields.Boolean('Neuropsychological assessment')
    dementia_assessment = fields.Boolean('Dementia assessment')
    # uronefrology
    andro_mood_changes = fields.Boolean('Mood changes',
                    help="Andropause signs")
    andro_fatigue = fields.Boolean('Fatigue',
                    help="Andropause signs")
    andro_weight_inc = fields.Boolean('Weight increment',
                    help="Andropause signs")
    andro_irritability = fields.Boolean('Irritability',
                    help="Andropause signs")
    andro_depression = fields.Boolean('Depression',
                    help="Andropause signs")
    andro_sweat = fields.Boolean('Sweat', help="Andropause signs")
    andro_headache = fields.Boolean('Headache', help="Andropause signs")
    annual_testicular_control = fields.Boolean('Annual testicular control',
                                help="Up to 45 years")
    annual_testicular_us = fields.Boolean('Annual testicular ultrasound')

    @classmethod
    def on_change_with_email(cls, patient, name=None):
        result = {}
        for a in patient:
            result[a.id] = a.name.email or ''
        return result

    @classmethod
    def set_party_email(cls, patient, name, value):
        pool = Pool()
        ContactMechanism = pool.get('party.contact_mechanism')
        for a in patient:
            if not a.name:
                continue
            party_email = ContactMechanism.search([
                ('party', '=', a.name),
                ('type', '=', 'email'),
                ])
            if party_email:
                ContactMechanism.write(party_email, {
                    'value': value,
                    })
            else:
                ContactMechanism.create([{
                    'party': a.name.id,
                    'type': 'email',
                    'value': value,
                    }])

    def get_last_confirmed_appointment(self, name=None):
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        appointments = Appointment.search([
                    ('patient', '=', self.id),
                    ('state', '=', 'confirmed'),
                    ('appointment_date', '<', datetime.today())
                    ], order=[('appointment_date','DESC')])
        if appointments:
            return appointments[0].appointment_date
        return None

    def get_last_appointment_attended(self, name=None):
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        appointments = Appointment.search([
                    ('patient', '=', self.id),
                    ('state', 'in', ['checked','done']),
                    ('appointment_date', '<', datetime.today())
                    ], order=[('appointment_date','DESC')])
        if appointments:
            return appointments[0].appointment_date
        return None

    def get_next_appointment_confirmed(self, name=None):
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        appointments = Appointment.search([
                    ('patient', '=', self.id),
                    ('state', '=', 'confirmed'),
                    ('appointment_date', '>', datetime.today())
                    ], order=[('appointment_date','ASC')])
        if appointments:
            return appointments[0].appointment_date
        return None

    def get_next_appointment_to_confirm(self, name=None):
        pool = Pool()
        Appointment = pool.get('gnuhealth.appointment')
        appointments = Appointment.search([
                    ('patient', '=', self.id),
                    ('state', '=', 'to_confirm'),
                    ('appointment_date', '>', datetime.today())
                    ], order=[('appointment_date','ASC')])
        if appointments:
            return appointments[0].appointment_date
        return None

    @staticmethod
    def default_subsidized_percentage():
        return 0


class Appointment(metaclass=PoolMeta):
    __name__ = 'gnuhealth.appointment'

    confirm_date = fields.DateTime('Confirm date',
                states={'invisible': Eval('state').in_([None, 'free', 'to_confirm'])})
    doc_submitted = fields.Boolean('Documentation submitted')
    doc_submitted_date = fields.Date('Date of submitted documentation',
                states={'invisible': Not(Bool(Eval('doc_submitted')))})
    email = fields.Function(
        fields.Char('Email'),
        'on_change_with_email', setter='set_email')
    referred_hp = fields.Many2One('gnuhealth.healthprofessional', 'Referred Health Prof',
            states={'invisible': Eval('visit_type') != 'referral'})
    referred_hp_speciality = fields.Many2One('gnuhealth.specialty', 'Referred Speciality',
            states={'invisible': Eval('visit_type') != 'referral'})

    @fields.depends('patient')
    def on_change_patient(self):
        super().on_change_patient()
        if self.patient:
            self.state = 'to_confirm'
            self.email = self.patient.email
            self.visit_type = 'scheduled_appointment'
        else:
            self.email = None

    @fields.depends('state')
    def on_change_state(self, name=None):
        if self.state == 'confirmed':
            self.confirm_date = datetime.today()

    @fields.depends('doc_submitted')
    def on_change_with_doc_submitted_date(self, name=None):
        if self.doc_submitted:
            return datetime.today().date()
        return None

    @classmethod
    def on_change_with_email(cls, appointment, name=None):
        result = {}
        for a in appointment:
            result[a.id] = a.patient and a.patient.name.email or ''
        return result

    @classmethod
    def set_email(cls, appointment, name, value):
        pool = Pool()
        ContactMechanism = pool.get('party.contact_mechanism')
        for a in appointment:
            if not a.patient:
                continue
            email = ContactMechanism.search([
                ('party', '=', a.patient.name),
                ('type', '=', 'email'),
                ])
            if email:
                ContactMechanism.write(email, {
                    'value': value,
                    })
            else:
                ContactMechanism.create([{
                    'party': a.patient.name.id,
                    'type': 'email',
                    'value': value,
                    }])

    @fields.depends('referred_hp')
    def on_change_with_referred_hp_speciality(self, name=None):
        if self.referred_hp and self.referred_hp.main_specialty:
            return self.referred_hp.main_specialty.specialty
        return None

    @classmethod
    def __setup__(cls):
        super().__setup__()
        # insert the visit_type values on the first
        cls.visit_type.selection.insert(1,('first_consultation', 'First consultation'))
        cls.visit_type.selection.insert(2,('second_consultation', 'Second consultation'))
        cls.visit_type.selection.insert(4,('referral', 'Referral'))
        cls.visit_type.selection[cls.visit_type.selection.index(
            ('followup', 'Followup'))], \
            cls.visit_type.selection[3] = \
                cls.visit_type.selection[3], \
                cls.visit_type.selection[cls.visit_type.selection.index(
                    ('followup', 'Followup'))]

        # insert the new state 'to_confirm'
        cls.state.selection.insert(2,('to_confirm','To confirm'))
        # remove options from visit_type
        cls.visit_type.selection.remove(('well_child', 'Well Child visit'))
        cls.visit_type.selection.remove(('well_woman', 'Well Woman visit'))
        cls.visit_type.selection.remove(('well_man', 'Well Man visit'))

    @staticmethod
    def default_state():
        return 'to_confirm'

    @classmethod
    def write(cls, appointments, values):
        for appointment in appointments:
            if appointment.name is None:
                values['name'] = cls.generate_code()
        return super().write(appointments, values)


class PatientEvaluation(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.evaluation'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.visit_type.selection.insert(1,('first_consultation', 'First consultation'))
        cls.visit_type.selection.insert(2,('second_consultation', 'Second consultation'))
        cls.visit_type.selection.insert(4,('referral', 'Referral'))
        cls.visit_type.selection[cls.visit_type.selection.index(
            ('followup', 'Followup'))], \
            cls.visit_type.selection[3] = \
                cls.visit_type.selection[3], \
                cls.visit_type.selection[cls.visit_type.selection.index(
                    ('followup', 'Followup'))]

        #insert the new state 'to_confirm'
        cls.state.selection.insert(2,('to_confirm','To confirm'))


class PatientDiseaseInfo(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.disease'

    congenital = fields.Boolean('Congenital')
    age_selection = fields.Selection([
        (None, ''),
        ('days', 'Days'),
        ('months', 'Months'),
        ('years', 'Years')
        ], 'Age selección', sort=False)

    @staticmethod
    def default_age_selection():
        return 'years'


class PatientECG(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.ecg'

    ecg_age = fields.Selection([
        (None, ''),
        ('20years', '20 years'),
        ('40years', '40 years'),
        ('every5years', 'Every 5 years')
        ],'ECG for age', sort=False)
