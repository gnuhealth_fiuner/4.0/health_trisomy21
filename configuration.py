from trytond.model import ModelSingleton, ModelSQL, ModelView, fields


class TrisomyConfiguration(ModelSingleton, ModelSQL, ModelView):
    "Trisomy21 Configuration"
    __name__ = 'gnuhealth.trisomy21.configuration'

    # start
    start_briefing = fields.Text("Start briefing")
    # pediatrics
    pediatrics_briefing = fields.Text("Pediatrics briefing")
    # anamnesis
    conditions_briefing = fields.Text("Conditions briefing")
    neurologic_briefing = fields.Text("Neurologic briefing")
    psychiatric_briefing = fields.Text("Psychiatric briefing")
    gastrointestinal_briefing = fields.Text("Gastrointestinal briefing")
    neumonology_briefing = fields.Text("Neumonology briefing")
    orl_briefing = fields.Text("ORL briefing")
    odontostomatology_briefing = fields.Text("Odontostomatology briefing")
    ophthalmology_briefing = fields.Text("Ophthalmology briefing")
    endocrinology_briefing = fields.Text("Endocrinology briefing")
    gynecology_briefing = fields.Text("Gynecology briefing")
    fertility_briefing = fields.Text("Fertility briefing")
    andrology_briefing = fields.Text("Andrology briefing")
    uronefrology_briefing = fields.Text("Uronefrology briefing")
    orthopedics_traumato_briefing = fields.Text(
                    "Orthopedics and Traumatology briefing")
    disability_briefing = fields.Text("Disability briefing")
    dermatology_briefing = fields.Text("Dermatology briefing")
    oncohematology_briefing = fields.Text("Oncohematology briefing")
    # vaccines
    vaccines_briefing = fields.Text("Vaccines briefing")
    # medication
    medication_briefing = fields.Text("Medication briefing")
    # current therapies
    current_therapies_briefing = fields.Text("Current therapies briefing")
    #physical exam
    vital_signs_briefing = fields.Text("Vital signs briefing")
    lymphatic_briefing = fields.Text("Lymphatic briefing")
    respiratory_briefing = fields.Text("Respiratory briefing")
    cardiovascular_briefing = fields.Text("Cardiovascular briefing")
    digestive_briefing = fields.Text("Digestive briefing")
    urologic_briefing = fields.Text("Urologic briefing")
    oam_briefing = fields.Text("OAM briefing", help="Osteo Arthro Muscular")
    neurologic_exam_briefing = fields.Text("Neurologic exam briefing")
    # schoolarship
    schoolarship_briefing = fields.Text("Schoolarship briefing")
    # psycologic
    psycologic_briefing = fields.Text("Psycologic briefing")
    # barthel
    barthel_briefing = fields.Text("Barthel briefing")
    # lawton_brody
    lawton_brody_briefing = fields.Text("Lawton and Brody briefing")
    # behaviour
    behaviour_briefing = fields.Text("Behaviour briefing")
    # requests and suggestions
    requests_briefing = fields.Text("Requests briefing")
    suggestions_briefing = fields.Text("Suggestions briefing")
    # laboratory
    laboratory_briefing = fields.Text("Laboratory briefing")
